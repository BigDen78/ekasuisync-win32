
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "pies"
#pragma resource "*.dfm"
TForm1 *Form1;

//---------------------------------------------------------------------------
void DecodePUT_GL_Text(System::UnicodeString Src, System::UnicodeString* NAME, int* NAPR, System::UnicodeString* IDPUT, int* StKm, int* StM, int* EdKm, int* EdM) {

//IDNAPR;NAMEPUT;NDOR;NAPR;NPUT;IDPUT;RWIDTH;S_KM;S_M;E_KM;E_M
//IDNAPR;                      NAMEPUT;   NDOR;   NAPR;   NPUT;       IDPUT;  RWIDTH; S_KM; S_M;  E_KM; E_M
//UP_224; ������� ���� ������-��������,I;    1;  11034;      I; 110000122892;   1520;    0; 982;     2; 421

	if ((NAME) && (NAPR) && (IDPUT) && (StKm) && (StM) && (EdKm) && (EdM)) {

		TStringList* delim1 = new (TStringList);
		delim1->Delimiter = ';';
		delim1->StrictDelimiter = True;
		delim1->DelimitedText = Src;

		*NAME = delim1->Strings[1];
		*NAPR = StrToInt(delim1->Strings[3]);
		*IDPUT = delim1->Strings[5];
		*StKm = StrToInt(delim1->Strings[7]);
		*StM = StrToInt(delim1->Strings[8]);
		*EdKm = StrToInt(delim1->Strings[9]);
		*EdM = StrToInt(delim1->Strings[10]);
		delete delim1;
	}
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
	Config_ = new cConfig();
	Config_->SkipData = true;
	Config_->DebugMode = true;
	Config_->ExLOG = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdHTTP1Work(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount)
{
	if (AWorkMode = wmRead) OnTextToLog("IdHTTP -> Work::wmRead " + IntToStr(AWorkCount)); else OnTextToLog("IdHTTP -> Work::wmWrite - " + IntToStr(AWorkCount));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdHTTP1WorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax)
{
	if (AWorkMode = wmRead) OnTextToLog("IdHTTP -> WorkBegin::wmRead " + IntToStr(AWorkCountMax)); else OnTextToLog("IdHTTP -> WorkBegin::wmWrite - " + IntToStr(AWorkCountMax));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdHTTP1WorkEnd(TObject *ASender, TWorkMode AWorkMode)
{
	if (AWorkMode = wmRead) OnTextToLog("IdHTTP -> WorkEnd::wmRead"); else OnTextToLog("IdHTTP -> WorkEnd::wmWrite");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdHTTP1Status(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText)
{
	OnTextToLog("IdHTTP -> Status: " + AStatusText);
}
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TForm1::GetDataFileName(const System::UnicodeString Name, const System::UnicodeString Ext)
{
	++DataFileNumber;
	return ExtractFilePath(Application->ExeName) + "in_data\\" + Name + "_" + DataFileName + "_" + IntToStr(DataFileNumber) + "." + Ext;
}

void SelSiteList(TStrings * items)
{
	items->Clear();
	items->AddObject("1 - �����������", (TObject*)1);
	items->AddObject("10 - ���������������", (TObject*)10);
	items->AddObject("17 - ����������", (TObject*)17);
	items->AddObject("24 - �����������", (TObject*)24);
	items->AddObject("28 - ��������", (TObject*)28);
	items->AddObject("51 - ������-����������", (TObject*)51);
	items->AddObject("58 - ���-���������", (TObject*)58);
	items->AddObject("61 - �����������", (TObject*)61);
	items->AddObject("63 - ������������", (TObject*)63);
	items->AddObject("76 - ������������", (TObject*)76);
	items->AddObject("80 - ����-���������", (TObject*)80);
	items->AddObject("83 - �������-���������", (TObject*)83);
	items->AddObject("88 - ������������", (TObject*)88);
	items->AddObject("92 - ��������-���������", (TObject*)92);
	items->AddObject("94 - �������������", (TObject*)94);
	items->AddObject("96 - ���������������", (TObject*)96);
}



//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	this->Caption = "������� �������� ���������� ����� � ������";

	sgRubki->Cells[0][0] = "����";
	sgRubki->Cells[1][0] = "������";
	sgRubki->Cells[2][0] = "�����";
	sgRubki->Cells[3][0] = "�����, �";
	sgRubki->Cells[4][0] = "�����������, t�";

	sgRubki->ColWidths[1] = 110;
	sgRubki->ColWidths[2] = 110;
	sgRubki->ColWidths[4] = 90;

	pcData->ActivePageIndex = 0;

	ScrollBox->VertScrollBar->Position = 0;
	pLavel2->Visible = False;
	pLavel3->Visible = False;
	pLavel4->Visible = False;

	TabSheet1->TabVisible = false;
	TabSheet2->TabVisible = false;
//	TabSheet3->TabVisible = false;
	tsCutting->TabVisible = false;
	tsDIRECTION->TabVisible = false;
	tsPUT_GL->TabVisible = false;
	TabSheet6->TabVisible = false;
	tsCutting->Show();
//	PageControl1->

	EKASUI = new WebServiceEKASUI(this);
	EKASUI->setLog(OnTextToLog);
	EKASUI->StartWork();

	// ---------------------

	UnicodeString tmp = "_";

	DataFileNumber = 0;
	DataFileName = DateTimeToStr(Now());
	while (DataFileName.Pos(":") != 0) DataFileName[DataFileName.Pos(":")] = tmp[1];

	LogFileName = DateTimeToStr(Now());
	while (LogFileName.Pos(":") != 0) LogFileName[LogFileName.Pos(":")] = tmp[1];
//	LogFileName = ExtractFilePath(Application->ExeName) + "\\log_" + LogFileName + ".txt";
	LogFileName = ExtractFilePath(Application->ExeName) + "\\log.txt";

	SelSiteList(ComboBox_SD->Items);
	ComboBox_SD->ItemIndex = 0;
	SelSiteList(ComboBox_BPD->Items);
	ComboBox_BPD->ItemIndex = 0;




	TStringList* delim1 = new (TStringList);
	delim1->Delimiter = ';';
	delim1->StrictDelimiter = True;

	DIRECTION_OnlyName = new(TStringList);
	PUT_GL_OnlyName = new(TStringList);

	for (int i = 1; i < mDIRECTIONMemo->Lines->Count; i++) {

		delim1->DelimitedText = mDIRECTIONMemo->Lines->Strings[i];
		int idDIR = StrToInt(delim1->Strings[1]);
		DIRECTION_OnlyName->AddObject(delim1->Strings[2], (TObject*)idDIR);

//		cbDIRECTION->Items->AddObject(delim1->Strings[2], (TObject*)i);
//		int idDIR = StrToInt(delim1->Strings[1]);
//		TObject* tmp = (TObject*)idDIR;
//		DIRECTION_OnlyName->AddObject(delim1->Strings[2] /*+ " " + IntToStr(idDIR)*/, mDIRECTION_OnlyName);
//		DIRECTION_OnlyName->Objects[mDIRECTION_OnlyName->Lines->Count - 1] = mDIRECTION_OnlyName;
	}

	mDIRECTION_OnlyName->Lines->Assign(DIRECTION_OnlyName);
	lbDIRECTION->Items->Assign(DIRECTION_OnlyName);
	eDIRECTION->Items->Assign(DIRECTION_OnlyName);

//	lbDIRECTION->
/*
	delim2->DelimitedText = delim1->Strings[0]; // 402 km 2.9 = 402 km 224.6

	delim3->DelimitedText = delim2->Strings[0];
	String tmp1 = delim3->Strings[0];
	int J = tmp1.Length();
	while (true) {
		if (!((((int)(tmp1[J]) >= 48) && ((int)(tmp1[J]) <= 57)) || ((int)(tmp1[J]) == 46))) tmp1.Delete(J, 1);
		J--;
		if (J <= 0) break;
	};


	TStringList* a;
	a->StrictDelimiter */

}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormDestroy(TObject *Sender)
{
	delete EKASUI;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
	if (EKASUI) {
		int TypeSD;

		switch (ComboBox_SD_2->ItemIndex) {
			case 0: TypeSD = 0; break;
			case 1: TypeSD = 1; break;
			case 2: TypeSD = 2; break;
		}

		EKASUI->GetEtbDm(TypeSD, (int)ComboBox_SD->Items->Objects[ComboBox_SD->ItemIndex]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button9Click(TObject *Sender)
{
	if (EKASUI)	{

		Button9->Tag = 1;
		EKASUI->GetBPD((int)ComboBox_BPD->Items->Objects[ComboBox_BPD->ItemIndex]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	if (EKASUI) {

		tTransactionData* pItem = EKASUI->GetFirstProcessedItem();
		if (pItem)
		{
			TStringList * tmp = new TStringList();
	   //		TMemoryStream * Datatmp = new TMemoryStream();

			switch (pItem->DataType) {
				case aGetEtbDm          : {
											  pItem->ReceivedData->Position = 0;
										   // Datatmp->LoadFromStream(pItem->ReceivedData);
										   // Datatmp->SaveToFile(WorkDir + "SDdata_xml.cmd");
											  UnicodeString WorkDir = ExtractFilePath(Application->ExeName) + "TEMP\\";


											//  pItem->ReceivedData->SaveToFile(WorkDir + "SDdata_xml.cmd");

											  pItem->ReceivedData->SaveToFile(WorkDir + "SDdata_" + IntToStr(pItem->Tag) + ".xml");

											  break;
										  }
				case aGetBPD            : {
											  pItem->ReceivedData->Position = 0;
											  UnicodeString WorkDir = ExtractFilePath(Application->ExeName) + "TEMP\\";
											  pItem->ReceivedData->SaveToFile(WorkDir + "BPD_RRid" + IntToStr(pItem->Tag) + ".zip");

											  if (Button9->Tag == 2) {

												  BPDLoaded++;
												  if (BPDLoaded == 16) {

													  tZipFilesManager* Zips = new tZipFilesManager();
													  UnicodeString NSIVer;
													  if (Zips->Work(WorkDir, WorkDir, WorkDir, &NSIVer)) ShowMessage("OK"); else ShowMessage("ERROR");
													  ShowMessage("NSIVer = " + NSIVer);
													  // �������� zip ����� �� LOCA  + NSIVer
												  }
											  }


											  break;

										 /*	  pItem->ReceivedData->Position = 0;
											  Datatmp->LoadFromStream(pItem->ReceivedData);
											  UnicodeString WorkDir = ExtractFilePath(Application->ExeName) + "TEMP\\";
											  Datatmp->SaveToFile(WorkDir + "BPD_RRid" + IntToStr(pItem->Tag) + ".zip");
											  BPDLoaded++;
											  if (BPDLoaded == 16) {

												  tZipFilesManager* Zips = new tZipFilesManager();
												  UnicodeString NSIVer;
												  if (Zips->Work(WorkDir, WorkDir, WorkDir, &NSIVer)) ShowMessage("OK"); else ShowMessage("ERROR");
												  ShowMessage("NSIVer = " + NSIVer);
												  // �������� zip ����� �� LOCA  + NSIVer
											  }
											  break; */
										  }
				case aGetDefect         : break;
				case aSendVideoIncidents:
				case aSendDefect        : {
											  pItem->ReceivedData->Position = 0;
											  tmp->LoadFromStream(pItem->ReceivedData);
											  OnTextToLog(tmp->Text);
											  UnicodeString WorkDir = ExtractFilePath(Application->ExeName) + "TEMP\\";
											  tmp->SaveToFile(WorkDir + "Send_" + ExtractFileName(pItem->FileName) + IntToStr(pItem->Tag) + ".xml");
											  break;
										  }
							  case aPing: {

											  OnTextToLog("Connection: " + IntToStr(pItem->Tag));
											  tmp->LoadFromStream(pItem->ReceivedData);
											  UnicodeString WorkDir = ExtractFilePath(Application->ExeName) + "TEMP\\";
											  tmp->SaveToFile(WorkDir + "Ping_" + IntToStr((int)GetTickCount()) + ".xml");
											  break;
										  }
							   case aURL: {

											  pItem->ReceivedData->Position = 0;
											  OnTextToLog("URL: " + pItem->FileName);
											  tmp->LoadFromStream(pItem->ReceivedData);
											  UnicodeString WorkDir = ExtractFilePath(Application->ExeName) + "TEMP\\";
											  tmp->SaveToFile(WorkDir + "Ping_" + IntToStr((int)GetTickCount()) + ".xml");
											  OnTextToLog("answer: " + tmp->Text);
											  break;
										  }
			}
			delete tmp;
//			delete Datatmp;
			EKASUI->DeleteFirstProcessedItem();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button10Click(TObject *Sender)
{
	OpenDialog->InitialDir = ExtractFilePath(Application->ExeName) + "out_data\\";
	UnicodeString fn = ExtractFilePath(Application->ExeName) + "out_data\\UZK_test2.xml";
	if (cbSelectFile->Checked) {
		if (OpenDialog->Execute()) fn = OpenDialog->FileName; else return;
	}

	if (EKASUI)	EKASUI->SendDefect(fn, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button11Click(TObject *Sender)
{
	OpenDialog->InitialDir = ExtractFilePath(Application->ExeName) + "out_data\\";
	UnicodeString fn = ExtractFilePath(Application->ExeName) + "out_data\\UZK_test2.xml";
	if (cbSelectFile->Checked) {
		if (OpenDialog->Execute()) fn = OpenDialog->FileName; else return;
	}

	if (EKASUI)	EKASUI->SendVideoIncidents(fn, 0);
}
//---------------------------------------------------------------------------

void TForm1::OnTextToLog(UnicodeString Text)
{

	while (Pos("\r\n", Text, 1) != 0) {

		int pos = Pos("\r\n", Text);
		Text.Delete(pos, 2);
	}

	Text = DateTimeToStr(Now()) + "| " + Text;
	Form1->LogMemo->Lines->Add(Text);
//	Form1->LogMemo->Lines->SaveToFile(Form1->LogFileName);
	Form1->LogMemo->Lines->SaveToFile(Form1->LogFileName);
//	Form1->LogMemo->Lines->SaveToFile("c:\\temp\\log.txt");
}

void __fastcall TForm1::Button12Click(TObject *Sender)
{
	if (EKASUI)	{

		Button9->Tag = 2;
		BPDLoaded = 0;
		for (int RRidx = 0; RRidx < 16; RRidx++) {

			EKASUI->GetBPD(RRid[RRidx]);

		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if (EKASUI)	EKASUI->Ping();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
	EKASUI->EndWork();
	while (!EKASUI->EndWorkOK()) Application->ProcessMessages();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if (EKASUI)	EKASUI->URL(cbURL->Text);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormShow(TObject *Sender)
{
	Panel4Resize(Sender);
//	Form1->LogMemo->Lines->LoadFromFile("c:\\temp\\log.txt");

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	if (cbFileIdx->ItemIndex == 0) EKASUI->SendGis(ExtractFilePath(Application->ExeName) + "gis-test-1.txt", cbUrlIdx->ItemIndex);
	if (cbFileIdx->ItemIndex == 1) EKASUI->SendGis(ExtractFilePath(Application->ExeName) + "gis-test-2.txt", cbUrlIdx->ItemIndex);
	if (cbFileIdx->ItemIndex == 2) EKASUI->SendGis(ExtractFilePath(Application->ExeName) + "gis-test-3.txt", cbUrlIdx->ItemIndex);
	if (cbFileIdx->ItemIndex == 3) EKASUI->SendGis(ExtractFilePath(Application->ExeName) + "gis-test-4.txt", cbUrlIdx->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	std::srand(std::time(NULL));
	TStringList* res = new (TStringList);

	TStringList* delim1 = new (TStringList);
	delim1->Delimiter = ';';
	delim1->StrictDelimiter = True;

	TStringList* delim2 = new (TStringList);
	delim2->Delimiter = '=';
	delim2->StrictDelimiter = True;

	TStringList* delim3 = new (TStringList);
	delim3->Delimiter = 'k';
	delim3->StrictDelimiter = True;

	TStringList* data = new (TStringList);
	TStringList* data2 = new (TStringList);
	TStringList* data3 = NULL;

	res->Add("<?xml version=\"1.0\"?>");
	res->Add("<videocontrol>");
	UnicodeString PathId;
	UnicodeString FileName;

	if (cbFileName->ItemIndex == 0)	{

		data->LoadFromFile(ExtractFilePath(Application->ExeName) + "[494]_2019_09_20_01 [����-2019-09-20-18-00] �������� - ���.��. 1 ��, � 122 �� �� 288 ��, ���� ����� - ����������� 1 ��.csv");
		Memo2->Lines->Assign(data);
		res->Add("<proezd road=\"01\" vagon=\"A20606672\" proezd=\"20190920_180000\" month=\"9\" year=\"2019\" proverka=\"2\">");
		PathId = "110000123027";
		FileName = "2019_09_20_01";

		data2->LoadFromFile(ExtractFilePath(Application->ExeName) + "path1.csv");
		data3 =  new (TStringList);
		data3->LoadFromFile(ExtractFilePath(Application->ExeName) + "path1_2.csv");
		Memo1->Lines->Assign(data2);


	} else {

		data->LoadFromFile(ExtractFilePath(Application->ExeName) + "[494]_2019_09_18_01 [����-2019-09-18-06-05] ���.��. - �������� 2 ��, � 288 �� �� 402 ��, ���� ����� - ������������ 2 ��.csv");
		Memo2->Lines->Assign(data);

		res->Add("<proezd road=\"01\" vagon=\"A20606672\" proezd=\"20190918_060530\" month=\"9\" year=\"2019\" proverka=\"2\">");
		PathId = "110000123030";
		FileName = "2019_09_18_01";

		data2->LoadFromFile(ExtractFilePath(Application->ExeName) + "path2.csv");
		Memo1->Lines->Assign(data2);
		Memo3->Visible;
	}

	cbFileName->Enabled = false;
	Button4->Enabled = false;

// UP_273;������� ���� ��� (�������) - ��������, I;1;15101;I;110000123027;1520;11;376;1449;117
// UP_273;������� ���� ��� (�������) - ��������, II;1;15101;II;110000123030;1520;10;203;1449;117
// <rels left="0" way="A2542574" bkm="11" bm="392.453" bgap="14" bt="27.3" binsulate="0" ekm="11" em="906.903" egap="3" et="28.1" einsulate="0" length="12.5"/>
// <rels left="0" way="A2542574" bkm="11" bm="392.453" bgap="14" bt="27.3" binsulate="0" ekm="11" em="906.903" egap="3" et="28.1" einsulate="0" length="25"/>
// 20190918-06-05

	bool LShift = false;
	bool RShift = false;
	for (int i = 0; i < data->Count; i++) {

		dataItem NewItem;

		FormatSettings.DecimalSeparator = '.';

		// --- �������������� ������ � �������� ---
		delim1->DelimitedText = data->Strings[i];
		delim2->DelimitedText = delim1->Strings[0]; // 402 km 2.9 = 402 km 224.6

		delim3->DelimitedText = delim2->Strings[0];
		String tmp1 = delim3->Strings[0];
		int J = tmp1.Length();
		while (true) {
			if (!((((int)(tmp1[J]) >= 48) && ((int)(tmp1[J]) <= 57)) || ((int)(tmp1[J]) == 46))) tmp1.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		String tmp2 = delim3->Strings[1];
		J = tmp2.Length();
		while (true) {
			if (!((((int)(tmp2[J]) >= 48) && ((int)(tmp2[J]) <= 57)) || ((int)(tmp2[J]) == 46))) tmp2.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		//float val1;
		//float val2;
		TryStrToInt(tmp1, NewItem.startKm);
		TryStrToFloat(tmp2, NewItem.startMetr);
		NewItem.start = NewItem.startKm * 1000 + NewItem.startMetr;

		delim3->DelimitedText = delim2->Strings[1];
		tmp1 = delim3->Strings[0];
		J = tmp1.Length();
		while (true) {
			if (!((((int)(tmp1[J]) >= 48) && ((int)(tmp1[J]) <= 57)) || ((int)(tmp1[J]) == 46))) tmp1.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		tmp2 = delim3->Strings[1];
		J = tmp2.Length();
		while (true) {
			if (!((((int)(tmp2[J]) >= 48) && ((int)(tmp2[J]) <= 57)) || ((int)(tmp2[J]) == 46))) tmp2.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		TryStrToInt(tmp1, NewItem.endKm);
		TryStrToFloat(tmp2, NewItem.endMetr);
		NewItem.end = NewItem.endKm * 1000 + NewItem.endMetr;

		TryStrToFloat(delim1->Strings[1], NewItem.len);

		tmp1 = delim1->Strings[2];
		NewItem.left = (tmp1.Pos("Left") != 0);

		tmp2 = delim1->Strings[3];
		NewItem.inSwitch = (tmp2.Pos("Yes") != 0);

		FormatSettings.DecimalSeparator = ',';
		TryStrToFloat(delim1->Strings[4], NewItem.temperature);


		// "���������"

		TChartShape *Series1 = new TChartShape(Chart1);
		Chart1->AddSeries(Series1);
		Series1->Style = chasRectangle;
//		Series1->Style = chasCube;
		if (NewItem.left) {
			Series1->Y0 = 13  + (int)LShift * 2;
			Series1->Y1 = 20 + (int)LShift * 2;
			LShift = !LShift;

//			Memo1->Lines->Add(Format("%3.1f - %3.1f ", ARRAYOFCONST((NewItem.start, NewItem.end))));

		} else {
			Series1->Y0 = - 13 - (int)RShift * 2;
			Series1->Y1 = - 20 - (int)RShift * 2;
			RShift = !RShift;
//			Series1->Visible = false;
		}
		if (NewItem.inSwitch) Series1->Color = clMaroon;
			else Series1->Color = clHighlight;
		Series1->X0 = NewItem.start;
		Series1->X1 = NewItem.end;
//		Series1->Text->Text = Format("%3.1f (%d)", ARRAYOFCONST((NewItem.len, i)));
		Series1->Text->Text = Format("%3.1f", ARRAYOFCONST((NewItem.len)));
		Series1->Font->Color = clBlack;

		A03Items.push_back(NewItem);
		Series1->Tag = A03Items.size() - 1;
		Series1->OnClick = Series1_Click;

		// �������� "xml"

		res->Add(Format("<rels left=\"%d\" way=\"%s\" bkm=\"%d\" bm=\"%3.1f\" bgap=\"%d\" bt=\"%2.1f\" binsulate=\"%d\" ekm=\"%d\" em=\"%2.1f\" egap=\"%d\" et=\"%2.1f\" einsulate=\"%d\" length=\"%3.3f\"/>",
						ARRAYOFCONST(((int)NewItem.left, PathId, NewItem.startKm, NewItem.startMetr, 2 + (std::rand() % 18), NewItem.temperature, 0, NewItem.endKm, NewItem.endMetr, 2 + (std::rand() % 18), NewItem.temperature, 0, NewItem.len))));

	}

	res->Add("</proezd>");
	res->Add("</videocontrol>");
	res->SaveToFile(ExtractFilePath(Application->ExeName) + FileName + ".xml");



	LShift = false;
	RShift = false;
	FormatSettings.DecimalSeparator = ',';

	for (int i = 0; i < data2->Count; i++) {

		dataItem NewItem;


// ��	��	�	����	����� ��������� �����, �
//261;2;50;0;11,00
//261;9;20;0;11,00
//262;2;25;0;12,50


		// --- �������������� ������ � �������� ---
		delim1->DelimitedText = data2->Strings[i];


		if ((!delim1->Strings[0].IsEmpty()) &&
			(!delim1->Strings[1].IsEmpty()) &&
			(!delim1->Strings[2].IsEmpty()) &&
			(!delim1->Strings[3].IsEmpty()) &&
			(!delim1->Strings[4].IsEmpty())) {

			NewItem.start = StrToInt(delim1->Strings[0]) * 1000 + (StrToInt(delim1->Strings[1]) - 1) * 100  + StrToFloat(delim1->Strings[2]);
			NewItem.len = StrToFloat(delim1->Strings[4]);
			NewItem.end = NewItem.start + NewItem.len;
			NewItem.left = (StrToInt(delim1->Strings[3]) == 0);
			NewItem.inSwitch = false;
			NewItem.temperature = 0;

			RZDItems.push_back(NewItem);


			// "���������"

			TChartShape *Series1 = new TChartShape(Chart1);
			Chart1->AddSeries(Series1);
			Series1->Style = chasRectangle;
	//		Series1->Style = chasCube;
			if (NewItem.left) {
				Series1->Y0 = 2 + (int)LShift * 2;
				Series1->Y1 = 9 + (int)LShift * 2;
				LShift = !LShift;

	//			Memo1->Lines->Add(Format("%3.1f - %3.1f ", ARRAYOFCONST((NewItem.start, NewItem.end))));

			} else {
				Series1->Y0 = - 2 - (int)RShift * 2;
				Series1->Y1 = - 9 - (int)RShift * 2;
				RShift = !RShift;
	//			Series1->Visible = false;
			}
			if (NewItem.inSwitch) Series1->Color = clMaroon;
				else Series1->Color = clRed;
			Series1->X0 = NewItem.start;
			Series1->X1 = NewItem.end;
//			Series1->Text->Text = Format("%3.2f (%d)", ARRAYOFCONST((NewItem.len, i)));
			Series1->Text->Text = Format("%3.2f", ARRAYOFCONST((NewItem.len)));
			Series1->Font->Color = clBlack;

			Series1->Tag = RZDItems.size() - 1;
			Series1->OnClick = Series2_Click;

		}
	}

//SITEID;WAY;    NIT;KM;  PK;PMN;  M1     ;L
//1;110000123027;1;  1017; 3; 39;1017,0239;11,88

	if (data3) {

		Memo3->Lines->Clear();
		Memo3->Visible = false;
		Chart1->LeftAxis->Minimum = - 31;
		Chart1->LeftAxis->Maximum = + 31;
		for (int i = 0; i < data3->Count; i++) {

			dataItem NewItem;

			// --- �������������� ������ � �������� ---
			delim1->DelimitedText = data3->Strings[i];


			if ((!delim1->Strings[2].IsEmpty()) &&
				(!delim1->Strings[6].IsEmpty()) &&
				(!delim1->Strings[7].IsEmpty()) &&
				(StrToFloat(delim1->Strings[6]) >= 109) &&
				(StrToFloat(delim1->Strings[6]) <= 310)) {


				Memo3->Lines->Add(data3->Strings[i]);

				double tmp = StrToFloat(delim1->Strings[6]);
				double tmp1;
				double tmp2;

				tmp2 = modf(tmp, &tmp1);
				tmp2 = tmp2 * 10;

				NewItem.start = (tmp1 + tmp2) * 1000;
				NewItem.len = StrToFloat(delim1->Strings[7]);
				NewItem.end = NewItem.start + NewItem.len;
				NewItem.left = (StrToInt(delim1->Strings[2]) == 0);
				NewItem.inSwitch = false;
				NewItem.temperature = 0;

				RZD2Items.push_back(NewItem);


				// "���������"

				TChartShape *Series1 = new TChartShape(Chart1);
				Chart1->AddSeries(Series1);
				Series1->Style = chasRectangle;
		//		Series1->Style = chasCube;
				if (NewItem.left) {
					Series1->Y0 = 24  +(int)LShift * 2;
					Series1->Y1 = 29 + (int)LShift * 2;
					LShift = !LShift;

		//			Memo1->Lines->Add(Format("%3.1f - %3.1f ", ARRAYOFCONST((NewItem.start, NewItem.end))));

				} else {
					Series1->Y0 = - 24 - (int)RShift * 2;
					Series1->Y1 = - 29 - (int)RShift * 2;
					RShift = !RShift;
		//			Series1->Visible = false;
				}
				if (NewItem.inSwitch) Series1->Color = clLime;
					else Series1->Color = clGreen;
				Series1->X0 = NewItem.start;
				Series1->X1 = NewItem.end;
				Series1->Text->Text = Format("%3.2f", ARRAYOFCONST((NewItem.len)));
				//Series1->Text->Text = Format("%3.2f (%d)", ARRAYOFCONST((NewItem.len, RZD2Items.size() - 1)));
				Series1->Font->Color = clBlack;

				Series1->Tag = RZD2Items.size() - 1;
				Series1->OnClick = Series3_Click;

			}
		}
		Memo3->Visible = true;
	}


//288 km 602.8 = 288 km 627.8;24.991;������;;5,0
//288 km 602.8 = 288 km 627.8;25.052;�����;;7,5

}
//---------------------------------------------------------------------------



void __fastcall TForm1::Chart1GetLegendRect(TCustomChart *Sender, TRect &Rect)
{
//	Memo1->Lines->Add(Format("%d - %df ", ARRAYOFCONST((Rect.Left, Rect.Top))));
}
//---------------------------------------------------------------------------

/*
XML-���� �������� ������ �� ������ ����� � ������� � ����������� ��������
���������� �������:

<?xml version="1.0"?>
<videocontrol>
<proezd road="17" vagon="A22804216" proezd="20190701_123251" month="7" year="2019" proverka="2">
<rels left="0" way="A2542574" bkm="11" bm="392.453" bgap="14" bt="27.3" binsulate="0" ekm="11" em="906.903" egap="3" et="28.1" einsulate="0" length="513.832"/>
<rels left="0" way="A2542574" bkm="11" bm="392.453" bgap="14" bt="27.3" binsulate="0" ekm="11" em="906.903" egap="3" et="28.1" einsulate="0" length="12.5"/>
<rels left="0" way="A2542574" bkm="11" bm="392.453" bgap="14" bt="27.3" binsulate="0" ekm="11" em="906.903" egap="3" et="28.1" einsulate="0" length="25"/>
</proezd>
</videocontrol>

������� ��������:
<rels>
������� ���� �� ����������� �������� ������������ �������
�������� ����, ���������� �������/�������/�������������� ��������
������� ���� �������� ��� ������� � ������� (�� ����� ��
�����), �� �������� ������������� ��������.
�������� �����:
left - (0 - ������ ����, 1 - ����� ����)
way - ������������� �������� ���� ������
road � ��� ������
bkm, bm, ekm, em - ������� �������
length - ����� ������� � ������
bgap, egap - �������� ������� � ������ � � ����� �������
="513.832"
bt, et - ����������� ������� � ������ ������� � � ����� �������
binsulate, einsulate - (������� �������������� ����� � ������ � �����, 1 - �������������, 0 - ���)
vagon � ������������� �������� ����������� ������.
proezd � ���� � ����� �������;
month � �����;
year � ���;
proverka - ��� ��������, ������� ����� ��������� �������� (0 -
�������, 1 - �����������, 2 - �������������� (��������������� ���
������ ������������� � ��������� ��������)
*/



//void __fastcall TForm1::Button5Click(TObject *Sender) // RZD
//{
//
//}
//---------------------------------------------------------------------------

//void __fastcall TForm1::Button6Click(TObject *Sender) // A03
//void  TForm1::Button6Click(TChartSeries* Sender, int ValueIndex, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);{
//
//}
//---------------------------------------------------------------------------

// A03M
void __fastcall TForm1::Series1_Click(TChartSeries *Sender, int ValueIndex, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	int Index = (int)Sender->Tag;
	Memo2->SelStart = Memo2->Perform(EM_LINEINDEX, Index, 0);
	Memo2->SelLength = Memo2->Lines->Strings[Index].Length();
	Label4->Caption = "������: " + IntToStr(Index) + " ";
}
//---------------------------------------------------------------------------

// RZD
void __fastcall TForm1::Series2_Click(TChartSeries *Sender, int ValueIndex, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	int Index = (int)Sender->Tag;
	Memo1->SelStart = Memo1->Perform(EM_LINEINDEX, Index, 0);
	Memo1->SelLength = Memo1->Lines->Strings[Index].Length();
	Label5->Caption = "������: " + IntToStr(Index) + " ";
}
//---------------------------------------------------------------------------

// RZD2
void __fastcall TForm1::Series3_Click(TChartSeries *Sender, int ValueIndex, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	int Index = (int)Sender->Tag;
	Memo3->SelStart = Memo3->Perform(EM_LINEINDEX, Index, 0);
	Memo3->SelLength = Memo3->Lines->Strings[Index].Length();
	Label3->Caption = "������: " + IntToStr(Index) + " ";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Panel4Resize(TObject *Sender)
{
	Panel5->Width = (Panel4->Width - Splitter3->Width * 2) / 3;
	Panel7->Width = (Panel4->Width - Splitter3->Width * 2) / 3;
}
//---------------------------------------------------------------------------



void __fastcall TForm1::bSelectFileClick(TObject *Sender)
{
	if (OpenDialog1->Execute()) {

		lOpenFile->Caption = OpenDialog1->FileName;
		data_ = new (TStringList);
		data_->LoadFromFile(OpenDialog1->FileName, TEncoding::GetEncoding("866"));
		Memo2->Lines->Assign(data_);
		PathId_ = "";
		FileName__ = "Rubku" + DateTimeToStr(Now());


		StartLine_ = - 1;

		for (int i = 0; i < data_->Count; i++) {


/*			if (data_->Strings[i].Pos("�������� ��������� �����:") != 0) {
				proezd_DataTime = data_->Strings[i];
				proezd_DataTime.Delete(1, 32);
				proezd_DataTime.Delete(proezd_DataTime.Pos(";"), 256);
			} */
			if (data_->Strings[i].Pos("����") != 0) {

				UnicodeString tmp = data_->Strings[i];

				while (tmp.Pos(";;") != 0) tmp.Delete(tmp.Pos(";;"), 1);
				while (tmp.Pos("__") != 0) tmp.Delete(tmp.Pos("__"), 1);

				lCuttingFileInfo->Caption = "����������: " + tmp;

				// ��������� ���� �������
				tmp.Delete(1, 6);
				tmp.Delete(tmp.Pos(" ;"), tmp.Length());
				while (tmp.Pos(":") != 0) tmp[tmp.Pos(":")] = '_';
				this->Caption = tmp;
				proezd_DataTime = tmp;
				FileName__ = "[494] " + tmp;

				// �����, ���
				TryStrToInt(tmp.SubString(1, 4), year_);
				TryStrToInt(tmp.SubString(6, 2), month_);


				// ��������� ������ ��
				tmp = data_->Strings[i];
				if (tmp.Pos("��") != 0)
					tmp.Delete(1, tmp.Pos("�� -") - 1);
				if (tmp.Pos("��") != 0)
					tmp.Delete(1, tmp.Pos("�� -") - 1);
				if (tmp.Pos(",") != 0)
					tmp.Delete(tmp.Pos(","), tmp.Length());
				while (tmp.Pos("_") != 0) tmp.Delete(tmp.Pos("_"), 1);
				while (tmp.Pos(" ") != 0) tmp.Delete(tmp.Pos(" "), 1);
				mDebug->Lines->Add(tmp);

				FileName__ = FileName__ + " (" + tmp + ")" + ".xml";
				lCuttingFileName->Caption = "����: " + FileName__;

				mDebug->Lines->Add("data time: " + proezd_DataTime);
				mDebug->Lines->Add("year: " + IntToStr(year_));
				mDebug->Lines->Add("month_: " + IntToStr(month_));

			}

			if (data_->Strings[i].Pos("���������") != 0) {

				StartLine_ = i + 1;
				break;
			}
		}

		//PageControl->ActivePage = tsSelectDir;
		pLavel1->Enabled = False;
		pLavel2->Visible = True;
		pLavel2->Top = pLavel1->Top + pLavel1->Height + 1;
		ScrollBox->VertScrollBar->Position = MaxInt;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::eDIRECTIONChange(TObject *Sender)
{

	if (eDIRECTION->Text.Length() != 0) {

		lbDIRECTION->Items->Clear();
		for (int i = 0; i < mDIRECTION_OnlyName->Lines->Count; i++) {

			if (mDIRECTION_OnlyName->Lines->Strings[i].UpperCase().Pos(eDIRECTION->Text.UpperCase()) != 0)
				lbDIRECTION->Items->AddObject(DIRECTION_OnlyName->Strings[i], DIRECTION_OnlyName->Objects[i]);
		}
	} else lbDIRECTION->Items->Assign(DIRECTION_OnlyName);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::lbDIRECTIONClick(TObject *Sender)
{

	if (lbDIRECTION->ItemIndex != -1) {

		PUT_GL_OnlyName->Clear();

		int selNAPR = (int)lbDIRECTION->Items->Objects[lbDIRECTION->ItemIndex];
//		Label6->Caption = IntToStr(selNAPR);

//		TStringList* delim1 = new (TStringList);
//		delim1->Delimiter = ';';
//		delim1->StrictDelimiter = True;

		System::UnicodeString NAME;
		int NAPR;
		System::UnicodeString IDPUT;
		int StKm;
		int StM;
		int EdKm;
		int EdM;

		for (int i = 1; i < mPUT_GL->Lines->Count; i++) {

//			delim1->DelimitedText = mPUT_GL->Lines->Strings[i];
			DecodePUT_GL_Text(mPUT_GL->Lines->Strings[i], &NAME, &NAPR, &IDPUT, &StKm, &StM, &EdKm, &EdM);



			if (NAPR == selNAPR)
				PUT_GL_OnlyName->AddObject(Format("%s [%d �� %d � - %d �� %d �]", ARRAYOFCONST((NAME, StKm, StM, EdKm, EdM))), (TObject*)i);

	//		cbDIRECTION->Items->AddObject(delim1->Strings[2], (TObject*)i);
	//		int idDIR = StrToInt(delim1->Strings[1]);
	//		TObject* tmp = (TObject*)idDIR;
	//		DIRECTION_OnlyName->AddObject(delim1->Strings[2] /*+ " " + IntToStr(idDIR)*/, mDIRECTION_OnlyName);
	//		DIRECTION_OnlyName->Objects[mDIRECTION_OnlyName->Lines->Count - 1] = mDIRECTION_OnlyName;
		}
		lbPUT_GL->Items->Assign(PUT_GL_OnlyName);


	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::lbPUT_GLClick(TObject *Sender)
{
	if (lbPUT_GL->ItemIndex != -1) {

		System::UnicodeString NAME;
		int NAPR;
		System::UnicodeString IDPUT;
		int StKm;
		int StM;
		int EdKm;
		int EdM;


		DecodePUT_GL_Text(mPUT_GL->Lines->Strings[(int)PUT_GL_OnlyName->Objects[lbPUT_GL->ItemIndex]], &NAME, &NAPR, &IDPUT, &StKm, &StM, &EdKm, &EdM);

//		Label7->Caption = IDPUT;
		bNext1->Enabled = True;
		PathId_ = IDPUT;

	}

}
//---------------------------------------------------------------------------

// ����: 2019:10:06:07:01 ;;����������� ��,   �� - __31__,   �������� - ���.��. 1 ��, � ��:  122 �� ��:  243, ���� �������� ���� - ����������� 1 ��;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;
// ���������;����� (�);�����;� �������;�, �� ;����������� ;;;;;;;;;;;;;

void __fastcall TForm1::bNext1Click(TObject *Sender)
{
	//std::srand(std::time(NULL));

	TStringList* res = new (TStringList);

	TStringList* delim1 = new (TStringList);
	delim1->Delimiter = ';';
	delim1->StrictDelimiter = True;

	TStringList* delim2 = new (TStringList);
	delim2->Delimiter = '=';
	delim2->StrictDelimiter = True;

	TStringList* delim3 = new (TStringList);
	delim3->Delimiter = 'k';
	delim3->StrictDelimiter = True;

  //	TStringList* data2 = new (TStringList);
  //	TStringList* data3 = NULL;

	res->Add("<?xml version=\"1.0\"?>");
	res->Add("<videocontrol>");

//         res->Add("<proezd road=\"01\" vagon=\"A20606672\" proezd=\"20190920_180000\" month=\"9\" year=\"2019\" proverka=\"2\">");
	res->Add(Format("<proezd road=\"01\" vagon=\"A20606672\" proezd=\"%s\" month=\"%d\" year=\"%d\" proverka=\"0\">", ARRAYOFCONST((proezd_DataTime, month_, year_))));


	//data2->LoadFromFile(ExtractFilePath(Application->ExeName) + "path1.csv");
	//data3 =  new (TStringList);
	//data3->LoadFromFile(ExtractFilePath(Application->ExeName) + "path1_2.csv");
	//Memo1->Lines->Assign(data2);

	bSelectFile->Enabled = false;
//		Button4->Enabled = false;

	bool LShift = false;
	bool RShift = false;
	float MAX_crd = 0;
	float MIN_crd = FLT_MAX;

	if (StartLine_ == -1) return;

	for (int i = StartLine_; i < data_->Count; i++) {

		dataItem NewItem;

		FormatSettings.DecimalSeparator = '.';

		// --- �������������� ������ � �������� ---
		delim1->DelimitedText = data_->Strings[i];
		if (delim1->Strings[0].Length() == 0) break;
		delim2->DelimitedText = delim1->Strings[0]; // 402 km 2.9 = 402 km 224.6

		delim3->DelimitedText = delim2->Strings[0];
		String tmp1 = delim3->Strings[0];
		int J = tmp1.Length();
		while (true) {
			if (!((((int)(tmp1[J]) >= 48) && ((int)(tmp1[J]) <= 57)) || ((int)(tmp1[J]) == 46))) tmp1.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		String tmp2 = delim3->Strings[1];
		J = tmp2.Length();
		while (true) {
			if (!((((int)(tmp2[J]) >= 48) && ((int)(tmp2[J]) <= 57)) || ((int)(tmp2[J]) == 46))) tmp2.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		//float val1;
		//float val2;
		TryStrToInt(tmp1, NewItem.startKm);
		TryStrToFloat(tmp2, NewItem.startMetr);
		NewItem.start = NewItem.startKm * 1000 + NewItem.startMetr;

		delim3->DelimitedText = delim2->Strings[1];
		tmp1 = delim3->Strings[0];
		J = tmp1.Length();
		while (true) {
			if (!((((int)(tmp1[J]) >= 48) && ((int)(tmp1[J]) <= 57)) || ((int)(tmp1[J]) == 46))) tmp1.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		tmp2 = delim3->Strings[1];
		J = tmp2.Length();
		while (true) {
			if (!((((int)(tmp2[J]) >= 48) && ((int)(tmp2[J]) <= 57)) || ((int)(tmp2[J]) == 46))) tmp2.Delete(J, 1);
			J--;
			if (J <= 0) break;
		};

		TryStrToInt(tmp1, NewItem.endKm);
		TryStrToFloat(tmp2, NewItem.endMetr);
		NewItem.end = NewItem.endKm * 1000 + NewItem.endMetr;

		TryStrToFloat(delim1->Strings[1], NewItem.len);

		tmp1 = delim1->Strings[2];
		NewItem.left = (tmp1.Pos("�����") != 0);

		tmp2 = delim1->Strings[3];
		NewItem.inSwitch = (tmp2.Pos("��") != 0);

		FormatSettings.DecimalSeparator = ',';
		TryStrToFloat(delim1->Strings[4], NewItem.temperature);


		// "���������"


		TChartShape *Series1 = new TChartShape(ShowChart);
		ShowChart->AddSeries(Series1);
		Series1->Style = chasRectangle;
//		Series1->Style = chasCube;
		if (NewItem.left) {
			Series1->Y0 = 03  + (int)LShift * 2;
			Series1->Y1 = 10 + (int)LShift * 2;
//			LShift = !LShift;

//			Memo1->Lines->Add(Format("%3.1f - %3.1f ", ARRAYOFCONST((NewItem.start, NewItem.end))));

		} else {
			Series1->Y0 = - 03 - (int)RShift * 2;
			Series1->Y1 = - 10 - (int)RShift * 2;
//			RShift = !RShift;
//			Series1->Visible = false;
		}
		if (NewItem.inSwitch) Series1->Color = clMaroon;
			else Series1->Color = clHighlight;
		Series1->X0 = NewItem.start;
		Series1->X1 = NewItem.end;
//		Series1->Text->Text = Format("%3.1f (%d)", ARRAYOFCONST((NewItem.len, i)));
		Series1->Text->Text = Format("%3.1f", ARRAYOFCONST((NewItem.len)));
		Series1->Font->Color = clBlack;
		Series1->OnGetMarkText = ChartShape1GetMarkText;

//		Lines->Add(Format("%3.1f - %3.1f ", ARRAYOFCONST((NewItem.start, NewItem.end))));


		sgRubki->RowCount = sgRubki->RowCount + 1;

		//����:
		UnicodeString tmp = "����: ";
		if (NewItem.left) sgRubki->Cells[0][sgRubki->RowCount - 1] = "�����";
		else sgRubki->Cells[0][sgRubki->RowCount - 1] = "������";
		sgRubki->Cells[1][sgRubki->RowCount - 1] = Format("%d �� %3.1f �", ARRAYOFCONST((NewItem.endKm, NewItem.endMetr)));
		sgRubki->Cells[2][sgRubki->RowCount - 1] = Format("%d �� %3.1f �", ARRAYOFCONST((NewItem.endKm, NewItem.endMetr)));
		sgRubki->Cells[3][sgRubki->RowCount - 1] = Format("%3.3f", ARRAYOFCONST((NewItem.len)));
		sgRubki->Cells[4][sgRubki->RowCount - 1] = Format("%2.1f", ARRAYOFCONST((NewItem.temperature)));


//		mRubkiText->Lines->Add(tmp + Format(" %d �� %3.1f � - %d �� %3.1f �; L = %3.3f t = %2.1f",
//								ARRAYOFCONST((NewItem.startKm, NewItem.startMetr, NewItem.endKm, NewItem.endMetr, NewItem.len, NewItem.temperature))));


		MAX_crd = std::max(MAX_crd, NewItem.start);
		MIN_crd = std::min(MIN_crd, NewItem.start);
		MAX_crd = std::max(MAX_crd, NewItem.end);
		MIN_crd = std::min(MIN_crd, NewItem.end);

//		A03Items.push_back(NewItem);
//		Series1->Tag = A03Items.size() - 1;
//		Series1->OnClick = Series1_Click;

		// �������� "xml"


	  //res->Add(Format("<rels left=\"%d\" way=\"%s\" bkm=\"%d\" bm=\"%3.1f\" bgap=\"%d\" bt=\"%2.1f\" binsulate=\"%d\" ekm=\"%d\" em=\"%2.1f\" egap=\"%d\" et=\"%2.1f\" einsulate=\"%d\" length=\"%3.3f\"/>",
	  //				ARRAYOFCONST(((int)NewItem.left, PathId_, NewItem.startKm, NewItem.startMetr, 2 + (std::rand() % 18), NewItem.temperature, 0, NewItem.endKm, NewItem.endMetr, 2 + (std::rand() % 18), NewItem.temperature, 0, NewItem.len))));
		res->Add(Format("<rels left=\"%d\" way=\"%s\" bkm=\"%d\" bm=\"%3.1f\" bgap=\"\" bt=\"%2.1f\" binsulate=\"%d\" ekm=\"%d\" em=\"%2.1f\" egap=\"\" et=\"%2.1f\" einsulate=\"%d\" length=\"%3.3f\"/>",
						ARRAYOFCONST(((int)NewItem.left, PathId_, NewItem.startKm, NewItem.startMetr, NewItem.temperature, 0, NewItem.endKm, NewItem.endMetr, NewItem.temperature, 0, NewItem.len))));

	}
	sgRubki->FixedRows = 1;

	ChartShape1->Style = chasRectangle;
	ChartShape1->Y0 = 03;
	ChartShape1->Y1 = 10;
	ChartShape1->X0 = MIN_crd - 25;
	ChartShape1->X1 = MAX_crd + 25;
//	ChartShape1->Color = clBlack;
	ChartShape1->Brush->Style = bsClear;

	ChartShape2->Style = chasRectangle;
	ChartShape2->Y0 = -03;
	ChartShape2->Y1 = -10;
	ChartShape2->X0 = MIN_crd - 25;
	ChartShape2->X1 = MAX_crd + 25;
	ChartShape2->Brush->Style = bsClear;


	res->Add("</proezd>");
	res->Add("</videocontrol>");
	res->SaveToFile(ExtractFilePath(Application->ExeName) + "temp.xml");

	//PageControl->ActivePage = tsShow;
	pLavel2->Enabled = False;
	pLavel3->Visible = True;
	pLavel3->Top = pLavel2->Top + pLavel2->Height + 1;
	ScrollBox->VertScrollBar->Position = MaxInt;

//288 km 602.8 = 288 km 627.8;24.991;������;;5,0
//288 km 602.8 = 288 km 627.8;25.052;�����;;7,5

}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button7Click(TObject *Sender)
{
	//PageControl->ActivePage = tsSaveToFtp;
	pLavel3->Enabled = False;
	pLavel4->Visible = True;
	pLavel4->Top = pLavel3->Top + pLavel3->Height + 1;
	ScrollBox->VertScrollBar->Position = MaxInt;
}
//---------------------------------------------------------------------------

bool TForm1::FileExists_(UnicodeString FileName)
{

	bool ResFlag = false;
	if(IdFTP1->Connected()) {

	  IdFTP1->List(NULL, "", true);
	  for (int J = 0; J < IdFTP1->DirectoryListing->Count; J++) {
		if (IdFTP1->DirectoryListing->Items[J]->ItemType != ditDirectory)
			ResFlag = ResFlag || (FileName.UpperCase().Compare(IdFTP1->DirectoryListing->Items[J]->FileName.UpperCase()) == 0);
	  }
	}
	return ResFlag;
}

void __fastcall TForm1::Button13Click(TObject *Sender)
{
//	if(IdFTP1->Connected()) {
//			Label10->Caption = "Connected";
		   //	IdFTP1->ChangeDir("/MyFolder/");
//		   UnicodeString FileName =

//		   IdFTP1->Delete()
//		   IdFTP1->Get()
//			IdFTP1->Put(ExtractFilePath(Application->ExeName) + "temp.xml", FileName_ + ".xml",true);
//	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button14Click(TObject *Sender)
{
	Button14->Enabled = false;
	mFTPLog->Lines->Add("����: " + lCuttingFileName->Caption);
	mFTPLog->Lines->Add("�����������...");
	mFTPLog->Refresh();


	IdFTP1->Username = "LIGHTBPD01";
	IdFTP1->Password = "5sFrcXt5";
//	IdFTP1->Host = "10.23.197.236/INTEGRATION/BPDLight/";
	IdFTP1->Host = "10.23.197.236";
/*
	IdFTP1->Username = "service";
	IdFTP1->Password = "wFso34s";
	IdFTP1->Host = "radioavionica.ru";
*/

	IdFTP1->Port = 21;
	IdFTP1->Passive = True;
	IdFTP1->Connect();
	if(IdFTP1->Connected()) {

//		FileName__ = FileName__ + ".xml";
		mFTPLog->Lines->Add("����������");
		mFTPLog->Refresh();

		while (true) {

			if (!FileExists_(FileName__)) break;

			int St = 5 + FileName__.SubString(6, FileName__.Length()).Pos("[");
			int Ed = 5 + FileName__.SubString(6, FileName__.Length()).Pos("]");
			bool UpdateFlg = false;
			if ((St != 5) && (Ed != 5)) {

				int Count;
				if (TryStrToInt(  FileName__.SubString(St + 1, Ed - 1 - St), Count)) {

					Count++;
					FileName__.Delete(St + 1, Ed - 1 - St);
					FileName__.Insert(IntToStr(Count), St + 1);
					UpdateFlg = true;
				}
			}

			if (!UpdateFlg)
				FileName__ = StringReplace(ExtractFileName(FileName__),ExtractFileExt(FileName__),"", TReplaceFlags()<< rfReplaceAll << rfIgnoreCase) + " [1]" + ExtractFileExt(FileName__);

		}

		mFTPLog->Lines->Add("�������� ����� \"" + FileName__ + "\"...");
		mFTPLog->Refresh();
		IdFTP1->Put(ExtractFilePath(Application->ExeName) + "temp.xml", FileName__,true);
		mFTPLog->Lines->Add("��������...");
		mFTPLog->Refresh();
		TMemoryStream* FTPTest = new TMemoryStream();
		TMemoryStream* LOCALTest = new TMemoryStream();
		IdFTP1->Get(FileName__, FTPTest);
		LOCALTest->LoadFromFile(ExtractFilePath(Application->ExeName) + "temp.xml");
		if ((LOCALTest->Size == FTPTest->Size) && (CompareMem(LOCALTest->Memory, FTPTest->Memory, FTPTest->Size)))
			{
				mFTPLog->Lines->Add("���� �������");
				bExit->Enabled = true;
			}
			else
			{
				mFTPLog->Lines->Add("������ ��������");
				Button14->Enabled = true;
			}

		mFTPLog->Refresh();

		delete FTPTest;
		delete LOCALTest;

	}
	else {

		mFTPLog->Lines->Add("������ �����������");
		Button14->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::pLavel2Resize(TObject *Sender)
{
	Panel15->Width = (float)pLavel2->ClientWidth * (float)0.333;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ChartShape1GetMarkText(TChartSeries *Sender, int ValueIndex,
		  UnicodeString &MarkText)
{
Panel15->Width = (float)pLavel2->ClientWidth * (float)0.333;
//
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ApplicationEvents1Message(tagMSG &Msg, bool &Handled)
{
	if (((int)Msg.message == 522) && (Msg.hwnd != sgRubki->Handle)) {
		if ((int)Msg.wParam < 0) ScrollBox->VertScrollBar->Position = ScrollBox->VertScrollBar->Position + 5;
							else ScrollBox->VertScrollBar->Position = ScrollBox->VertScrollBar->Position - 5;
		Handled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lbPUT_GLDblClick(TObject *Sender)
{
	bNext1Click(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::bExitClick(TObject *Sender)
{
	this->Close();
}
//---------------------------------------------------------------------------

