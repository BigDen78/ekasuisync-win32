object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1058#1077#1089#1090#1086#1074#1072#1103' '#1091#1090#1080#1083#1080#1090#1072' '#1088#1072#1073#1086#1090#1099' '#1089' '#1045#1050#1040#1057#1059#1048' ('#1074#1077#1088#1089#1080#1103' 1.0)'
  ClientHeight = 764
  ClientWidth = 974
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 974
    Height = 764
    ActivePage = tsCutting
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1056#1072#1079#1085#1086#1045
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 966
        Height = 141
        Align = alTop
        TabOrder = 0
        object Bevel1: TBevel
          Left = 285
          Top = 4
          Width = 4
          Height = 133
        end
        object Bevel2: TBevel
          Left = 139
          Top = 4
          Width = 4
          Height = 133
        end
        object Bevel3: TBevel
          Left = 464
          Top = 2
          Width = 4
          Height = 133
          Visible = False
        end
        object Bevel4: TBevel
          Left = 614
          Top = 2
          Width = 4
          Height = 133
          Visible = False
        end
        object Label1: TLabel
          Left = 639
          Top = 63
          Width = 17
          Height = 13
          Caption = 'Url:'
        end
        object Label2: TLabel
          Left = 639
          Top = 90
          Width = 20
          Height = 13
          Caption = 'File:'
        end
        object ComboBox_SD: TComboBox
          Left = 10
          Top = 77
          Width = 117
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '1 - '#1054#1082#1090#1103#1073#1088#1100#1089#1082#1072#1103
          Items.Strings = (
            '1 - '#1054#1082#1090#1103#1073#1088#1100#1089#1082#1072#1103
            '17 - '#1052#1086#1089#1082#1086#1074#1089#1082#1072#1103
            '24 - '#1043#1086#1088#1100#1082#1086#1074#1089#1082#1072#1103)
        end
        object ComboBox_SD_2: TComboBox
          Left = 10
          Top = 46
          Width = 117
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '0 - '#1052#1086#1073#1080#1083#1100#1085#1099#1077
          Items.Strings = (
            '0 - '#1052#1086#1073#1080#1083#1100#1085#1099#1077
            '1 - '#1057#1098#1077#1084#1085#1099#1077
            '2 - '#1057#1098#1077#1084#1085#1099#1077' '#1080' '#1084#1086#1073#1080#1083#1100#1085#1099#1077)
        end
        object ComboBox_BPD: TComboBox
          Left = 155
          Top = 46
          Width = 117
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 2
          Text = '1 - '#1054#1082#1090#1103#1073#1088#1100#1089#1082#1072#1103
          Items.Strings = (
            '1 - '#1054#1082#1090#1103#1073#1088#1100#1089#1082#1072#1103
            '17 - '#1052#1086#1089#1082#1086#1074#1089#1082#1072#1103
            '24 - '#1043#1086#1088#1100#1082#1086#1074#1089#1082#1072#1103)
        end
        object ComboBox1: TComboBox
          Left = 10
          Top = 108
          Width = 117
          Height = 21
          Style = csDropDownList
          Enabled = False
          ItemIndex = 0
          TabOrder = 3
          Text = 'Doroga'
          Visible = False
          Items.Strings = (
            'Doroga'
            'SiteId')
        end
        object Button8: TButton
          Left = 10
          Top = 14
          Width = 117
          Height = 21
          Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1057#1044' ='
          TabOrder = 4
          OnClick = Button8Click
        end
        object Button9: TButton
          Left = 155
          Top = 14
          Width = 117
          Height = 21
          Caption = 'Get BPD ='
          TabOrder = 5
          OnClick = Button9Click
        end
        object Button10: TButton
          Left = 302
          Top = 14
          Width = 145
          Height = 21
          Caption = 'Send US A03 Defect ='
          TabOrder = 6
          OnClick = Button10Click
        end
        object Button11: TButton
          Left = 302
          Top = 46
          Width = 145
          Height = 21
          Caption = 'Send Video A03 Defect  ='
          TabOrder = 7
          OnClick = Button11Click
        end
        object Button12: TButton
          Left = 482
          Top = 14
          Width = 119
          Height = 21
          Caption = 'Get all BPD '
          TabOrder = 8
          OnClick = Button12Click
        end
        object Button1: TButton
          Left = 482
          Top = 46
          Width = 119
          Height = 21
          Caption = 'Test Connection'
          TabOrder = 9
          OnClick = Button1Click
        end
        object cbURL: TComboBox
          Left = 760
          Top = 29
          Width = 137
          Height = 21
          TabOrder = 10
          Text = 'http://10.23.197.34'
          Visible = False
          Items.Strings = (
            'http://10.23.197.34'
            'http://10.23.197.34:7080'
            'http://10.23.197.34:7080/services/'
            'http://10.23.197.34:7080/services/GetEtbDm'
            'http://10.23.197.34:7080/services/getLightBPD'
            'http://10.23.197.34:7080/services/GetDefect'
            'http://10.23.197.34:7080/services/VideoIncidents'
            'http://10.23.197.34:7080/services/SendDefect'
            'http://10.23.197.34:7080/services/GetDefect')
        end
        object Button2: TButton
          Left = 760
          Top = 59
          Width = 75
          Height = 21
          Caption = 'Test'
          TabOrder = 11
          Visible = False
          OnClick = Button2Click
        end
        object cbSelectFile: TCheckBox
          Left = 320
          Top = 79
          Width = 97
          Height = 17
          Caption = 'Select File'
          TabOrder = 12
        end
        object cbUrlIdx: TComboBox
          Left = 662
          Top = 59
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 13
          Text = '1'
          Items.Strings = (
            '1'
            '2'
            '3')
        end
        object cbFileIdx: TComboBox
          Left = 662
          Top = 86
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 14
          Text = '1'
          Items.Strings = (
            '1'
            '2'
            '3'
            '4')
        end
        object Button3: TButton
          Left = 639
          Top = 28
          Width = 75
          Height = 25
          Caption = 'Gis-Test'
          TabOrder = 15
          OnClick = Button3Click
        end
      end
      object LogMemo: TMemo
        Left = 0
        Top = 141
        Width = 966
        Height = 595
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1091#1073#1082#1080
      Enabled = False
      ImageIndex = 1
      object Splitter2: TSplitter
        Left = 0
        Top = 573
        Width = 966
        Height = 8
        Cursor = crVSplit
        Align = alBottom
        ExplicitLeft = 8
        ExplicitTop = 485
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 966
        Height = 41
        Align = alTop
        TabOrder = 0
        object Button4: TButton
          Left = 6
          Top = 7
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button4Click
        end
        object cbFileName: TComboBox
          Left = 104
          Top = 9
          Width = 601
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = 
            '2019-09-20 [494] '#1052#1091#1088#1084#1072#1085#1089#1082' - '#1057#1055#1041'.'#1043#1051'. 1 '#1075#1083', '#1089' 122 '#1082#1084' '#1087#1086' 288 '#1082#1084', '#1087#1091 +
            #1090#1100' '#1057#1074#1080#1088#1100' - '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081' 1 '#1075#1083' '
          Items.Strings = (
            
              '2019-09-20 [494] '#1052#1091#1088#1084#1072#1085#1089#1082' - '#1057#1055#1041'.'#1043#1051'. 1 '#1075#1083', '#1089' 122 '#1082#1084' '#1087#1086' 288 '#1082#1084', '#1087#1091 +
              #1090#1100' '#1057#1074#1080#1088#1100' - '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081' 1 '#1075#1083' '
            
              '2019-09-18 [494] '#1057#1055#1041'.'#1043#1051'. - '#1052#1091#1088#1084#1072#1085#1089#1082' 2 '#1075#1083', '#1089' 288 '#1082#1084' '#1087#1086' 402 '#1082#1084', '#1087#1091 +
              #1090#1100' '#1057#1074#1080#1088#1100' - '#1055#1077#1090#1088#1086#1079#1072#1074#1086#1076#1089#1082' 2 '#1075#1083)
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 41
        Width = 966
        Height = 532
        Align = alClient
        TabOrder = 1
        object ScrollBar1: TScrollBar
          Left = 1
          Top = 514
          Width = 964
          Height = 17
          Align = alBottom
          PageSize = 0
          TabOrder = 0
          Visible = False
        end
        object Chart1: TChart
          Left = 1
          Top = 1
          Width = 964
          Height = 513
          AllowPanning = pmHorizontal
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          OnGetLegendRect = Chart1GetLegendRect
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Automatic = False
          LeftAxis.AutomaticMaximum = False
          LeftAxis.AutomaticMinimum = False
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Maximum = 30.000000000000000000
          LeftAxis.Minimum = -30.000000000000000000
          LeftAxis.Visible = False
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Direction = tzdHorizontal
          Zoom.Pen.Mode = pmNotXor
          Align = alClient
          Color = clGray
          TabOrder = 1
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object Series1_: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clWhite
            OnClick = Series1_Click
            Style = chasCube
            X0 = 5.000000000000000000
            X1 = 10.000000000000000000
            Y0 = 20.000000000000000000
            Y1 = 10.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000000000000000144000000000000034400000000000002440000000
              0000002440}
          end
          object Series2_: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clWhite
            OnClick = Series2_Click
            Style = chasCube
            X0 = 5.000000000000000000
            X1 = 60.000000000000000000
            Y0 = 20.000000000000000000
            Y1 = 10.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000000000000000144000000000000034400000000000004E40000000
              0000002440}
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 581
        Width = 966
        Height = 155
        Align = alBottom
        TabOrder = 2
        OnResize = Panel4Resize
        object Splitter1: TSplitter
          Left = 305
          Top = 1
          Width = 8
          Height = 153
          ExplicitLeft = 441
          ExplicitTop = -2
        end
        object Splitter3: TSplitter
          Left = 649
          Top = 1
          Width = 8
          Height = 153
          ExplicitLeft = 365
          ExplicitTop = 6
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 304
          Height = 153
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 304
            Height = 27
            Align = alTop
            Caption = #1054#1090' '#1042#1077#1088#1073#1099
            TabOrder = 0
            object Pie1: TPie
              Left = 0
              Top = 1
              Width = 25
              Height = 25
              Angles.StartAngle = 180
              Angles.EndAngle = 180
              Brush.Color = clLime
              Pen.Style = psClear
            end
            object Label3: TLabel
              Left = 299
              Top = 1
              Width = 4
              Height = 25
              Align = alRight
              Caption = '-'
              Layout = tlCenter
              ExplicitHeight = 13
            end
          end
          object Memo3: TMemo
            Left = 0
            Top = 27
            Width = 304
            Height = 126
            Align = alClient
            HideSelection = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
        object Panel7: TPanel
          Left = 313
          Top = 1
          Width = 336
          Height = 153
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 336
            Height = 27
            Align = alTop
            Caption = #1040#1074#1080#1082#1086#1085' - 494'
            TabOrder = 0
            object Pie2: TPie
              Left = 0
              Top = 1
              Width = 25
              Height = 25
              Angles.StartAngle = 180
              Angles.EndAngle = 180
              Brush.Color = clHighlight
              Pen.Style = psClear
            end
            object Label4: TLabel
              Left = 331
              Top = 1
              Width = 4
              Height = 25
              Align = alRight
              Caption = '-'
              Layout = tlCenter
              ExplicitHeight = 13
            end
          end
          object Memo2: TMemo
            Left = 0
            Top = 27
            Width = 336
            Height = 126
            Align = alClient
            HideSelection = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
        object Panel9: TPanel
          Left = 657
          Top = 1
          Width = 308
          Height = 153
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 308
            Height = 27
            Align = alTop
            Caption = #1054#1050#1058' '#1078'/'#1076
            TabOrder = 0
            object Pie3: TPie
              Left = 0
              Top = 1
              Width = 25
              Height = 25
              Angles.StartAngle = 180
              Angles.EndAngle = 180
              Brush.Color = clRed
              Pen.Style = psClear
            end
            object Label5: TLabel
              Left = 303
              Top = 1
              Width = 4
              Height = 25
              Align = alRight
              Caption = '-'
              Layout = tlCenter
              ExplicitHeight = 13
            end
          end
          object Memo1: TMemo
            Left = 0
            Top = 27
            Width = 308
            Height = 126
            Align = alClient
            HideSelection = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
      end
    end
    object tsCutting: TTabSheet
      BorderWidth = 2
      Caption = #1056#1091#1073#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 3
      ParentFont = False
      object ScrollBox: TScrollBox
        Left = 0
        Top = 0
        Width = 962
        Height = 732
        VertScrollBar.Position = 316
        VertScrollBar.Tracking = True
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object pLavel2: TPanel
          Left = 0
          Top = -268
          Width = 945
          Height = 412
          Align = alTop
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnResize = pLavel2Resize
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 945
            Height = 100
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object lCuttingFileInfo: TLabel
              Left = 0
              Top = 0
              Width = 945
              Height = 17
              Align = alTop
              AutoSize = False
              Caption = 'Label1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 11
              ExplicitWidth = 941
            end
            object lText2: TLabel
              Left = 0
              Top = 83
              Width = 945
              Height = 17
              Align = alBottom
              AutoSize = False
              Caption = '3. '#1047#1072#1076#1072#1081#1090#1077' '#1085#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1082#1086#1085#1090#1088#1086#1083#1103' '#1080' '#1087#1091#1090#1100':'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitTop = 64
              ExplicitWidth = 941
            end
            object lText5: TLabel
              Left = 0
              Top = 17
              Width = 945
              Height = 23
              Align = alTop
              AutoSize = False
              Caption = '2. '#1057#1088#1077#1076#1090#1074#1086' '#1076#1080#1072#1075#1085#1086#1089#1090#1080#1082#1080
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Layout = tlBottom
              ExplicitTop = 34
            end
            object lSDText1: TLabel
              Left = 0
              Top = 40
              Width = 945
              Height = 17
              Align = alTop
              AutoSize = False
              Caption = #1042#1072#1075#1086#1085'-'#1076#1077#1092#1077#1082#1090#1086#1089#1082#1086#1087' '#8470' '#1055#1057'-494 (A20606672)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 4
              ExplicitTop = 80
            end
          end
          object Panel14: TPanel
            Left = 0
            Top = 371
            Width = 945
            Height = 41
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object Panel11: TPanel
              Left = 859
              Top = 0
              Width = 86
              Height = 41
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object bNext1: TButton
                Left = 0
                Top = 7
                Width = 75
                Height = 25
                Caption = #1044#1072#1083#1100#1096#1077'...'
                Enabled = False
                TabOrder = 0
                OnClick = bNext1Click
              end
            end
          end
          object Panel15: TPanel
            Left = 0
            Top = 100
            Width = 473
            Height = 271
            Align = alLeft
            BevelOuter = bvNone
            Caption = 'Panel15'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object lText3: TLabel
              Left = 0
              Top = 0
              Width = 473
              Height = 13
              Align = alTop
              Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077':'
              ExplicitWidth = 71
            end
            object eDIRECTION: TComboBox
              Left = 0
              Top = 13
              Width = 473
              Height = 21
              Align = alTop
              TabOrder = 0
              OnChange = eDIRECTIONChange
            end
            object lbDIRECTION: TListBox
              Left = 0
              Top = 39
              Width = 473
              Height = 232
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ItemHeight = 13
              ParentFont = False
              TabOrder = 1
              OnClick = lbDIRECTIONClick
            end
            object Panel19: TPanel
              Left = 0
              Top = 34
              Width = 473
              Height = 5
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 2
            end
          end
          object Panel16: TPanel
            Left = 473
            Top = 100
            Width = 472
            Height = 271
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 3
            object lbPUT_GL: TListBox
              Left = 0
              Top = 39
              Width = 472
              Height = 232
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
              OnClick = lbPUT_GLClick
              OnDblClick = lbPUT_GLDblClick
            end
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 472
              Height = 39
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object lText4: TLabel
                Left = 0
                Top = 0
                Width = 472
                Height = 13
                Align = alTop
                Caption = #1055#1091#1090#1100':'
                ExplicitWidth = 29
              end
            end
          end
        end
        object pLavel1: TPanel
          Left = 0
          Top = -316
          Width = 945
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object lText1: TLabel
            Left = 2
            Top = 2
            Width = 318
            Height = 44
            Align = alLeft
            Caption = '1. '#1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1088#1091#1073#1086#1082' (.csv) '#1076#1083#1103' '#1074#1099#1075#1088#1091#1079#1082#1080' '#1074' '#1045#1050#1040#1057#1059#1048
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitHeight = 13
          end
          object lOpenFile: TLabel
            Left = 85
            Top = 24
            Width = 4
            Height = 13
            Caption = '-'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object bSelectFile: TButton
            Left = 2
            Top = 18
            Width = 75
            Height = 25
            Caption = #1060#1072#1081#1083'...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = bSelectFileClick
          end
        end
        object pLavel3: TPanel
          Left = 0
          Top = 144
          Width = 945
          Height = 339
          Align = alTop
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          object Label6: TLabel
            Left = 0
            Top = 0
            Width = 945
            Height = 17
            Align = alTop
            AutoSize = False
            Caption = '4. '#1044#1072#1085#1085#1099#1077' :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Panel12: TPanel
            Left = 0
            Top = 298
            Width = 945
            Height = 41
            Align = alBottom
            TabOrder = 0
            object Panel18: TPanel
              Left = 858
              Top = 1
              Width = 86
              Height = 39
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object Button7: TButton
                Left = 0
                Top = 6
                Width = 75
                Height = 25
                Caption = #1044#1072#1083#1100#1096#1077'...'
                TabOrder = 0
                OnClick = Button7Click
              end
            end
          end
          object pcData: TPageControl
            Left = 0
            Top = 17
            Width = 945
            Height = 281
            ActivePage = tsText
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object tsText: TTabSheet
              Caption = #1058#1072#1073#1083#1080#1095#1085#1086#1077' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              object sgRubki: TStringGrid
                Left = 0
                Top = 0
                Width = 937
                Height = 253
                Align = alClient
                FixedCols = 0
                RowCount = 1
                FixedRows = 0
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect, goThumbTracking]
                ParentFont = False
                ScrollBars = ssVertical
                TabOrder = 0
              end
            end
            object tsGrapfics: TTabSheet
              Caption = #1043#1088#1072#1092#1080#1095#1077#1089#1082#1086#1077' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ImageIndex = 1
              ParentFont = False
              object ShowChart: TChart
                Left = 0
                Top = 0
                Width = 937
                Height = 253
                AllowPanning = pmHorizontal
                Legend.Visible = False
                Title.Text.Strings = (
                  'TChart')
                Title.Visible = False
                OnGetLegendRect = Chart1GetLegendRect
                BottomAxis.LabelsFormat.TextAlignment = taCenter
                DepthAxis.LabelsFormat.TextAlignment = taCenter
                DepthTopAxis.LabelsFormat.TextAlignment = taCenter
                LeftAxis.Automatic = False
                LeftAxis.AutomaticMaximum = False
                LeftAxis.AutomaticMinimum = False
                LeftAxis.LabelsFormat.TextAlignment = taCenter
                LeftAxis.Maximum = 15.000000000000000000
                LeftAxis.Minimum = -15.000000000000000000
                LeftAxis.Visible = False
                RightAxis.LabelsFormat.TextAlignment = taCenter
                TopAxis.LabelsFormat.TextAlignment = taCenter
                View3D = False
                Zoom.Direction = tzdHorizontal
                Zoom.Pen.Mode = pmNotXor
                Align = alClient
                Color = clGray
                TabOrder = 0
                DefaultCanvas = 'TGDIPlusCanvas'
                ColorPaletteIndex = 13
                object ChartShape1: TChartShape
                  Marks.Visible = False
                  SeriesColor = clWhite
                  OnClick = Series1_Click
                  OnGetMarkText = ChartShape1GetMarkText
                  Pen.SmallSpace = 1
                  Style = chasRectangle
                  X0 = 5.000000000000000000
                  X1 = 10.000000000000000000
                  Y0 = 20.000000000000000000
                  Y1 = 10.000000000000000000
                  XValues.Name = 'X'
                  XValues.Order = loAscending
                  YValues.Name = 'Y'
                  YValues.Order = loNone
                  Data = {
                    0102000000000000000000144000000000000034400000000000002440000000
                    0000002440}
                end
                object ChartShape2: TChartShape
                  Marks.Visible = False
                  SeriesColor = clWhite
                  OnClick = Series2_Click
                  Style = chasCube
                  X0 = 5.000000000000000000
                  X1 = 60.000000000000000000
                  Y0 = 20.000000000000000000
                  Y1 = 10.000000000000000000
                  XValues.Name = 'X'
                  XValues.Order = loAscending
                  YValues.Name = 'Y'
                  YValues.Order = loNone
                  Data = {
                    0102000000000000000000144000000000000034400000000000004E40000000
                    0000002440}
                end
              end
            end
          end
        end
        object pLavel4: TPanel
          Left = 0
          Top = 483
          Width = 945
          Height = 249
          Align = alTop
          TabOrder = 3
          object Label7: TLabel
            Left = 1
            Top = 1
            Width = 943
            Height = 17
            Align = alTop
            AutoSize = False
            Caption = '5. '#1042#1099#1079#1088#1091#1079#1082#1072' '#1074' '#1045#1050#1040#1057#1059#1048
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 2
            ExplicitTop = 9
          end
          object lCuttingFileName: TLabel
            Left = 1
            Top = 18
            Width = 943
            Height = 17
            Align = alTop
            AutoSize = False
            Caption = 'Label1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            Visible = False
            ExplicitLeft = 2
            ExplicitTop = 32
          end
          object Button14: TButton
            Left = 4
            Top = 20
            Width = 75
            Height = 25
            Caption = #1057#1090#1072#1088#1090
            TabOrder = 0
            OnClick = Button14Click
          end
          object mFTPLog: TMemo
            Left = 1
            Top = 51
            Width = 943
            Height = 156
            Align = alBottom
            TabOrder = 1
          end
          object Panel20: TPanel
            Left = 1
            Top = 207
            Width = 943
            Height = 41
            Align = alBottom
            TabOrder = 2
            object Panel21: TPanel
              Left = 856
              Top = 1
              Width = 86
              Height = 39
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object bExit: TButton
                Left = 0
                Top = 5
                Width = 75
                Height = 25
                Caption = #1042#1099#1093#1086#1076
                Enabled = False
                TabOrder = 0
                OnClick = bExitClick
              end
            end
          end
        end
      end
    end
    object tsDIRECTION: TTabSheet
      Caption = 'DIRECTION'
      ImageIndex = 1
      object mDIRECTIONMemo: TMemo
        Left = 0
        Top = 0
        Width = 966
        Height = 736
        Align = alClient
        Lines.Strings = (
          'NDOR;NAPR;NAME;IDNAPR'
          '1;15146;'#1041#1055' 1256 - '#1040#1087#1072#1090#1080#1090#1099' '#1087#1088'/'#1086#1090#1087'('#1095#1077#1088#1077#1079' '#1040#1087#1072#1090#1080#1090#1099' '#1089#1086#1088#1090');UP_1137'
          '1;15263;'#1055#1088#1077#1076#1087#1086#1088#1090#1086#1074#1072#1103' - '#1040#1074#1090#1086#1074#1086';UP_1143'
          '1;15239;'#1044#1072#1095#1072' '#1044#1086#1083#1075#1086#1088#1091#1082#1086#1074#1072' - '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II;UP_1157'
          '1;14643;'#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' '#1090#1086#1074'. '#1052#1086#1089#1082' - '#1043#1083#1091#1093#1086#1086#1079#1077#1088#1089#1082#1072#1103';UP_1158'
          '1;15135;'#1050#1086#1095#1082#1086#1084#1072' - '#1051#1077#1076#1084#1086#1079#1077#1088#1086' II;UP_1159'
          '1;11531;'#1042#1077#1090#1074#1100' 1 '#1091#1079#1083#1072' '#1055#1086#1074#1072#1088#1086#1074#1086';UP_116'
          '1;14642;'#1057#1083#1072#1074#1103#1085#1082#1072' - '#1056#1099#1073#1072#1094#1082#1086#1077';UP_1160'
          '1;11532;'#1042#1077#1090#1074#1080' 2 '#1080' 5 '#1091#1079#1083#1072' '#1055#1086#1074#1072#1088#1086#1074#1086';UP_117'
          '1;14835;'#1042#1077#1090#1074#1100' '#1091#1079#1083#1072' '#1050#1086#1090#1083#1099';UP_1183'
          '1;15201;'#1051#1086#1089#1077#1074#1086' I - '#1050#1072#1084#1077#1085#1085#1086#1075#1086#1088#1089#1082';UP_1210'
          '1;16301;'#1051#1080#1093#1086#1089#1083#1072#1074#1083#1100'-'#1042#1103#1079#1100#1084#1072';UP_179'
          '1;10431;'#1053#1080#1082#1086#1083#1072#1077#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100';UP_20'
          '1;10936;'#1042#1077#1090#1074#1100' N 1 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072';UP_217'
          '1;10937;'#1042#1077#1090#1074#1100' N 3 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072';UP_218'
          '1;10938;'#1042#1077#1090#1074#1100' N 4 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072';UP_219'
          '1;10939;'#1047#1077#1084#1094#1099'-'#1046#1072#1088#1082#1086#1074#1089#1082#1080#1081';UP_220'
          '1;11002;'#1052#1075#1072'-'#1054#1074#1080#1085#1080#1097#1077';UP_221'
          '1;11032;'#1050#1072#1083#1103#1079#1080#1085'-'#1059#1075#1083#1080#1095';UP_222'
          '1;11033;'#1054#1074#1080#1085#1080#1097#1077'-'#1042#1077#1089#1100#1077#1075#1086#1085#1089#1082';UP_223'
          '1;11034;'#1050#1080#1088#1080#1096#1080'-'#1040#1085#1076#1088#1077#1077#1074#1086';UP_224'
          '1;11035;'#1050#1080#1088#1080#1096#1080'-'#1048#1088#1089#1072';UP_225'
          '1;14601;'#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1052#1086#1089#1082#1074#1072';UP_226'
          '1;14631;'#1057#1055#1073' '#1090#1086#1074' '#1052#1086#1089#1082'.- '#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103';UP_228'
          '1;14632;'#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'. - '#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103';UP_229'
          '1;14634;'#1057#1072#1073#1083#1080#1085#1086'-'#1055#1086#1089#1090' 22 '#1082#1084';UP_230'
          '1;14635;'#1058#1086#1089#1085#1086'-'#1064#1072#1087#1082#1080';UP_231'
          '1;14636;'#1058#1086#1088#1092#1103#1085#1086#1077'-'#1063#1091#1076#1086#1074#1086';UP_232'
          '1;14637;'#1059#1075#1083#1086#1074#1082#1072'-'#1041#1086#1088#1086#1074#1080#1095#1080';UP_233'
          '1;14638;'#1042#1077#1090#1074#1100' N 5 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_234'
          '1;14639;'#1064#1083#1102#1079'-'#1042#1080#1085#1086#1082#1086#1083#1099';UP_235'
          '1;14640;'#1044#1086#1088#1086#1096#1080#1093#1072'-'#1042#1072#1089#1080#1083#1100#1077#1074#1089#1082#1080#1081' '#1052#1086#1093';UP_236'
          '1;14641;'#1050#1086#1085#1072#1082#1086#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100';UP_237'
          '1;14645;'#1042#1077#1090#1074#1100' N 2 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_238'
          '1;14646;'#1042#1077#1090#1074#1100' N 7 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_239'
          '1;14701;'#1057#1055#1073' '#1060#1080#1085#1083'.-'#1051#1091#1078#1072#1081#1082#1072'- '#1075#1086#1089#1075#1088#1072#1085#1080#1094#1072';UP_240'
          '1;14702;'#1051#1072#1085#1089#1082#1072#1103'-'#1041#1077#1083#1086#1086#1089#1090#1088#1086#1074';UP_241'
          '1;14703;'#1047#1077#1083#1077#1085#1086#1075#1086#1088#1089#1082'-'#1042#1099#1073#1086#1088#1075';UP_242'
          '1;14730;'#1055#1080#1093#1090#1086#1074#1086#1077'-'#1042#1099#1089#1086#1094#1082';UP_243'
          '1;14731;'#1042#1099#1073#1086#1088#1075'-'#1042#1077#1097#1077#1074#1086';UP_244'
          '1;14732;'#1042#1099#1073#1086#1088#1075'-'#1058#1072#1084#1084#1080#1089#1091#1086';UP_245'
          '1;14733;'#1042#1099#1073#1086#1088#1075'-'#1090#1086#1074'.-'#1042#1099#1073#1086#1088#1075'-'#1087#1086#1088#1090';UP_246'
          '1;14801;'#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1053#1072#1088#1074#1072';UP_247'
          '1;14802;'#1051#1080#1075#1086#1074#1086'-'#1042#1077#1081#1084#1072#1088#1085';UP_248'
          '1;14830;'#1051#1077#1073#1103#1078#1100#1077'-'#1050#1088#1072#1089#1085#1086#1092#1083#1086#1090#1089#1082';UP_249'
          '1;14831;'#1050#1086#1090#1083#1099'-'#1059#1089#1090#1100'-'#1051#1091#1075#1072';UP_250'
          '1;14832;'#1043#1076#1086#1074'-'#1042#1077#1081#1084#1072#1088#1085';UP_251'
          '1;14834;'#1050#1086#1089#1082#1086#1083#1086#1074#1086'-'#1051#1091#1078#1089#1082#1072#1103';UP_252'
          '1;14901;'#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1056#1077#1079#1077#1082#1085#1077';UP_253'
          '1;14932;'#1057#1087#1073' '#1042#1072#1088#1096'.-'#1062#1074#1077#1090#1086#1095#1085#1072#1103';UP_256'
          '1;14933;'#1055#1088#1077#1076#1087#1086#1088#1090#1086#1074#1072#1103'-'#1051#1080#1075#1086#1074#1086';UP_257'
          '1;14934;'#1064#1086#1089#1089#1077#1081#1085#1072#1103'-'#1057#1088#1077#1076#1085#1077'-'#1056#1086#1075#1072#1090#1089#1082#1072#1103';UP_258'
          '1;14935;'#1043#1072#1090#1095#1080#1085#1072'-'#1042#1072#1088#1096'.-'#1043#1072#1090#1095#1080#1085#1072'-'#1041#1072#1083#1090'.;UP_259'
          '1;14936;'#1043#1072#1090#1095#1080#1085#1072' '#1042#1072#1088#1096'.'#1041' - '#1060#1088#1077#1079#1077#1088#1085#1099#1081';UP_260'
          '1;14937;'#1043#1072#1090#1095#1080#1085#1072' '#1042#1072#1088#1096'.'#1041' - '#1043#1072#1090#1095#1080#1085#1072' '#1090#1086#1074'. '#1041';UP_261'
          '1;14944;'#1043#1086#1089'.'#1075#1088'. '#1089' '#1051#1072#1090#1074#1080#1077#1081' '#1055#1091#1088#1074#1084#1072#1083#1072'-'#1055#1099#1090#1072#1083#1086#1074#1086';UP_262'
          '1;15001;'#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1042#1080#1090#1077#1073#1089#1082';UP_263'
          '1;15030;'#1057#1055#1073'.'#1089#1086#1088#1090'.'#1042#1080#1090'.-'#1057#1055#1073'.'#1090#1086#1074'.'#1042#1080#1090';UP_264'
          '1;15031;'#1064#1091#1096#1072#1088#1099'-'#1055#1086#1089#1090' 19 '#1082#1084';UP_265'
          '1;15032;'#1064#1091#1096#1072#1088#1099'-'#1055#1086#1089#1090' 16 '#1082#1084';UP_266'
          '1;15033;'#1050#1086#1073#1088#1072#1083#1086#1074#1086'-'#1042#1083#1072#1076#1080#1084#1080#1088#1089#1082#1072#1103';UP_267'
          '1;15034;'#1057#1077#1084#1088#1080#1085#1086'-'#1055#1086#1089#1090' 21 '#1082#1084';UP_268'
          '1;15035;'#1042#1099#1088#1080#1094#1072'-'#1055#1086#1089#1077#1083#1086#1082';UP_269'
          '1;15036;'#1041#1072#1090#1077#1094#1082#1072#1103'-'#1055#1086#1089#1090' 38 '#1082#1084';UP_270'
          '1;15037;'#1042#1077#1090#1074#1100' N 1 '#1091#1079#1083#1072' '#1053#1077#1074#1077#1083#1100';UP_271'
          '1;15038;'#1042#1077#1090#1074#1080' 5 '#1080' 3 '#1091#1079#1083#1072' '#1053#1077#1074#1077#1083#1100';UP_272'
          '1;15101;'#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082';UP_273'
          '1;15130;'#1052#1075#1072'-'#1053#1077#1074#1076#1091#1073#1089#1090#1088#1086#1081';UP_274'
          '1;15131;'#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081' II-'#1053#1086#1074#1086#1086#1082#1090#1103#1073#1088#1100#1089#1082#1080#1081';UP_275'
          '1;15133;'#1054#1073#1074#1086#1076#1085#1072#1103' '#1074#1077#1090#1074#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1103';UP_276'
          '1;15134;'#1042#1077#1090#1074#1100' '#1052#1077#1076#1074#1077#1078#1100#1077#1075#1086#1088#1089#1082#1086#1075#1086' '#1055#1086#1088#1090#1072';UP_277'
          '1;15137;'#1051#1086#1091#1093#1080'-'#1057#1086#1092#1087#1086#1088#1086#1075';UP_280'
          '1;15138;'#1056#1091#1095#1100#1080' '#1050#1072#1088#1077#1083#1100#1089#1082#1080#1077'-'#1040#1083#1072#1082#1091#1088#1090#1080';UP_281'
          '1;15139;'#1055#1080#1085#1086#1079#1077#1088#1086'-'#1050#1086#1074#1076#1086#1088';UP_282'
          '1;15140;'#1040#1087#1072#1090#1080#1090#1099' '#1087#1086#1087' - '#1058#1080#1090#1072#1085';UP_283'
          '1;15141;'#1040#1087#1072#1090#1080#1090#1099' I-'#1040#1087#1072#1090#1080#1090#1099';UP_284'
          '1;15142;'#1040#1087#1072#1090#1080#1090#1099'-'#1043#1088' - '#1041#1083#1086#1082' '#1087#1086#1089#1090' 1268 '#1082#1084';UP_285'
          '1;15143;'#1054#1083#1077#1085#1077#1075#1086#1088#1089#1082'-'#1052#1086#1085#1095#1077#1075#1086#1088#1089#1082';UP_286'
          '1;15144;'#1042#1099#1093#1086#1076#1085#1086#1081'-'#1051#1080#1080#1085#1072#1093#1072#1084#1072#1088#1080';UP_287'
          '1;15145;'#1051#1091#1086#1089#1090#1072#1088#1080'-'#1053#1080#1082#1077#1083#1100';UP_288'
          '1;15147;'#1050#1086#1083#1072'-'#1055#1086#1089#1090' 9 '#1082#1084';UP_289'
          '1;15148;'#1052#1091#1088#1084#1072#1085#1089#1082'-'#1042#1072#1077#1085#1075#1072';UP_290'
          '1;15149;'#1055#1086#1083#1103#1088#1085#1099#1077' '#1047#1086#1088#1080' - '#1041'.'#1087'. 4 '#1082#1084';UP_291'
          '1;15150;'#1053#1103#1083'-'#1057#1077#1083#1074#1080#1085#1076#1078';UP_292'
          '1;15202;'#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1058#1086#1084#1080#1094#1099';UP_293'
          '1;15203;'#1042#1099#1073#1086#1088#1075'-'#1061#1080#1080#1090#1086#1083#1072';UP_294'
          '1;15204;'#1071#1085#1080#1089#1100#1103#1088#1074#1080'-'#1051#1086#1076#1077#1081#1085#1086#1077' '#1055#1086#1083#1077';UP_295'
          '1;15205;'#1047#1072#1087#1072#1076#1085#1086'-'#1050#1072#1088#1077#1083#1100#1089#1082#1072#1103' '#1084#1072#1075#1080#1089#1090#1088#1072#1083#1100';UP_296'
          '1;15206;'#1057#1055#1073'-'#1089#1086#1088#1090'. '#1052#1086#1089#1082'. - '#1057#1055#1073' '#1060#1080#1085#1083'.;UP_297'
          '1;15207;'#1056#1091#1095#1100#1080'-'#1055#1086#1083#1102#1089#1090#1088#1086#1074#1086';UP_298'
          '1;15208;'#1056#1091#1095#1100#1080'-'#1055#1072#1088#1075#1086#1083#1086#1074#1086';UP_299'
          '1;15209;'#1057#1077#1074#1077#1088#1085#1086#1077' '#1087#1086#1083#1091#1082#1086#1083#1100#1094#1086';UP_300'
          '1;15210;'#1055#1072#1074#1083#1086#1074#1089#1082'-'#1053#1086#1074#1075#1086#1088#1086#1076';UP_301'
          '1;15211;'#1052#1075#1072'-'#1057#1090#1077#1082#1086#1083#1100#1085#1099#1081';UP_302'
          '1;15212;'#1043#1072#1090#1095#1080#1085#1072'-'#1058#1086#1089#1085#1086';UP_303'
          '1;15213;'#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1063#1091#1076#1086#1074#1086';UP_304'
          '1;15214;'#1063#1091#1076#1086#1074#1086'-'#1053#1086#1074#1075#1086#1088#1086#1076';UP_305'
          '1;15215;'#1051#1091#1075#1072'-'#1053#1086#1074#1075#1086#1088#1086#1076';UP_306'
          '1;15216;'#1041#1091#1076#1086#1075#1086#1097#1100'-'#1058#1080#1093#1074#1080#1085';UP_307'
          '1;15217;'#1054#1082#1091#1083#1086#1074#1082#1072'-'#1053#1077#1073#1086#1083#1095#1080';UP_308'
          '1;15218;'#1055#1086#1076#1073#1086#1088#1086#1074#1100#1077'-'#1050#1072#1073#1086#1078#1072';UP_309'
          '1;15219;'#1058#1086#1088#1078#1086#1082'-'#1057#1086#1073#1083#1072#1075#1086';UP_310'
          '1;15220;'#1070#1078#1085#1086#1077' '#1087#1086#1083#1091#1082#1086#1083#1100#1094#1086';UP_311'
          '1;15230;'#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1051#1072#1085#1089#1082#1072#1103';UP_312'
          '1;15231;'#1055#1080#1089#1082#1072#1088#1077#1074#1082#1072'-'#1051#1072#1076#1086#1078#1089#1082#1086#1077' '#1054#1079#1077#1088#1086';UP_313'
          '1;15232;'#1052#1077#1083#1100#1085#1080#1095#1080#1081' '#1056#1091#1095#1077#1081'-'#1053#1077#1074#1089#1082#1072#1103' '#1044#1091#1073#1088#1086#1074#1082#1072';UP_314'
          '1;15233;'#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II - '#1056#1078#1077#1074#1082#1072';UP_315'
          '1;15235;'#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' I - '#1043#1086#1088#1099';UP_317'
          '1;15236;'#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II - '#1053#1077#1074#1072';UP_318'
          '1;15237;'#1055#1086#1083#1102#1089#1090#1088#1086#1074#1086'-'#1055#1086#1089#1090' 2 '#1082#1084';UP_319'
          '1;15238;'#1055#1086#1089#1090' 2 '#1082#1084'-'#1056#1091#1095#1100#1080';UP_320'
          '1;15240;'#1050#1072#1084#1077#1085#1085#1086#1075#1086#1088#1089#1082'-'#1057#1074#1077#1090#1086#1075#1086#1088#1089#1082';UP_321'
          '1;15242;'#1052#1072#1090#1082#1072#1089#1077#1083#1100#1082#1103'-'#1042#1103#1088#1090#1089#1080#1083#1103';UP_322'
          '1;15243;'#1041#1088#1091#1089#1085#1080#1095#1085#1072#1103'-'#1051#1077#1085#1076#1077#1088#1099';UP_323'
          '1;15245;'#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1043#1083#1091#1093#1086#1086#1079#1077#1088#1089#1082#1072#1103';UP_324'
          '1;15246;'#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1057#1055#1073'.-'#1090#1086#1074'.-'#1042#1080#1090'.;UP_325'
          '1;15247;'#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1041#1072#1076#1072#1077#1074#1089#1082#1072#1103';UP_326'
          '1;15248;'#1062#1074#1077#1090#1086#1095#1085#1072#1103' - '#1041#1088#1086#1085#1077#1074#1072#1103';UP_327'
          '1;15249;'#1062#1074#1077#1090#1086#1095#1085#1072#1103' - '#1053#1072#1088#1074#1089#1082#1072#1103';UP_328'
          '1;15250;'#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103'-'#1064#1091#1096#1072#1088#1099';UP_329'
          '1;15251;'#1057#1088#1077#1076#1085#1077#1088#1086#1075#1072#1090#1080#1085#1089#1082#1072#1103' '#1074#1077#1090#1074#1100';UP_330'
          '1;15252;'#1053#1072#1088#1074#1089#1082#1072#1103'-'#1057#1055#1073' '#1041#1072#1083#1090'.;UP_331'
          '1;15253;'#1053#1072#1088#1074#1089#1082#1072#1103'-'#1040#1074#1090#1086#1074#1086';UP_332'
          '1;15254;'#1040#1074#1090#1086#1074#1086'-'#1061#1083#1077#1073#1085#1086'-'#1051#1077#1089#1085#1086#1081' '#1052#1086#1083';UP_333'
          '1;15256;'#1042#1086#1081#1090#1086#1083#1086#1074#1082#1072'-'#1043#1086#1088#1099';UP_335'
          '1;15257;'#1055#1086#1089#1090' 22 '#1082#1084'-'#1055#1086#1089#1090' 43 '#1082#1084';UP_336'
          '1;15258;'#1041'.'#1087'.116 '#1082#1084'-'#1050#1091#1082#1086#1083#1100';UP_337'
          '1;15259;'#1048#1088#1089#1072'-'#1055#1086#1089#1072#1076#1085#1080#1082#1086#1074#1086';UP_338'
          '1;15260;'#1051#1091#1075#1072'-'#1056#1079#1076'. 144 '#1082#1084';UP_339'
          '1;15261;'#1051#1077#1076#1084#1086#1079#1077#1088#1086'-'#1050#1080#1074#1080#1103#1088#1074#1080';UP_340'
          '1;15262;'#1063#1072#1083#1085#1072'-'#1064#1091#1081#1089#1082#1072#1103';UP_341'
          '1;15301;'#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1042#1086#1083#1086#1075#1076#1072';UP_342'
          '1;15802;'#1056#1099#1073#1080#1085#1089#1082'-'#1055#1089#1082#1086#1074';UP_343'
          '1;15803;'#1055#1089#1082#1086#1074'-'#1055#1077#1095#1086#1088#1099' '#1055#1089#1082#1086#1074#1089#1082#1080#1077'-'#1042#1072#1083#1075#1072';UP_344'
          '1;15835;'#1042#1072#1083#1076#1072#1081'-'#1050#1088#1077#1089#1090#1094#1099';UP_345'
          '1;15836;'#1052#1086#1088#1086#1079#1086#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100';UP_346'
          '1;15837;'#1055#1089#1082#1086#1074' II-'#1055#1089#1082#1086#1074'-'#1090#1086#1074'.;UP_347'
          '1;15838;'#1051#1102#1073#1103#1090#1086#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100';UP_348'
          '1;15839;'#1055#1083#1072#1090#1080#1097#1077#1085#1082#1072'-'#1041#1083#1086#1082' '#1087#1086#1089#1090' N 122;UP_349'
          '1;15840;'#1042#1077#1090#1074#1100' N 6 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_350'
          '1;15843;'#1055#1077#1095#1086#1088#1099'-'#1055#1089#1082#1086#1074#1089#1082#1080#1077'-'#1058#1072#1088#1090#1091';UP_351'
          '1;15901;'#1041#1086#1083#1086#1075#1086#1077'-'#1050#1083#1103#1089#1090#1080#1094#1072';UP_352'
          '1;15930;'#1042#1077#1090#1074#1100' N 1 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_353'
          '1;15931;'#1042#1077#1090#1074#1100' N 3 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_354'
          '1;15932;'#1042#1077#1090#1074#1100' N 4 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_355'
          '1;16330;'#1042#1077#1090#1074#1100' N 2 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072';UP_356'
          '1;20401;'#1041#1077#1083#1086#1084#1086#1088#1089#1082'-'#1054#1073#1086#1079#1077#1088#1089#1082#1072#1103';UP_357'
          '1;20430;'#1042#1099#1075'-'#1043#1086#1088#1077#1083#1099#1081' '#1052#1086#1089#1090';UP_358'
          '1;15132;'#1055#1072#1088#1082' '#1087#1088#1080#1077#1084#1082#1080' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081' I-'#1055#1086#1088#1086#1075#1080';UP_383'
          '1;10901;'#1052#1086#1089#1082#1074#1072'-'#1056#1080#1075#1072';UP_64'
          '1;11001;'#1052#1086#1089#1082#1074#1072'-'#1054#1074#1080#1085#1080#1097#1077';UP_69'
          '1;14633;'#1057#1087#1073' '#1058#1086#1074' '#1052#1086#1089#1082'. - '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'.;UP_736'
          '1;11130;'#1042#1077#1090#1074#1080' 2 '#1080' 2'#1072' '#1052#1054#1050';UP_75'
          '1;14647;'#1042#1077#1090#1074#1100' N 31 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077';UP_756'
          '1;15152;'#1050#1086#1084#1089#1086#1084#1086#1083#1100#1089#1082'-'#1055#1088#1086#1084#1099#1096#1083#1077#1085#1085#1072#1103';UP_762')
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object tsPUT_GL: TTabSheet
      Caption = 'PUT_GL'
      ImageIndex = 3
      object mPUT_GL: TMemo
        Left = 0
        Top = 0
        Width = 966
        Height = 736
        Align = alClient
        Lines.Strings = (
          'IDNAPR;NAMEPUT;NDOR;NAPR;NPUT;IDPUT;RWIDTH;S_KM;S_M;E_KM;E_M'
          
            'UP_224;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1080#1088#1080#1096#1080'-'#1040#1085#1076#1088#1077#1077#1074#1086', I;1;11034;I;110000122892;15' +
            '20;0;982;2;421'
          
            'UP_240;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' '#1060#1080#1085#1083'.-'#1051#1091#1078#1072#1081#1082#1072'- '#1075#1086#1089#1075#1088#1072#1085#1080#1094#1072', I;1;14701;I;1' +
            '10000122936;1520;1;290;158;208'
          
            'UP_248;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1080#1075#1086#1074#1086'-'#1042#1077#1081#1084#1072#1088#1085', II;1;14802;II;110000122967;1' +
            '520;13;958;149;745'
          
            'UP_249;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1077#1073#1103#1078#1100#1077'-'#1050#1088#1072#1089#1085#1086#1092#1083#1086#1090#1089#1082', I;1;14830;I;1100001229' +
            '70;1520;61;3;68;498'
          
            'UP_267;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1073#1088#1072#1083#1086#1074#1086'-'#1042#1083#1072#1076#1080#1084#1080#1088#1089#1082#1072#1103', I;1;15033;I;11000012' +
            '3013;1520;1;0;2;989'
          
            'UP_284;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1040#1087#1072#1090#1080#1090#1099' I-'#1040#1087#1072#1090#1080#1090#1099', II;1;15141;II;11000012305' +
            '6;1520;1;580;6;512'
          
            'UP_296;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1072#1087#1072#1076#1085#1086'-'#1050#1072#1088#1077#1083#1100#1089#1082#1072#1103' '#1084#1072#1075#1080#1089#1090#1088#1072#1083#1100', I;1;15205;I;1' +
            '10000123093;1520;0;329;335;787'
          
            'UP_325;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1057#1055#1073'.-'#1090#1086#1074'.-'#1042#1080#1090'., II;1;15246;II;110' +
            '000123132;1520;1;303;2;305'
          
            'UP_337;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041'.'#1087'.116 '#1082#1084'-'#1050#1091#1082#1086#1083#1100', II;1;15258;II;11000012315' +
            '6;1520;1;0;20;308'
          
            'UP_1160;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1083#1072#1074#1103#1085#1082#1072' - '#1056#1099#1073#1072#1094#1082#1086#1077', I;1;14642;I;1100001232' +
            '20;1520;1;136;3;643'
          
            'UP_226;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1052#1086#1089#1082#1074#1072', I;1;14601;I;11000012' +
            '2897;1520;1;0;650;307'
          
            'UP_304;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1063#1091#1076#1086#1074#1086', IV;1;15213;IV;1100001231' +
            '10;1520;101;694;108;105'
          
            'UP_310;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1058#1086#1088#1078#1086#1082'-'#1057#1086#1073#1083#1072#1075#1086', I;1;15219;I;110000123116;152' +
            '0;0;475;165;345'
          
            'UP_341;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1063#1072#1083#1085#1072'-'#1064#1091#1081#1089#1082#1072#1103', I;1;15262;I;110000123163;1520' +
            ';1;0;1;735'
          
            'UP_345;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1072#1083#1076#1072#1081'-'#1050#1088#1077#1089#1090#1094#1099', I;1;15835;I;110000123171;152' +
            '0;7;653;60;0'
          
            'UP_736;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1058#1086#1074' '#1052#1086#1089#1082'. - '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'., 4'#1090';1;14633;4' +
            #1090';110000123198;1520;1;681;11;643'
          
            'UP_253;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1056#1077#1079#1077#1082#1085#1077', II;1;14901;II;110' +
            '000122979;1520;3;414;241;126'
          
            'UP_69;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1086#1089#1082#1074#1072'-'#1054#1074#1080#1085#1080#1097#1077', 1;1;11001;1;110000122852;1520' +
            ';128;525;328;0'
          
            'UP_222;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1072#1083#1103#1079#1080#1085'-'#1059#1075#1083#1080#1095', IV;1;11032;IV;110000122885;15' +
            '20;182;0;184;500'
          
            'UP_265;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1064#1091#1096#1072#1088#1099'-'#1055#1086#1089#1090' 19 '#1082#1084', I;1;15031;I;110000123008;' +
            '1520;1;0;5;0'
          
            'UP_281;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1056#1091#1095#1100#1080' '#1050#1072#1088#1077#1083#1100#1089#1082#1080#1077'-'#1040#1083#1072#1082#1091#1088#1090#1080', I;1;15138;I;11000' +
            '0123047;1520;0;408;100;85'
          
            'UP_354;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 3 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;15931;I;11000012' +
            '3187;1520;1;0;4;64'
          
            'UP_736;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1058#1086#1074' '#1052#1086#1089#1082'. - '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'., 6'#1090';1;14633;6' +
            #1090';110000123201;1520;1;277;4;340'
          
            'UP_297;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073'-'#1089#1086#1088#1090'. '#1052#1086#1089#1082'. - '#1057#1055#1073' '#1060#1080#1085#1083'., II;1;15206;II;1' +
            '10000123096;1520;3;297;8;68'
          
            'UP_314;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1077#1083#1100#1085#1080#1095#1080#1081' '#1056#1091#1095#1077#1081'-'#1053#1077#1074#1089#1082#1072#1103' '#1044#1091#1073#1088#1086#1074#1082#1072', I;1;15232;' +
            'I;110000123121;1520;1;632;39;194'
          
            'UP_300;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1077#1074#1077#1088#1085#1086#1077' '#1087#1086#1083#1091#1082#1086#1083#1100#1094#1086', II;1;15209;II;110000123' +
            '102;1520;1;389;13;780'
          
            'UP_331;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1053#1072#1088#1074#1089#1082#1072#1103'-'#1057#1055#1073' '#1041#1072#1083#1090'., I;1;15252;I;110000123146' +
            ';1520;1;0;3;683'
          
            'UP_349;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1083#1072#1090#1080#1097#1077#1085#1082#1072'-'#1041#1083#1086#1082' '#1087#1086#1089#1090' N 122, I;1;15839;I;1100' +
            '00123178;1520;1;0;14;350'
          
            'UP_1137;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041#1055' 1256 - '#1040#1087#1072#1090#1080#1090#1099' '#1087#1088'/'#1086#1090#1087'('#1095#1077#1088#1077#1079' '#1040#1087#1072#1090#1080#1090#1099' '#1089#1086#1088#1090 +
            '), IV;1;15146;IV;110000123063;1520;1;4;4;110'
          
            'UP_330;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1088#1077#1076#1085#1077#1088#1086#1075#1072#1090#1080#1085#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', II;1;15251;II;11000' +
            '0123143;1520;1;0;2;787'
          
            'UP_339;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1091#1075#1072'-'#1056#1079#1076'. 144 '#1082#1084', I;1;15260;I;110000123159;1' +
            '520;1;0;6;186'
          
            'UP_330;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1088#1077#1076#1085#1077#1088#1086#1075#1072#1090#1080#1085#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', I;1;15251;I;1100001' +
            '23141;1520;1;0;3;691'
          
            'UP_317;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' I - '#1043#1086#1088#1099', II;1;15235;II;11000' +
            '0123211;1520;2;214;35;43'
          
            'UP_220;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1077#1084#1094#1099'-'#1046#1072#1088#1082#1086#1074#1089#1082#1080#1081', I;1;10939;I;110000122876;1' +
            '520;1;488;52;990'
          
            'UP_235;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1064#1083#1102#1079'-'#1042#1080#1085#1086#1082#1086#1083#1099', I;1;14639;I;110000122925;1520' +
            ';1;513;5;0'
          
            'UP_238;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 2 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;14645;I;11000012' +
            '2931;1520;2;273;7;398'
          
            'UP_274;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1075#1072'-'#1053#1077#1074#1076#1091#1073#1089#1090#1088#1086#1081', I;1;15130;I;110000123037;15' +
            '20;1;118;13;766'
          
            'UP_286;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1054#1083#1077#1085#1077#1075#1086#1088#1089#1082'-'#1052#1086#1085#1095#1077#1075#1086#1088#1089#1082', I;1;15143;I;110000123' +
            '065;1520;1;800;32;376'
          
            'UP_289;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1083#1072'-'#1055#1086#1089#1090' 9 '#1082#1084', I;1;15147;I;110000123072;152' +
            '0;0;389;7;988'
          
            'UP_304;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1063#1091#1076#1086#1074#1086', II;1;15213;II;1100001231' +
            '09;1520;3;911;7;794'
          
            'UP_305;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1063#1091#1076#1086#1074#1086'-'#1053#1086#1074#1075#1086#1088#1086#1076', I;1;15214;I;110000123111;15' +
            '20;1;590;72;963'
          
            'UP_323;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041#1088#1091#1089#1085#1080#1095#1085#1072#1103'-'#1051#1077#1085#1076#1077#1088#1099', I;1;15243;I;110000123129' +
            ';1520;1;270;61;702'
          
            'UP_353;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 1 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;15930;I;11000012' +
            '3186;1520;0;912;2;958'
          
            'UP_355;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 4 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;15932;I;11000012' +
            '3189;1520;1;0;4;285'
          
            'UP_263;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1042#1080#1090#1077#1073#1089#1082', III;1;15001;III;110' +
            '000123001;1520;1;133;14;675'
          
            'UP_280;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1086#1091#1093#1080'-'#1057#1086#1092#1087#1086#1088#1086#1075', I;1;15137;I;110000123045;152' +
            '0;1;760;103;278'
          
            'UP_75;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1080' 2 '#1080' 2'#1072' '#1052#1054#1050', 2'#1074';1;11130;2'#1074';110000122855;' +
            '1520;2;829;3;129'
          
            'UP_117;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1080' 2 '#1080' 5 '#1091#1079#1083#1072' '#1055#1086#1074#1072#1088#1086#1074#1086', 5'#1074';1;11532;5'#1074';110' +
            '000122865;1520;599;403;599;818'
          
            'UP_179;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1080#1093#1086#1089#1083#1072#1074#1083#1100'-'#1042#1103#1079#1100#1084#1072', 1;1;16301;1;110000122868;' +
            '1520;1;303;166;0'
          
            'UP_222;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1072#1083#1103#1079#1080#1085'-'#1059#1075#1083#1080#1095', I;1;11032;I;110000122882;1520' +
            ';1;0;47;700'
          
            'UP_223;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1054#1074#1080#1085#1080#1097#1077'-'#1042#1077#1089#1100#1077#1075#1086#1085#1089#1082', I;1;11033;I;110000122891' +
            ';1520;333;700;376;200'
          
            'UP_229;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'. - '#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103', II;1;14632;II;1' +
            '10000122909;1520;12;558;14;868'
          
            'UP_230;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1073#1083#1080#1085#1086'-'#1055#1086#1089#1090' 22 '#1082#1084', I;1;14634;I;110000122914' +
            ';1520;1;89;2;230'
          
            'UP_383;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1072#1088#1082' '#1087#1088#1080#1077#1084#1082#1080' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081' I-'#1055#1086#1088#1086#1075#1080', I;1;15132' +
            ';I;110000123195;1520;1;0;3;120'
          
            'UP_294;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1099#1073#1086#1088#1075'-'#1061#1080#1080#1090#1086#1083#1072', I;1;15203;I;110000123088;152' +
            '0;1;352;94;287'
          
            'UP_295;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1071#1085#1080#1089#1100#1103#1088#1074#1080'-'#1051#1086#1076#1077#1081#1085#1086#1077' '#1055#1086#1083#1077', I;1;15204;I;1100001' +
            '23090;1520;0;0;220;873'
          
            'UP_1158;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' '#1090#1086#1074'. '#1052#1086#1089#1082' - '#1043#1083#1091#1093#1086#1086#1079#1077#1088#1089#1082#1072#1103', ' +
            'II;1;14643;II;110000123218;1520;2;600;4;160'
          
            'UP_306;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1091#1075#1072'-'#1053#1086#1074#1075#1086#1088#1086#1076', I;1;15215;I;110000123112;1520' +
            ';1;430;94;580'
          
            'UP_309;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1086#1076#1073#1086#1088#1086#1074#1100#1077'-'#1050#1072#1073#1086#1078#1072', I;1;15218;I;110000123115;' +
            '1520;1;0;95;146'
          
            'UP_347;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1089#1082#1086#1074' II-'#1055#1089#1082#1086#1074'-'#1090#1086#1074'., I;1;15837;I;11000012317' +
            '5;1520;1;0;2;281'
          
            'UP_340;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1077#1076#1084#1086#1079#1077#1088#1086'-'#1050#1080#1074#1080#1103#1088#1074#1080', I;1;15261;I;110000123161' +
            ';1520;0;454;123;719'
          
            'UP_290;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1091#1088#1084#1072#1085#1089#1082'-'#1042#1072#1077#1085#1075#1072', I;1;15148;I;110000123074;15' +
            '20;1;289;31;694'
          
            'UP_762;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1084#1089#1086#1084#1086#1083#1100#1089#1082'-'#1055#1088#1086#1084#1099#1096#1083#1077#1085#1085#1072#1103', I;1;15152;I;110000' +
            '123204;1520;1;465;4;150'
          
            'UP_247;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1053#1072#1088#1074#1072', III;1;14801;III;110' +
            '000122962;1520;3;69;3;924'
          
            'UP_69;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1086#1089#1082#1074#1072'-'#1054#1074#1080#1085#1080#1097#1077', 2;1;11001;2;A22627444;1520;26' +
            '1;448;263;101'
          
            'UP_64;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1086#1089#1082#1074#1072'-'#1056#1080#1075#1072', 2;1;10901;2;110000122850;1520;47' +
            '5;208;480;204'
          
            'UP_294;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1099#1073#1086#1088#1075'-'#1061#1080#1080#1090#1086#1083#1072', II;1;15203;II;110000122955;1' +
            '520;1;383;41;957'
          
            'UP_261;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1072#1090#1095#1080#1085#1072' '#1042#1072#1088#1096'.'#1041' - '#1043#1072#1090#1095#1080#1085#1072' '#1090#1086#1074'. '#1041', I;1;14937;I' +
            ';110000122992;1520;0;90;1;58'
          
            'UP_293;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1058#1086#1084#1080#1094#1099', I;1;15202;I;110000123079;1' +
            '520;4;322;534;802'
          
            'UP_293;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1058#1086#1084#1080#1094#1099', III;1;15202;III;1100001230' +
            '84;1520;90;86;92;201'
          
            'UP_303;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1072#1090#1095#1080#1085#1072'-'#1058#1086#1089#1085#1086', I;1;15212;I;110000123106;1520' +
            ';0;398;33;214'
          
            'UP_304;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1063#1091#1076#1086#1074#1086', I;1;15213;I;110000123108' +
            ';1520;1;825;108;538'
          
            'UP_338;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1048#1088#1089#1072'-'#1055#1086#1089#1072#1076#1085#1080#1082#1086#1074#1086', I;1;15259;I;110000123158;1' +
            '520;1;503;3;246'
          
            'UP_344;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1089#1082#1086#1074'-'#1055#1077#1095#1086#1088#1099' '#1055#1089#1082#1086#1074#1089#1082#1080#1077'-'#1042#1072#1083#1075#1072', I;1;15803;I;11' +
            '0000123169;1520;1;97;48;807'
          
            'UP_313;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1080#1089#1082#1072#1088#1077#1074#1082#1072'-'#1051#1072#1076#1086#1078#1089#1082#1086#1077' '#1054#1079#1077#1088#1086', I;1;15231;I;1100' +
            '00123119;1520;0;0;47;292'
          
            'UP_258;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1064#1086#1089#1089#1077#1081#1085#1072#1103'-'#1057#1088#1077#1076#1085#1077'-'#1056#1086#1075#1072#1090#1089#1082#1072#1103', I;1;14934;I;1100' +
            '00122986;1520;0;926;3;989'
          
            'UP_217;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 1 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072', I;1;10936;I;110000' +
            '122870;1520;1;440;4;56'
          
            'UP_282;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1080#1085#1086#1079#1077#1088#1086'-'#1050#1086#1074#1076#1086#1088', I;1;15139;I;110000123049;15' +
            '20;0;0;117;380'
          
            'UP_241;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1072#1085#1089#1082#1072#1103'-'#1041#1077#1083#1086#1086#1089#1090#1088#1086#1074', IIX;1;14702;IIX;11000012' +
            '2947;1520;43;24;43;584'
          
            'UP_266;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1064#1091#1096#1072#1088#1099'-'#1055#1086#1089#1090' 16 '#1082#1084', I;1;15032;I;110000123010;' +
            '1520;1;0;1;466'
          
            'UP_273;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082', IV;1;15101;IV;1100' +
            '00123209;1520;14;796;20;915'
          
            'UP_336;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1086#1089#1090' 22 '#1082#1084'-'#1055#1086#1089#1090' 43 '#1082#1084', I;1;15257;I;110000123' +
            '154;1520;1;268;3;3'
          
            'UP_349;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1083#1072#1090#1080#1097#1077#1085#1082#1072'-'#1041#1083#1086#1082' '#1087#1086#1089#1090' N 122, II;1;15839;II;11' +
            '0000123180;1520;12;897;14;147'
          
            'UP_1183;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' '#1091#1079#1083#1072' '#1050#1086#1090#1083#1099', 1'#1040';1;14835;1'#1040';11000012322' +
            '2;1520;1;0;2;465'
          
            'UP_343;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1056#1099#1073#1080#1085#1089#1082'-'#1055#1089#1082#1086#1074', 1;1;15802;1;110000123168;1520' +
            ';104;662;655;116'
          
            'UP_298;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1056#1091#1095#1100#1080'-'#1055#1086#1083#1102#1089#1090#1088#1086#1074#1086', I;1;15207;I;110000123099;1' +
            '520;2;0;5;900'
          
            'UP_1143;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1088#1077#1076#1087#1086#1088#1090#1086#1074#1072#1103' - '#1040#1074#1090#1086#1074#1086', I;1;15263;I;11000012' +
            '3210;1520;1;0;5;33'
          
            'UP_20;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1053#1080#1082#1086#1083#1072#1077#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', I;1;10431;I;A19936962;152' +
            '0;1;0;1;307'
          
            'UP_1210;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1086#1089#1077#1074#1086' I - '#1050#1072#1084#1077#1085#1085#1086#1075#1086#1088#1089#1082', 1;1;15201;1;A22536' +
            '342;1520;0;0;62;169'
          
            'UP_275;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081' II-'#1053#1086#1074#1086#1086#1082#1090#1103#1073#1088#1100#1089#1082#1080#1081', III;1;15131;' +
            'III;110000123039;1520;1;215;3;235'
          
            'UP_301;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1072#1074#1083#1086#1074#1089#1082'-'#1053#1086#1074#1075#1086#1088#1086#1076', II;1;15210;II;11000012310' +
            '4;1520;165;588;171;125'
          
            'UP_313;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1080#1089#1082#1072#1088#1077#1074#1082#1072'-'#1051#1072#1076#1086#1078#1089#1082#1086#1077' '#1054#1079#1077#1088#1086', II;1;15231;II;11' +
            '0000123120;1520;1;113;17;632'
          
            'UP_321;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1072#1084#1077#1085#1085#1086#1075#1086#1088#1089#1082'-'#1057#1074#1077#1090#1086#1075#1086#1088#1089#1082', I;1;15240;I;1100001' +
            '23127;1520;1;245;27;715'
          
            'UP_348;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1102#1073#1103#1090#1086#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', I;1;15838;I;110000123176;' +
            '1520;1;0;4;213'
          
            'UP_273;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082', 3'#1050';1;15101;3'#1050';1100' +
            '00123206;1520;11;325;14;432'
          
            'UP_228;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' '#1090#1086#1074' '#1052#1086#1089#1082'.- '#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103', I;1;14631;I;11000' +
            '0122905;1520;1;0;2;661'
          
            'UP_293;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1058#1086#1084#1080#1094#1099', IV;1;15202;IV;110000123086' +
            ';1520;7;234;9;700'
          
            'UP_1157;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1044#1072#1095#1072' '#1044#1086#1083#1075#1086#1088#1091#1082#1086#1074#1072' - '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II, II;1;' +
            '15239;II;110000123215;1520;1;375;6;417'
          
            'UP_256;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1042#1072#1088#1096'.-'#1062#1074#1077#1090#1086#1095#1085#1072#1103', I;1;14932;I;11000012298' +
            '2;1520;1;0;2;872'
          
            'UP_262;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1086#1089'.'#1075#1088'. '#1089' '#1051#1072#1090#1074#1080#1077#1081' '#1055#1091#1088#1074#1084#1072#1083#1072'-'#1055#1099#1090#1072#1083#1086#1074#1086', I;1;149' +
            '44;I;110000122995;1520;177;162;188;402'
          
            'UP_276;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1054#1073#1074#1086#1076#1085#1072#1103' '#1074#1077#1090#1074#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1103', I;1;15133;I;1100' +
            '00123041;1520;1;0;4;400'
          
            'UP_240;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' '#1060#1080#1085#1083'.-'#1051#1091#1078#1072#1081#1082#1072'- '#1075#1086#1089#1075#1088#1072#1085#1080#1094#1072', II;1;14701;II' +
            ';110000122939;1520;1;490;126;109'
          
            'UP_248;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1080#1075#1086#1074#1086'-'#1042#1077#1081#1084#1072#1088#1085', I;1;14802;I;110000122965;152' +
            '0;14;115;149;925'
          
            'UP_251;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1076#1086#1074'-'#1042#1077#1081#1084#1072#1088#1085', I;1;14832;I;110000122975;1520;' +
            '115;0;216;591'
          
            'UP_342;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1042#1086#1083#1086#1075#1076#1072', 1;1;15301;1;11000012316' +
            '4;1520;125;82;462;0'
          
            'UP_329;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103'-'#1064#1091#1096#1072#1088#1099', I;1;15250;I;110000123138;' +
            '1520;1;0;6;140'
          
            'UP_297;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073'-'#1089#1086#1088#1090'. '#1052#1086#1089#1082'. - '#1057#1055#1073' '#1060#1080#1085#1083'., I;1;15206;I;110' +
            '000123095;1520;0;670;19;0'
          
            'UP_329;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103'-'#1064#1091#1096#1072#1088#1099', I14;1;15250;I14;110000123' +
            '140;1520;1;0;1;726'
          
            'UP_284;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1040#1087#1072#1090#1080#1090#1099' I-'#1040#1087#1072#1090#1080#1090#1099', I;1;15141;I;110000123053;' +
            '1520;2;714;6;629'
          
            'UP_1159;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1095#1082#1086#1084#1072' - '#1051#1077#1076#1084#1086#1079#1077#1088#1086' II, I;1;15135;I;1100001' +
            '23219;1520;0;87;121;280'
          
            'UP_250;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1090#1083#1099'-'#1059#1089#1090#1100'-'#1051#1091#1075#1072', 1'#1070';1;14831;1'#1070';110000123225;' +
            '1520;23;339;27;135'
          
            'UP_116;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' 1 '#1091#1079#1083#1072' '#1055#1086#1074#1072#1088#1086#1074#1086', 1'#1074';1;11531;1'#1074';1100001' +
            '22860;1520;599;634;599;833'
          
            'UP_218;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 3 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072', I;1;10937;I;110000' +
            '122872;1520;1;733;2;833'
          
            'UP_247;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1053#1072#1088#1074#1072', I;1;14801;I;1100001' +
            '22957;1520;1;0;159;157'
          
            'UP_236;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1044#1086#1088#1086#1096#1080#1093#1072'-'#1042#1072#1089#1080#1083#1100#1077#1074#1089#1082#1080#1081' '#1052#1086#1093', I;1;14640;I;11000' +
            '0122927;1520;1;705;18;768'
          
            'UP_241;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1072#1085#1089#1082#1072#1103'-'#1041#1077#1083#1086#1086#1089#1090#1088#1086#1074', I;1;14702;I;110000122942' +
            ';1520;6;425;44;98'
          
            'UP_241;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1072#1085#1089#1082#1072#1103'-'#1041#1077#1083#1086#1086#1089#1090#1088#1086#1074', II;1;14702;II;1100001229' +
            '44;1520;6;40;9;220'
          
            'UP_319;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1086#1083#1102#1089#1090#1088#1086#1074#1086'-'#1055#1086#1089#1090' 2 '#1082#1084', I;1;15237;I;1100001231' +
            '25;1520;1;0;2;325'
          
            'UP_75;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1080' 2 '#1080' 2'#1072' '#1052#1054#1050', 2'#1072';1;11130;2'#1072';110000122858;' +
            '1520;3;215;3;401'
          
            'UP_312;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1051#1072#1085#1089#1082#1072#1103', I;1;15230;I;110000123118;' +
            '1520;1;525;2;886'
          
            'UP_263;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1042#1080#1090#1077#1073#1089#1082', II;1;15001;II;11000' +
            '0122999;1520;1;39;85;912'
          
            'UP_263;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1042#1080#1090#1077#1073#1089#1082', '#1089#1074';1;15001;'#1089#1074';11000' +
            '0123003;1520;241;900;244;100'
          
            'UP_268;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1077#1084#1088#1080#1085#1086'-'#1055#1086#1089#1090' 21 '#1082#1084', I;1;15034;I;110000123015' +
            ';1520;1;457;5;457'
          
            'UP_317;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' I - '#1043#1086#1088#1099', I;1;15235;I;1100001' +
            '23123;1520;0;368;34;746'
          
            'UP_1157;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1044#1072#1095#1072' '#1044#1086#1083#1075#1086#1088#1091#1082#1086#1074#1072' - '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II, I;1;1' +
            '5239;I;110000123213;1520;1;0;6;318'
          
            'UP_337;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041'.'#1087'.116 '#1082#1084'-'#1050#1091#1082#1086#1083#1100', I;1;15258;I;110000123226;' +
            '1520;1;0;20;330'
          
            'UP_332;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1053#1072#1088#1074#1089#1082#1072#1103'-'#1040#1074#1090#1086#1074#1086', I;1;15253;I;110000123148;15' +
            '20;1;0;4;483'
          
            'UP_736;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1058#1086#1074' '#1052#1086#1089#1082'. - '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'., 5'#1090';1;14633;5' +
            #1090';110000123200;1520;1;76;3;975'
          
            'UP_229;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'. - '#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103', III;1;14632;III' +
            ';110000122911;1520;11;643;13;855'
          
            'UP_117;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1080' 2 '#1080' 5 '#1091#1079#1083#1072' '#1055#1086#1074#1072#1088#1086#1074#1086', 2'#1074';1;11532;2'#1074';110' +
            '000122863;1520;599;702;599;850'
          
            'UP_226;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1052#1086#1089#1082#1074#1072', III;1;14601;III;1100' +
            '00122902;1520;1;0;649;1000'
          
            'UP_231;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1058#1086#1089#1085#1086'-'#1064#1072#1087#1082#1080', I;1;14635;I;110000122916;1520;0' +
            ';790;20;913'
          
            'UP_260;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1072#1090#1095#1080#1085#1072' '#1042#1072#1088#1096'.'#1041' - '#1060#1088#1077#1079#1077#1088#1085#1099#1081', I;1;14936;I;1100' +
            '00122990;1520;1;0;2;435'
          
            'UP_264;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073'.'#1089#1086#1088#1090'.'#1042#1080#1090'.-'#1057#1055#1073'.'#1090#1086#1074'.'#1042#1080#1090', I;1;15030;I;11000' +
            '0123006;1520;1;0;1;685'
          
            'UP_273;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082', III;1;15101;III;11' +
            '0000123032;1520;12;208;574;982'
          
            'UP_273;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082', '#1086#1073#1093';1;15101;'#1086#1073#1093';11' +
            '0000123034;1520;769;743;788;343'
          
            'UP_283;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1040#1087#1072#1090#1080#1090#1099' '#1087#1086#1087' - '#1058#1080#1090#1072#1085', II;1;15140;II;110000123' +
            '051;1520;1;485;14;630'
          
            'UP_288;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1091#1086#1089#1090#1072#1088#1080'-'#1053#1080#1082#1077#1083#1100', I;1;15145;I;110000123070;15' +
            '20;1;0;46;887'
          
            'UP_308;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1054#1082#1091#1083#1086#1074#1082#1072'-'#1053#1077#1073#1086#1083#1095#1080', I;1;15217;I;110000123114;1' +
            '520;0;942;103;616'
          
            'UP_325;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1057#1055#1073'.-'#1090#1086#1074'.-'#1042#1080#1090'., I;1;15246;I;11000' +
            '0123131;1520;0;928;4;343'
          
            'UP_352;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041#1086#1083#1086#1075#1086#1077'-'#1050#1083#1103#1089#1090#1080#1094#1072', I;1;15901;I;110000123185;1' +
            '520;1;0;415;0'
          
            'UP_358;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1099#1075'-'#1043#1086#1088#1077#1083#1099#1081' '#1052#1086#1089#1090', I;1;20430;I;110000123193;1' +
            '520;10;460;11;600'
          
            'UP_225;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1080#1088#1080#1096#1080'-'#1048#1088#1089#1072', I;1;11035;I;110000122895;1520;1' +
            ';600;5;76'
          
            'UP_307;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041#1091#1076#1086#1075#1086#1097#1100'-'#1058#1080#1093#1074#1080#1085', I;1;15216;I;110000123113;15' +
            '20;19;652;96;40'
          
            'UP_219;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 4 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072', I;1;10938;I;110000' +
            '122874;1520;3;70;4;270'
          
            'UP_247;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1053#1072#1088#1074#1072', II;1;14801;II;11000' +
            '0122959;1520;1;0;121;677'
          
            'UP_263;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1042#1080#1090#1077#1073#1089#1082', I;1;15001;I;1100001' +
            '22997;1520;1;133;486;0'
          
            'UP_270;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041#1072#1090#1077#1094#1082#1072#1103'-'#1055#1086#1089#1090' 38 '#1082#1084', II;1;15036;II;110000123' +
            '019;1520;1;0;4;739'
          
            'UP_322;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1072#1090#1082#1072#1089#1077#1083#1100#1082#1103'-'#1042#1103#1088#1090#1089#1080#1083#1103', I;1;15242;I;1100001231' +
            '28;1520;0;54;26;612'
          
            'UP_326;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1041#1072#1076#1072#1077#1074#1089#1082#1072#1103', I;1;15247;I;110000123' +
            '133;1520;1;400;3;577'
          
            'UP_346;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1086#1088#1086#1079#1086#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', I;1;15836;I;110000123173;' +
            '1520;1;0;4;498'
          
            'UP_301;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1072#1074#1083#1086#1074#1089#1082'-'#1053#1086#1074#1075#1086#1088#1086#1076', I;1;15210;I;110000123103;' +
            '1520;28;151;171;125'
          
            'UP_64;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1086#1089#1082#1074#1072'-'#1056#1080#1075#1072', 1;1;10901;1;110000122848;1520;15' +
            '6;0;640;680'
          
            'UP_232;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1058#1086#1088#1092#1103#1085#1086#1077'-'#1063#1091#1076#1086#1074#1086', I;1;14636;I;110000122918;15' +
            '20;1;0;6;35'
          
            'UP_273;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082', I;1;15101;I;110000' +
            '123027;1520;11;376;1449;117'
          
            'UP_291;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1086#1083#1103#1088#1085#1099#1077' '#1047#1086#1088#1080' - '#1041'.'#1087'. 4 '#1082#1084', I;1;15149;I;11000' +
            '0123076;1520;1;0;4;700'
          
            'UP_330;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1088#1077#1076#1085#1077#1088#1086#1075#1072#1090#1080#1085#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', I13;1;15251;I13;110' +
            '000123144;1520;1;632;2;600'
          
            'UP_357;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1041#1077#1083#1086#1084#1086#1088#1089#1082'-'#1054#1073#1086#1079#1077#1088#1089#1082#1072#1103', 1;1;20401;1;1100001231' +
            '92;1520;3;470;132;0'
          
            'UP_736;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1058#1086#1074' '#1052#1086#1089#1082'. - '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'., 3'#1090';1;14633;3' +
            #1090';110000123197;1520;1;928;10;141'
          
            'UP_233;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1059#1075#1083#1086#1074#1082#1072'-'#1041#1086#1088#1086#1074#1080#1095#1080', I;1;14637;I;110000122920;1' +
            '520;1;20;31;93'
          
            'UP_259;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1072#1090#1095#1080#1085#1072'-'#1042#1072#1088#1096'.-'#1043#1072#1090#1095#1080#1085#1072'-'#1041#1072#1083#1090'., I;1;14935;I;110' +
            '000122988;1520;1;508;5;263'
          
            'UP_271;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 1 '#1091#1079#1083#1072' '#1053#1077#1074#1077#1083#1100', I;1;15037;I;110000123' +
            '021;1520;463;346;467;616'
          
            'UP_285;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1040#1087#1072#1090#1080#1090#1099'-'#1043#1088' - '#1041#1083#1086#1082' '#1087#1086#1089#1090' 1268 '#1082#1084', III;1;15142;' +
            'III;110000123061;1520;1;0;4;700'
          
            'UP_293;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1091#1096#1077#1083#1077#1074#1082#1072'-'#1058#1086#1084#1080#1094#1099', II;1;15202;II;110000123081' +
            ';1520;1;0;92;205'
          
            'UP_324;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1082#1086#1074#1089#1082#1072#1103'-'#1043#1083#1091#1093#1086#1086#1079#1077#1088#1089#1082#1072#1103', I;1;15245;I;110000' +
            '123130;1520;1;0;2;872'
          
            'UP_328;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1062#1074#1077#1090#1086#1095#1085#1072#1103' - '#1053#1072#1088#1074#1089#1082#1072#1103', I;1;15249;I;1100001231' +
            '37;1520;1;0;3;330'
          
            'UP_756;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 31 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;14647;I;1100001' +
            '23203;1520;1;0;2;693'
          
            'UP_277;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' '#1052#1077#1076#1074#1077#1078#1100#1077#1075#1086#1088#1089#1082#1086#1075#1086' '#1055#1086#1088#1090#1072', I;1;15134;I;11' +
            '0000123043;1520;1;0;1;347'
          
            'UP_229;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1087#1073' '#1057#1086#1088#1090' '#1052#1086#1089#1082'. - '#1050#1091#1087#1095#1080#1085#1089#1082#1072#1103', I;1;14632;I;110' +
            '000122907;1520;13;600;14;961'
          
            'UP_237;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1085#1072#1082#1086#1074#1089#1082#1072#1103' '#1074#1077#1090#1074#1100', I;1;14641;I;110000122929;' +
            '1520;2;320;37;716'
          
            'UP_243;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1080#1093#1090#1086#1074#1086#1077'-'#1042#1099#1089#1086#1094#1082', I;1;14730;I;110000122951;15' +
            '20;1;492;12;734'
          
            'UP_318;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II - '#1053#1077#1074#1072', I;1;15236;I;110000' +
            '123124;1520;1;215;7;270'
          
            'UP_320;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1086#1089#1090' 2 '#1082#1084'-'#1056#1091#1095#1100#1080', I;1;15238;I;110000123126;15' +
            '20;1;251;4;46'
          
            'UP_303;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1043#1072#1090#1095#1080#1085#1072'-'#1058#1086#1089#1085#1086', II;1;15212;II;110000123107;15' +
            '20;0;416;47;650'
          
            'UP_311;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1070#1078#1085#1086#1077' '#1087#1086#1083#1091#1082#1086#1083#1100#1094#1086', I;1;15220;I;110000123117;1' +
            '520;1;573;24;485'
          
            'UP_350;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 6 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;15840;I;11000012' +
            '3182;1520;1;73;3;248'
          
            'UP_342;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1083#1093#1086#1074#1089#1090#1088#1086#1081'-'#1042#1086#1083#1086#1075#1076#1072', 2;1;15301;2;11000012316' +
            '6;1520;124;986;462;0'
          
            'UP_1158;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' '#1090#1086#1074'. '#1052#1086#1089#1082' - '#1043#1083#1091#1093#1086#1086#1079#1077#1088#1089#1082#1072#1103', ' +
            'I;1;14643;I;110000123216;1520;1;0;1;470'
          
            'UP_297;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073'-'#1089#1086#1088#1090'. '#1052#1086#1089#1082'. - '#1057#1055#1073' '#1060#1080#1085#1083'., III;1;15206;III' +
            ';110000123097;1520;19;0;23;756'
          
            'UP_300;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1077#1074#1077#1088#1085#1086#1077' '#1087#1086#1083#1091#1082#1086#1083#1100#1094#1086', I;1;15209;I;11000012310' +
            '1;1520;1;799;8;588'
          
            'UP_234;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 5 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;14638;I;11000012' +
            '2922;1520;1;0;6;443'
          
            'UP_244;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1099#1073#1086#1088#1075'-'#1042#1077#1097#1077#1074#1086', I;1;14731;I;110000122953;1520' +
            ';0;318;23;100'
          
            'UP_302;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1075#1072'-'#1057#1090#1077#1082#1086#1083#1100#1085#1099#1081', II;1;15211;II;110000562190;1' +
            '520;1;738;32;363'
          
            'UP_257;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1055#1088#1077#1076#1087#1086#1088#1090#1086#1074#1072#1103'-'#1051#1080#1075#1086#1074#1086', I;1;14933;I;11000012298' +
            '4;1520;1;305;5;805'
          
            'UP_269;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1099#1088#1080#1094#1072'-'#1055#1086#1089#1077#1083#1086#1082', I;1;15035;I;110000123017;152' +
            '0;2;221;8;0'
          
            'UP_272;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1080' 5 '#1080' 3 '#1091#1079#1083#1072' '#1053#1077#1074#1077#1083#1100', I;1;15038;I;1100001' +
            '23024;1520;1;0;4;379'
          
            'UP_285;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1040#1087#1072#1090#1080#1090#1099'-'#1043#1088' - '#1041#1083#1086#1082' '#1087#1086#1089#1090' 1268 '#1082#1084', II;1;15142;I' +
            'I;110000123058;1520;1;0;4;367'
          
            'UP_287;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1099#1093#1086#1076#1085#1086#1081'-'#1051#1080#1080#1085#1072#1093#1072#1084#1072#1088#1080', I;1;15144;I;1100001230' +
            '67;1520;1;898;143;658'
          
            'UP_299;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1056#1091#1095#1100#1080'-'#1055#1072#1088#1075#1086#1083#1086#1074#1086', I;1;15208;I;110000123100;15' +
            '20;4;750;16;224'
          
            'UP_315;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1072#1085#1077#1074#1089#1082#1080#1081' '#1087#1086#1089#1090' II - '#1056#1078#1077#1074#1082#1072', I;1;15233;I;1100' +
            '00123122;1520;4;87;10;310'
          
            'UP_356;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 2 '#1056#1078#1077#1074#1089#1082#1086#1075#1086' '#1091#1079#1083#1072', I;1;16330;I;110000' +
            '123190;1520;1;0;3;265'
          
            'UP_273;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073' ('#1054#1073#1091#1093#1086#1074#1086') - '#1052#1091#1088#1084#1072#1085#1089#1082', II;1;15101;II;1100' +
            '00123030;1520;10;203;1449;117'
          
            'UP_297;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1055#1073'-'#1089#1086#1088#1090'. '#1052#1086#1089#1082'. - '#1057#1055#1073' '#1060#1080#1085#1083'., IV;1;15206;IV;1' +
            '10000123098;1520;19;0;23;678'
          
            'UP_253;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' - '#1056#1077#1079#1077#1082#1085#1077', I;1;14901;I;11000' +
            '0122977;1520;3;412;397;100'
          
            'UP_222;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1072#1083#1103#1079#1080#1085'-'#1059#1075#1083#1080#1095', V;1;11032;V;110000122888;1520' +
            ';185;0;186;200'
          
            'UP_226;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1052#1086#1089#1082#1074#1072', II;1;14601;II;110000' +
            '122900;1520;1;0;650;307'
          
            'UP_250;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1090#1083#1099'-'#1059#1089#1090#1100'-'#1051#1091#1075#1072', 1'#1057';1;14831;1'#1057';110000123224;' +
            '1520;21;494;27;320'
          
            'UP_302;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1075#1072'-'#1057#1090#1077#1082#1086#1083#1100#1085#1099#1081', I;1;15211;I;110000123105;152' +
            '0;0;193;33;805'
          
            'UP_327;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1062#1074#1077#1090#1086#1095#1085#1072#1103' - '#1041#1088#1086#1085#1077#1074#1072#1103', I;1;15248;I;1100001231' +
            '35;1520;1;0;2;215'
          
            'UP_226;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075'-'#1052#1086#1089#1082#1074#1072', IV;1;14601;IV;A14496' +
            '95;1520;11;536;650;24'
          
            'UP_335;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1086#1081#1090#1086#1083#1086#1074#1082#1072'-'#1043#1086#1088#1099', I;1;15256;I;110000123153;15' +
            '20;1;901;8;932'
          
            'UP_221;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1052#1075#1072'-'#1054#1074#1080#1085#1080#1097#1077', I;1;11002;I;110000122879;1520;0' +
            ';169;399;0'
          
            'UP_239;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1042#1077#1090#1074#1100' N 7 '#1091#1079#1083#1072' '#1041#1086#1083#1086#1075#1086#1077', I;1;14646;I;11000012' +
            '2934;1520;0;918;1;893'
          
            'UP_242;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1047#1077#1083#1077#1085#1086#1075#1086#1088#1089#1082'-'#1042#1099#1073#1086#1088#1075', I;1;14703;I;110000122949' +
            ';1520;52;580;167;216'
          
            'UP_250;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1090#1083#1099'-'#1059#1089#1090#1100'-'#1051#1091#1075#1072', I;1;14831;I;110000122972;15' +
            '20;0;684;21;493'
          
            'UP_252;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1050#1086#1089#1082#1086#1083#1086#1074#1086'-'#1051#1091#1078#1089#1082#1072#1103', 1;1;14834;1;A18980349;152' +
            '0;3;764;21;259'
          
            'UP_1210;'#1043#1083#1072#1074#1085#1099#1081' '#1087#1091#1090#1100' '#1051#1086#1089#1077#1074#1086' I - '#1050#1072#1084#1077#1085#1085#1086#1075#1086#1088#1089#1082', 2;1;15201;2;A22536' +
            '343;1520;1;0;62;427')
        TabOrder = 0
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'DIRECTION Only Name'
      ImageIndex = 2
      object mDIRECTION_OnlyName: TMemo
        Left = 0
        Top = 0
        Width = 966
        Height = 736
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object mDebug: TMemo
    Left = 502
    Top = 62
    Width = 403
    Height = 131
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 320
    Top = 344
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '*.xml'
    FileName = '*.xml'
    Filter = '*.xml'
    Left = 704
    Top = 400
  end
  object IdFTP1: TIdFTP
    IPVersion = Id_IPv4
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 224
    Top = 208
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.csv'
    FileName = '*.csv'
    Filter = '*.csv'
    Left = 648
    Top = 328
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 480
    Top = 384
  end
end
