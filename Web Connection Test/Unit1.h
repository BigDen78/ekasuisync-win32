
//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <System.NetEncoding>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <IWBaseControl.hpp>
#include <IWBaseHTMLControl.hpp>
#include <IWCompCheckbox.hpp>
#include <IWControl.hpp>
#include <IWDBStdCtrls.hpp>
#include <IWVCLBaseControl.hpp>
#include <Soap.InvokeRegistry.hpp>
#include <Soap.Rio.hpp>
#include <Soap.SOAPHTTPClient.hpp>
#include <Soap.SOAPHTTPTrans.hpp>
#include <Vcl.ComCtrls.hpp>
#include <IdIOHandler.hpp>
#include <IdIOHandlerSocket.hpp>
#include <IdIOHandlerStack.hpp>
#include <IdSSL.hpp>
#include <IdSSLOpenSSL.hpp>
#include <IdURI.hpp>
#include <Vcl.Dialogs.hpp>
#include <VCLTee.Chart.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.TeeShape.hpp>
#include "pies.h"
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdFTP.hpp>
#include <Vcl.AppEvnts.hpp>
#include <Vcl.Grids.hpp>
#include <memory>
#include "EKASUIDataUnit.h"
#include "ZipFilesUnit.h"
#include "TypesUnit.h"
#include <cfloat>

struct dataItem
{
	float start;
	float end;
	int startKm;
	float startMetr;
	int endKm;
	float endMetr;
	float len;
	bool left;
	bool inSwitch;
	float temperature;

	dataItem() {
		start  = 0;
		end = 0;
		len = 0;
		left = true;
		inSwitch = false;
		temperature = 0;
	}
};

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTimer *Timer1;
	TOpenDialog *OpenDialog;
	TPageControl *PageControl;
	TTabSheet *TabSheet1;
	TTabSheet *TabSheet2;
	TPanel *Panel1;
	TBevel *Bevel1;
	TBevel *Bevel2;
	TBevel *Bevel3;
	TBevel *Bevel4;
	TLabel *Label1;
	TLabel *Label2;
	TComboBox *ComboBox_SD;
	TComboBox *ComboBox_SD_2;
	TComboBox *ComboBox_BPD;
	TComboBox *ComboBox1;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TButton *Button1;
	TComboBox *cbURL;
	TButton *Button2;
	TCheckBox *cbSelectFile;
	TComboBox *cbUrlIdx;
	TComboBox *cbFileIdx;
	TMemo *LogMemo;
	TButton *Button3;
	TPanel *Panel2;
	TButton *Button4;
	TPanel *Panel3;
	TScrollBar *ScrollBar1;
	TChart *Chart1;
	TChartShape *Series1_;
	TChartShape *Series2_;
	TComboBox *cbFileName;
	TPanel *Panel4;
	TSplitter *Splitter1;
	TSplitter *Splitter2;
	TSplitter *Splitter3;
	TPanel *Panel5;
	TPanel *Panel6;
	TMemo *Memo3;
	TPie *Pie1;
	TPanel *Panel7;
	TPanel *Panel8;
	TPie *Pie2;
	TMemo *Memo2;
	TPanel *Panel9;
	TPanel *Panel10;
	TPie *Pie3;
	TMemo *Memo1;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TIdFTP *IdFTP1;
	TTabSheet *tsCutting;
	TOpenDialog *OpenDialog1;
	TTabSheet *tsDIRECTION;
	TMemo *mDIRECTIONMemo;
	TTabSheet *TabSheet6;
	TMemo *mDIRECTION_OnlyName;
	TTabSheet *tsPUT_GL;
	TMemo *mPUT_GL;
	TScrollBox *ScrollBox;
	TPanel *pLavel2;
	TButton *bSelectFile;
	TPanel *pLavel1;
	TPanel *pLavel3;
	TPanel *pLavel4;
	TButton *Button14;
	TPanel *Panel12;
	TPanel *Panel13;
	TLabel *lCuttingFileInfo;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel16;
	TLabel *lText3;
	TComboBox *eDIRECTION;
	TListBox *lbDIRECTION;
	TListBox *lbPUT_GL;
	TPanel *Panel17;
	TLabel *lText4;
	TPanel *Panel11;
	TButton *bNext1;
	TPanel *Panel18;
	TButton *Button7;
	TPanel *Panel19;
	TLabel *lText1;
	TLabel *lText2;
	TPageControl *pcData;
	TTabSheet *tsText;
	TTabSheet *tsGrapfics;
	TChart *ShowChart;
	TChartShape *ChartShape1;
	TChartShape *ChartShape2;
	TApplicationEvents *ApplicationEvents1;
	TStringGrid *sgRubki;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *lText5;
	TLabel *lSDText1;
	TLabel *lOpenFile;
	TMemo *mDebug;
	TMemo *mFTPLog;
	TLabel *lCuttingFileName;
	TPanel *Panel20;
	TPanel *Panel21;
	TButton *bExit;
	void __fastcall IdHTTP1Work(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount);
	void __fastcall IdHTTP1WorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax);
	void __fastcall IdHTTP1WorkEnd(TObject *ASender, TWorkMode AWorkMode);
	void __fastcall IdHTTP1Status(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText);
	System::UnicodeString __fastcall GetDataFileName(const System::UnicodeString Name, const System::UnicodeString Ext);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Chart1GetLegendRect(TCustomChart *Sender, TRect &Rect);
	void __fastcall Series1_Click(TChartSeries *Sender, int ValueIndex, TMouseButton Button,
          TShiftState Shift, int X, int Y);
	void __fastcall Series2_Click(TChartSeries *Sender, int ValueIndex, TMouseButton Button,
		  TShiftState Shift, int X, int Y);
	void __fastcall Series3_Click(TChartSeries *Sender, int ValueIndex, TMouseButton Button,
		  TShiftState Shift, int X, int Y);
	void __fastcall Panel4Resize(TObject *Sender);
	void __fastcall bSelectFileClick(TObject *Sender);
	void __fastcall eDIRECTIONChange(TObject *Sender);
	void __fastcall lbDIRECTIONClick(TObject *Sender);
	void __fastcall lbPUT_GLClick(TObject *Sender);
	void __fastcall bNext1Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall pLavel2Resize(TObject *Sender);
	void __fastcall ChartShape1GetMarkText(TChartSeries *Sender, int ValueIndex, UnicodeString &MarkText);
	void __fastcall ApplicationEvents1Message(tagMSG &Msg, bool &Handled);
	void __fastcall lbPUT_GLDblClick(TObject *Sender);
	void __fastcall bExitClick(TObject *Sender);



//	void  __fastcall Button6Click(TChartSeries* Sender, int ValueIndex, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
//	void __fastcall Button6Click(TObject *Sender);
//	void __fastcall Button5Click(TObject *Sender);

//TChartSeries* Sender, int ValueIndex, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y
//typedef void __fastcall (__closure *TSeriesClick)(TChartSeries* Sender, int ValueIndex, System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);





private:	// User declarations

	bool FileExists_(UnicodeString FileName);

	TStringList* DIRECTION_OnlyName;
	TStringList* PUT_GL_OnlyName;
	UnicodeString FileName__;
	UnicodeString PathId_;
	UnicodeString proezd_DataTime;
	int month_;
	int year_;

	WebServiceEKASUI* EKASUI;
	System::UnicodeString LogFileName;
	System::UnicodeString DataFileName;
	int DataFileNumber;
	int BPDLoaded;
	static void OnTextToLog(UnicodeString Text);

	std::vector <dataItem> A03Items;
	std::vector <dataItem> RZDItems;
	std::vector <dataItem> RZD2Items;

	int StartLine_;
	TStringList* data_;


public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern cConfig* Config_;
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

