//---------------------------------------------------------------------------

#ifndef TestUnitH
#define TestUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>

#include "ZModemUnit_.h"
#include "nrcommbox.hpp"
#include <Vcl.ExtCtrls.hpp>
#include "nrclasses.hpp"
#include "nrcomm.hpp"
#include <Vcl.Dialogs.hpp>
#include <Vcl.Grids.hpp>

//---------------------------------------------------------------------------
class TTestForm : public TForm
{
__published:	// IDE-managed Components
	TPageControl *PageControl1;
	TTabSheet *TestTabSheet;
	TTabSheet *ZModemTabSheet;
	TPanel *TopPanel;
	TnrDeviceBox *nrDeviceBox1;
	TnrComm *nrComm1;
	TButton *Button1;
	TOpenDialog *OpenDialog1;
	TPanel *Panel2;
	TPanel *Panel3;
	TButton *Button2;
	TStringGrid *StringGrid1;
	TTimer *Timer1;
	TLabel *StateLabel;
	TTabSheet *TabSheet1;
	TMemo *LogMemo;
	TPanel *Panel1;
	TPanel *Panel4;
	TStringGrid *StringGrid2;
	TComboBox *PCMode;
	TPanel *Panel5;
	TPanel *Panel6;
	TButton *SentButton;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall TestTabSheetResize(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall SentButtonClick(TObject *Sender);
private:	// User declarations

	int TestCount;
	int SendIdx;
	static void OnTransmissionProgress(tTransmissionState State, UnicodeString Fn, int Tag, int Progress, float Speed);
	static void OnFileReceived(UnicodeString Fn);
	static void OnTextToLog(UnicodeString Text);

public:		// User declarations
	__fastcall TTestForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TTestForm *TestForm;
//---------------------------------------------------------------------------
#endif
