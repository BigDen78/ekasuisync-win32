﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TestUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "nrcommbox"
#pragma link "nrclasses"
#pragma link "nrcomm"
#pragma resource "*.dfm"
TTestForm *TestForm;
//---------------------------------------------------------------------------
__fastcall TTestForm::TTestForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TTestForm::FormShow(TObject *Sender)
{
	if (TestForm->Tag == 0) {

		ZModem->Parent = ZModemTabSheet;
		ZModem->Align = alClient;
		ZModem->BorderStyle = bsNone;
		ZModem->Visible = true;

		nrDeviceBox1->ItemIndex = 0;

		TestForm->Tag = 1;
	}

	TestForm->BringToFront();
	TestForm->Show();

}
//---------------------------------------------------------------------------

void __fastcall TTestForm::Button1Click(TObject *Sender)
{
	ZModem->setLog(OnTextToLog);
	if (PCMode->ItemIndex == 0)	ZModem->Open(nrDeviceBox1->Text, pcmMaster);
						   else ZModem->Open(nrDeviceBox1->Text, pcmSlave);
	ZModem->SetReceivedDataPath(ExtractFilePath(Application->ExeName) + "\\ReceiveData\\");
	ZModem->setCallback_TransmissionProgress(OnTransmissionProgress);
	ZModem->setCallback_FileReceived(OnFileReceived);
}
//---------------------------------------------------------------------------

void __fastcall TTestForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	ZModem->Close();
}
//---------------------------------------------------------------------------

void __fastcall TTestForm::Button2Click(TObject *Sender)
{
	OpenDialog1->InitialDir = ExtractFilePath(Application->ExeName) + "SendData";
//	OpenDialog1->InitialDir = "D:\WORK\EKASUI\DTerminal-2\Win32\Debug\SendData";
	if (OpenDialog1->Execute()) {

		for (int idx = 0; idx < OpenDialog1->Files->Count; idx++) {

			StringGrid1->Tag = StringGrid1->Tag + 1;
			if (StringGrid1->Tag > StringGrid1->RowCount) StringGrid1->RowCount = StringGrid1->Tag;
			StringGrid1->Cells[0][StringGrid1->Tag - 1] = /*ExtractFileName(*/OpenDialog1->Files->Strings[idx]/*)*/;
			StringGrid1->Cells[2][StringGrid1->Tag - 1] = "-";
			if (PCMode->ItemIndex == 0)
				ZModem->SendFile(OpenDialog1->Files->Strings[idx], StringGrid1->Tag - 1, false);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TTestForm::TestTabSheetResize(TObject *Sender)
{
	Panel2->Height = (TestTabSheet->ClientHeight - TopPanel->Height) / 2;

	Panel2->Width = TestTabSheet->Width / 2;
}
//---------------------------------------------------------------------------

void TTestForm::OnTransmissionProgress(tTransmissionState State, UnicodeString Fn, int Tag, int Progress, float Speed)
{
	UnicodeString prg;

	if (ZModem->GetState() == zmsReceiving) {

		TestForm->TestCount++;
		if (TestForm->TestCount > 5) TestForm->TestCount = 0;
		switch (TestForm->TestCount) {

			case 0: prg = "---O"; break;
			case 1: prg = "--O-"; break;
			case 2: prg = "-O--"; break;       // ̶̶—––
			case 3: prg = "O---"; break;
			case 4: prg = "-O--"; break;       // ̶̶—––
			case 5: prg = "--O-"; break;
		}

		if (State == tsStart) {

			TestForm->StringGrid2->Tag = TestForm->StringGrid2->Tag + 1;
			if (TestForm->StringGrid2->Tag > TestForm->StringGrid2->RowCount) TestForm->StringGrid2->RowCount = TestForm->StringGrid2->RowCount + 1;
			TestForm->StringGrid2->Cells[0][TestForm->StringGrid2->Tag - 1] = ExtractFileName(Fn);
		} else
		if (State == tsEnd) {

			TestForm->StringGrid2->Cells[1][TestForm->StringGrid2->Tag - 1] = "OK";
		} else
		if (State == tsError)
			TestForm->StringGrid2->Cells[2][TestForm->StringGrid2->Tag - 1] = "Ошибка"; else
		TestForm->StringGrid2->Cells[1][TestForm->StringGrid2->Tag - 1] = Format("[%s] %d%%; %3.1f кб/с", ARRAYOFCONST((prg, Progress, Speed)));

	} else
	if (ZModem->GetState() == zmsSending) {

		TestForm->TestCount++;
		if (TestForm->TestCount > 5) TestForm->TestCount = 0;
		switch (TestForm->TestCount) {

			case 0: prg = "---O"; break;
			case 1: prg = "--O-"; break;
			case 2: prg = "-O--"; break;       // ̶̶—––
			case 3: prg = "O---"; break;
			case 4: prg = "-O--"; break;       // ̶̶—––
			case 5: prg = "--O-"; break;
		}

		if (State == tsEnd) {
			TestForm->StringGrid1->Cells[2][Tag] = "+";
			prg = "OK";
		}
		if (State == tsError)
			TestForm->StringGrid1->Cells[2][Tag] = "Ошибка"; else
		TestForm->StringGrid1->Cells[1][Tag] = Format("[%s] %d%%; %3.1f кб/с", ARRAYOFCONST((prg, Progress, Speed)));
	 //	TestForm->TestCount++;
	};
}
//---------------------------------------------------------------------------

void TTestForm::OnFileReceived(UnicodeString Fn)
 {
	TestForm->StringGrid2->Cells[2][TestForm->StringGrid2->Tag - 1] = "+";

	if (TestForm->PCMode->ItemIndex == 1) // GetFileToSent();
		TestForm->SentButton->Enabled = true;

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TTestForm::SentButtonClick(TObject *Sender)
{
		if (TestForm->SendIdx < TestForm->StringGrid1->Tag) {
/*
			if (TestForm->StringGrid1->Tag + 1 > TestForm->StringGrid1->RowCount) TestForm->StringGrid1->RowCount = TestForm->StringGrid1->Tag + 1;
			UnicodeString FN = ExtractFilePath(Application->ExeName) + "\\ReceiveData\\folders-11.png";
			if (!FileExists(FN)) ShowMessage("!!!");
			TestForm->StringGrid1->Cells[0][TestForm->StringGrid1->Tag] = FN;
			ZModem->SendFile(FN, TestForm->StringGrid1->Tag);
			TestForm->StringGrid1->Tag = TestForm->StringGrid1->Tag + 1;

			if (TestForm->StringGrid1->Tag + 1 > TestForm->StringGrid1->RowCount) TestForm->StringGrid1->RowCount = TestForm->StringGrid1->Tag + 1;
			FN = ExtractFilePath(Application->ExeName) + "\\ReceiveData\\folders-13.png";
			if (!FileExists(FN)) ShowMessage("!!!");
			TestForm->StringGrid1->Cells[0][TestForm->StringGrid1->Tag] = FN;
			ZModem->SendFile(FN, TestForm->StringGrid1->Tag);
			TestForm->StringGrid1->Tag = TestForm->StringGrid1->Tag + 1;

			if (TestForm->StringGrid1->Tag + 1 > TestForm->StringGrid1->RowCount) TestForm->StringGrid1->RowCount = TestForm->StringGrid1->Tag + 1;
			FN = ExtractFilePath(Application->ExeName) + "\\ReceiveData\\folders-14.png";
			if (!FileExists(FN)) ShowMessage("!!!");
			TestForm->StringGrid1->Cells[0][TestForm->StringGrid1->Tag] = FN;
			ZModem->SendFile(FN, TestForm->StringGrid1->Tag);
			TestForm->StringGrid1->Tag = TestForm->StringGrid1->Tag + 1;
*/

			ZModem->SendFile(TestForm->StringGrid1->Cells[0][TestForm->SendIdx], TestForm->SendIdx, 0);
			TestForm->SendIdx++;
			TestForm->SentButton->Enabled = false;
//			ZModem->SendFile(TestForm->StringGrid1->Cells[0][TestForm->SendIdx], TestForm->SendIdx, 0);
//			TestForm->SendIdx++;
		}

}
//---------------------------------------------------------------------------

void __fastcall TTestForm::Timer1Timer(TObject *Sender)
{
	switch (ZModem->GetState()) {
		case zmsClosed: StateLabel->Caption = "No Connection"; break;
		//case: zmsTestConnection: StateLabel->Caption = ""; break;
		case zmsReadyToSend: StateLabel->Caption = "Ready to Send"; break;
		case zmsSending: StateLabel->Caption = "Sending"; break;
		case zmsReceiving: StateLabel->Caption = "Receiving"; break;
		case zmsWait: StateLabel->Caption = "Wait"; break;
	}

/*	if (ZModem->GetState() == zmsReady_Idly)
		for (int i = 0; i < StringGrid1->Tag; i++) {

			if (StringGrid1->Cells[2][i].Compare("-") == 0)
				ZModem->SendFile(TestForm->StringGrid1->Cells[0][i], i);
		}*/
}
//---------------------------------------------------------------------------

void __fastcall TTestForm::FormCreate(TObject *Sender)
{
	TestCount = 0;
	SendIdx = 0;
}
//---------------------------------------------------------------------------

void TTestForm::OnTextToLog(UnicodeString Text)
{
	while (Pos("\r\n", Text, 1) != 0) {

		int pos = Pos("\r\n", Text);
		Text.Delete(pos, 2);
	}
/*
	Text = DateTimeToStr(Now()) + "| " + Text;

	std::wstring ws;
	ws.append(Text.w_str(), Text.Length());
	MainForm->log<<ws << '\n';
	MainForm->log.flush(); */
	TestForm->LogMemo->Lines->Add(Text);
}
void __fastcall TTestForm::FormResize(TObject *Sender)
{
	StringGrid1->ColWidths[0] = StringGrid1->Width - 200;
	StringGrid1->ColWidths[1] = 150;
	StringGrid1->ColWidths[2] = 50;


	StringGrid2->ColWidths[0] = StringGrid1->Width - 150;
	StringGrid2->ColWidths[1] = 150;

}
//---------------------------------------------------------------------------

