object TestForm: TTestForm
  Left = 0
  Top = 0
  Caption = 'TestForm'
  ClientHeight = 551
  ClientWidth = 610
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 610
    Height = 551
    ActivePage = TestTabSheet
    Align = alClient
    TabOrder = 0
    object TestTabSheet: TTabSheet
      Caption = 'Test'
      OnResize = TestTabSheetResize
      object TopPanel: TPanel
        Left = 0
        Top = 0
        Width = 602
        Height = 49
        Align = alTop
        TabOrder = 0
        object StateLabel: TLabel
          Left = 262
          Top = 16
          Width = 30
          Height = 13
          Caption = 'State:'
        end
        object nrDeviceBox1: TnrDeviceBox
          Left = 85
          Top = 13
          Width = 72
          Height = 21
          nrComm = nrComm1
          ResetOnChanged = False
          TabOrder = 0
          Text = 'COM1'
        end
        object Button1: TButton
          Left = 4
          Top = 11
          Width = 75
          Height = 25
          Caption = 'Connect'
          TabOrder = 1
          OnClick = Button1Click
        end
        object PCMode: TComboBox
          Left = 176
          Top = 13
          Width = 73
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 2
          Text = 'MASTER'
          Items.Strings = (
            'MASTER'
            'SLAVE')
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 49
        Width = 305
        Height = 474
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Panel3: TPanel
          Left = 0
          Top = 15
          Width = 305
          Height = 32
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Button2: TButton
            Left = 4
            Top = 3
            Width = 75
            Height = 25
            Caption = 'Add...'
            TabOrder = 0
            OnClick = Button2Click
          end
          object SentButton: TButton
            Left = 85
            Top = 3
            Width = 75
            Height = 25
            Caption = 'Send'
            Enabled = False
            TabOrder = 1
            OnClick = SentButtonClick
          end
        end
        object StringGrid1: TStringGrid
          Left = 0
          Top = 47
          Width = 305
          Height = 427
          Align = alClient
          ColCount = 3
          DefaultColWidth = 160
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 305
          Height = 15
          Align = alTop
          Caption = #1055#1077#1088#1077#1076#1072#1095#1072' '#1092#1072#1081#1083#1086#1074
          TabOrder = 2
        end
      end
      object Panel1: TPanel
        Left = 305
        Top = 49
        Width = 297
        Height = 474
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object Panel4: TPanel
          Left = 0
          Top = 15
          Width = 297
          Height = 32
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
        end
        object StringGrid2: TStringGrid
          Left = 0
          Top = 47
          Width = 297
          Height = 427
          Align = alClient
          ColCount = 2
          DefaultColWidth = 160
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 297
          Height = 15
          Align = alTop
          Caption = #1055#1088#1080#1077#1084' '#1092#1072#1081#1083#1086#1074
          TabOrder = 2
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Log'
      ImageIndex = 2
      object LogMemo: TMemo
        Left = 0
        Top = 0
        Width = 602
        Height = 523
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object ZModemTabSheet: TTabSheet
      Caption = 'ZModem'
      ImageIndex = 1
    end
  end
  object nrComm1: TnrComm
    Active = False
    BaudRate = 115200
    Parity = pNone
    StopBits = sbOne
    ByteSize = 8
    ComPortNo = 1
    ComPort = cpCOM1
    TraceStates = [tsRxChar]
    EventChar = #0
    StreamProtocol = spHardware
    BufferInSize = 40960
    BufferOutSize = 40960
    TimeoutRead = 0
    TimeoutWrite = 1000
    RS485Mode = False
    EnumPorts = epQuickAll
    UseMainThread = True
    KeepConnection = False
    TerminalUsage = tuBoth
    TerminalEcho = True
    Left = 328
    Top = 160
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.*'
    FileName = '*.*'
    Filter = '*.*'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
    Left = 392
    Top = 160
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 456
    Top = 160
  end
end
