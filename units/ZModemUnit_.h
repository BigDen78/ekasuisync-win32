//---------------------------------------------------------------------------
#ifndef ZModemUnit_H
#define ZModemUnit_H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "nrclasses.hpp"
#include "nrcomm.hpp"
#include "nrcommbox.hpp"
#include "nrlogfile.hpp"
#include "nrzmodem.hpp"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
#include "nrsemaphore.hpp"
#include <Vcl.ImgList.hpp>

#include "TypesUnit.h"
#include <Data.DB.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.Win.MConnect.hpp>
#include <Datasnap.Win.SConnect.hpp>

//#define DEBUG

//---------------------------------------------------------------------------

const zmMaxError = 5;

enum tFileSendState {

	fssNotSet = -1,
	fssReady = 0,
	fssTransferred = 1,
	fssError = 1
};

enum tPCMode {

	pcmMaster = 0,
	pcmSlave = 1
};

struct tFileSendData {

	tFileSendData(): State(fssNotSet),
					 FileName(""),
					 Tag(0),
					 ErrorCount(zmMaxError),
					 ResponseFile(false),
					 DeleteAfterSend(false) {};

	tFileSendState State; // ������ ����������?
	UnicodeString FileName;
	bool ResponseFile;
	bool DeleteAfterSend;
	int ErrorCount;
	int Tag;
};

class TZModem : public TForm
{
__published:	// IDE-managed Components
	TStatusBar *StatusBar1;
    TPanel *Panel1;
    TLabel *Label1;
    TMemo *Memo1;
    TLabel *Label2;
	TMemo *Memo2;
    TnrComm *nrComm1;
    TnrDeviceBox *nrDeviceBox1;
    TnrLogFile *nrLogFile1;
	TLabel *Label3;
    TnrZModem *nrZModem1;
	TOpenDialog *OpenDialog1;
    TCheckBox *chkActive;
	TButton *Button2;
    TImageList *ImageList1;
	TnrSemaphore *nrSemaphore1;
    TnrSemaphore *nrSemaphore2;
	TnrSemaphore *nrSemaphore3;
	TnrSemaphore *nrSemaphore4;
	TPanel *Panel2;
	TGroupBox *GroupBox1;
	TLabel *Label4;
	TLabel *Label5;
	TProgressBar *ProgressBar1;
	TCheckBox *chkReceive;
	TButton *bBreak;
	TButton *bSend;
	TButton *Button1;
	TEdit *Edit1;
	TLabel *Label6;
	TLabel *Label7;
	TTimer *WorkTimer;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall chkActiveClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall chkReceiveClick(TObject *Sender);
    void __fastcall bSendClick(TObject *Sender);
	void __fastcall bBreakClick(TObject *Sender);
	void __fastcall nrZModem1AfterFileSent(TObject *Sender, UnicodeString FileName, bool flComplete);
	void __fastcall nrZModem1SuccessfulEnd(TObject *Sender);
	void __fastcall nrZModem1Abort(TObject *Sender, TzAbortType Error);
	void __fastcall nrZModem1Progress(TObject *Sender);
	void __fastcall nrZModem1BeforeReceived(TObject *Sender, UnicodeString &FileName,
		  bool &flSkip);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall nrZModem1AfterFileReceived(TObject *Sender, UnicodeString FileName);
	void __fastcall Memo2Change(TObject *Sender);
	void __fastcall nrZModem1TimeOut(TObject *Sender);
	void __fastcall WorkTimerTimer(TObject *Sender);
	void __fastcall nrZModem1FatalError(TObject *Sender, DWORD ErrorCode, DWORD Detail, UnicodeString ErrorMsg, bool &RaiseException);
	void __fastcall nrZModem1CRCError(TObject *Sender);


private:	// User declarations

	tPCMode PCMode;
	tZModemState_ State;
	std::vector<tFileSendData> FileSendData;
	UnicodeString ReceiveFileName;
	long WaitForReceivingTime;

	int FileSendData_idx;
	void (*callback_TransmissionProgress)(tTransmissionState State, UnicodeString Fn, int Tag, int Progress, float Speed);
	void (*callback_FileReceived)(UnicodeString Fn);
	void (*callback_FileTransferred)(UnicodeString Fn, int Tag);
	void (*callback_WaitForReceivingTimeOut)(void);
	void (*callback_Error)(void);
	void (*AddToLog)(UnicodeString Text);
	void Error(UnicodeString ErrorText);
	bool SendFile(UnicodeString FileName, int Tag);

public:		// User declarations
	long SendTick;
	__fastcall TZModem(TComponent* Owner);

	bool Open(UnicodeString ComPortName, tPCMode PCMode_);
	void Close(void);
	tZModemState_ GetState(void);
	void AddToSendQueue(UnicodeString FileName, int Tag, bool DeleteAfterSend, bool ResponseFile = false);
	void SetReceivedDataPath(UnicodeString Path);
	void setCallback_TransmissionProgress(void (*func)(tTransmissionState State, UnicodeString Fn, int Tag, int Progress, float Speed));
	void setCallback_FileReceived(void (*func)(UnicodeString Fn));
	void setCallback_FileTransferred(void (*func)(UnicodeString Fn, int Tag));
	void setCallback_WaitForReceivingTimeOut(void (*func)(void));
	void setCallback_Error(void (*func)(void));
	void setLog(void (*func)(UnicodeString Text));
};
//---------------------------------------------------------------------------
extern PACKAGE TZModem *ZModem;
//extern const bool ExLOG;
//---------------------------------------------------------------------------
#endif
