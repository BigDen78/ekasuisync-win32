// ? ��������� ����� ����� ������� � Windows
// - ����� ����� ��� � ��� ������?
// - �������� ����������
// + ��� �������

//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ZModemUnit_.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "nrclasses"
#pragma link "nrcomm"
#pragma link "nrcommbox"
#pragma link "nrlogfile"
#pragma link "nrzmodem"
#pragma link "nrsemaphore"
#pragma resource "*.dfm"
TZModem *ZModem;
//---------------------------------------------------------------------------
__fastcall TZModem::TZModem(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TZModem::Button1Click(TObject *Sender)
{
//  if(OpenDialog1->Execute()){
//	Edit1->Text=OpenDialog1->FileName;
//  }
}
//---------------------------------------------------------------------------

void __fastcall TZModem::Button2Click(TObject *Sender)
{
  nrComm1->ConfigDialog();
}
//---------------------------------------------------------------------------

void __fastcall TZModem::Memo2Change(TObject *Sender)
{
	Memo2->SelStart = Memo2->Text.Length() - 1;
	Memo2->SelLength = 0;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::chkActiveClick(TObject *Sender)
{
  nrComm1->Active=chkActive->Checked;
  bSend->Enabled=nrComm1->Active;
  bBreak->Enabled=false;

  if (PCMode == pcmMaster)
  {
	#ifdef DEBUG
	 if (AddToLog) AddToLog("State = Ready To Send");
	#endif
	 State = zmsReadyToSend;
  }
  else
  {
	#ifdef DEBUG
	 if (AddToLog) AddToLog("State = Wait");
	#endif
	 State = zmsWait;
  }
}
//---------------------------------------------------------------------------

void __fastcall TZModem::FormShow(TObject *Sender)
{
//	Form1->Caption = Application->ExeName;
  chkActive->Checked=nrComm1->Active;
  chkReceive->Checked=nrZModem1->Active;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::chkReceiveClick(TObject *Sender)
{
   nrZModem1->Active=chkReceive->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::bSendClick(TObject *Sender)
{
/*
  OpenDialog1->InitialDir = ExtractFilePath(Application->ExeName);
  if(OpenDialog1->Execute()){

	  Edit1->Text=OpenDialog1->FileName;
	  Label5->Caption="Sending ...";

	  State = zmsSending;

	  nrZModem1->Send(Edit1->Text);
	  SendTick = GetTickCount();
	  bSend->Enabled=false;
	  bBreak->Enabled=true;
	  Label4->Caption=nrZModem1->FileName;
  } */
}
//---------------------------------------------------------------------------

void __fastcall TZModem::bBreakClick(TObject *Sender)
{
  Label5->Caption="Aborted";
  if (PCMode == pcmMaster)
  {
	#ifdef DEBUG
	 if (AddToLog) AddToLog("State = Ready To Send");
	#endif
	 State = zmsReadyToSend;
  }
  else
  {
	#ifdef DEBUG
	 if (AddToLog) AddToLog("State = Wait");
	#endif
	 State = zmsWait;
  }


  nrZModem1->Abort();
  bBreak->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1SuccessfulEnd(TObject *Sender)
{
  Label5->Caption="Ready";
//  State = zmsReady_Idly;
  bSend->Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1Abort(TObject *Sender, TzAbortType Error)
{
  bSend->Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1Progress(TObject *Sender)
{
  Label4->Caption = IntToStr(ProgressBar1->Position);

  long DeltaTick = GetTickCount() - SendTick;
  if (DeltaTick != 0)
	Label7->Caption = Format("%3.1f ��/�", ARRAYOFCONST((((float)ProgressBar1->Position / (float)1024) / ((float)DeltaTick / (float)1000))));

  if (State == zmsSending) {

	  if ((FileSendData_idx != - 1) && (FileSendData_idx < (int)FileSendData.size()))
		  if (callback_TransmissionProgress) {

			int Progress = (int)((float)100 * (float)ProgressBar1->Position / (float)ProgressBar1->Max);
			float Speed = 0;
			if (DeltaTick != 0) Speed = ((float)ProgressBar1->Position / (float)1024) / ((float)DeltaTick / (float)1000);
			callback_TransmissionProgress(tsInProcess, FileSendData[FileSendData_idx].FileName, FileSendData[FileSendData_idx].Tag, Progress, Speed);
		  }
  }
  else
  if (State == zmsReceiving)
	  if (callback_TransmissionProgress) {

		int Progress = (int)((float)100 * (float)ProgressBar1->Position / (float)ProgressBar1->Max);
		float Speed = 0;
		if (DeltaTick != 0) Speed = ((float)ProgressBar1->Position / (float)1024) / ((float)DeltaTick / (float)1000);
		callback_TransmissionProgress(tsInProcess, ReceiveFileName, -1, Progress, Speed);

	  }
}
//---------------------------------------------------------------------------

void __fastcall TZModem::FormCreate(TObject *Sender)
{
	nrZModem1->DefaultPath = ExtractFilePath(Application->ExeName);
	FileSendData_idx = - 1;
	callback_TransmissionProgress = NULL;
	callback_FileReceived = NULL;
	callback_FileTransferred = NULL;
	callback_WaitForReceivingTimeOut = NULL;
	callback_Error = NULL;
	State = zmsClosed;
	AddToLog = NULL;
	ReceiveFileName = "";
}
//---------------------------------------------------------------------------

bool TZModem::Open(UnicodeString ComPortName, tPCMode PCMode_)
{
	PCMode = PCMode_;
	int CPIdx;
	if (TryStrToInt(ComPortName.SubString(4, 2), CPIdx)) nrComm1->ComPortNo = CPIdx; else return false;

	CPIdx = nrDeviceBox1->Items->IndexOf(ComPortName);
	if (CPIdx != - 1) nrDeviceBox1->ItemIndex = CPIdx; // else return false;
	nrDeviceBox1->OnClick;

	chkActive->Checked = true;

	DCB dcb;                //��������� ��� ����� ������������� ����� DCB

	int tmp =  nrComm1->Handle;

		 //��������� ��������� DCB � ����
	if(!GetCommState((HANDLE)tmp, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
	{
		if (AddToLog) AddToLog("�� ������� ������ ��������� �����: " + nrDeviceBox1->Text);
		return false;
	}
/*
	Memo1->Lines->Add(Format("BaudRate: %d", ARRAYOFCONST((dcb.BaudRate))));
	Memo1->Lines->Add(Format("ByteSize: %d", ARRAYOFCONST((dcb.ByteSize))));
	Memo1->Lines->Add(Format("Parity: %d", ARRAYOFCONST((dcb.Parity))));
	Memo1->Lines->Add(Format("StopBits: %d", ARRAYOFCONST((dcb.StopBits))));
*/
	//������������� ��������� DCB
	dcb.BaudRate = 115200;       //����� �������� �������� 115200 ���
	dcb.fBinary = TRUE;                                    //�������� �������� ����� ������
	dcb.Parity = 0;                                        //��������� �������� ��������
	dcb.fNull = FALSE;                                     //��������� ���� ������� ������
	dcb.fAbortOnError = FALSE;                             //��������� ��������� ���� �������� ������/������ ��� ������
	dcb.ByteSize = 8;                                      //����� 8 ��� � �����
	dcb.StopBits = 0;                                      //����� ���� ����-���

//fOutxCtsFlow fOutxDsrFlow fDtrControl fRtsControl fDsrSensitivity

	// ���������� ������� - ����������;
	dcb.fOutxCtsFlow = TRUE;                               //�������� ����� �������� �� �������� CTS
	dcb.fOutxDsrFlow = TRUE;                               //�������� ����� �������� �� �������� DSR
	dcb.fDtrControl = DTR_CONTROL_ENABLE;                  //�������� ������������� ����� DTR
	dcb.fRtsControl = RTS_CONTROL_ENABLE;                  //�������� ������������� ����� RTS
	dcb.fDsrSensitivity = TRUE;                            //�������� ��������������� �������� � ��������� ����� DSR

	if(!SetCommState((HANDLE)tmp, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
	{
		if (AddToLog) AddToLog("�� ������� ������ ��������� �����: " + nrDeviceBox1->Text);
		return false;
	} else if (AddToLog) AddToLog("������ ����: " + nrDeviceBox1->Text);

	WorkTimer->Enabled = true;
	return true;

/*
	if(!GetCommState((HANDLE)tmp, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
	{
	   ShowMessage("�� ������� �������� DCB");
	}

	Memo1->Lines->Add(Format("BaudRate: %d", ARRAYOFCONST((dcb.BaudRate))));
	Memo1->Lines->Add(Format("ByteSize: %d", ARRAYOFCONST((dcb.ByteSize))));
	Memo1->Lines->Add(Format("Parity: %d", ARRAYOFCONST((dcb.Parity))));
	Memo1->Lines->Add(Format("StopBits: %d", ARRAYOFCONST((dcb.StopBits))));
*/
}
//---------------------------------------------------------------------------

void TZModem::Close(void)
{
	chkActive->Checked = false;
	if (AddToLog) AddToLog("������ ����: " + nrDeviceBox1->Text);
//	chkActiveClick(NULL);
}
//---------------------------------------------------------------------------

void TZModem::setCallback_TransmissionProgress(void (*func)(tTransmissionState State, UnicodeString Fn, int Tag, int Progress, float Speed))
{
	callback_TransmissionProgress = func;
}
//---------------------------------------------------------------------------

void TZModem::setCallback_FileReceived(void (*func)(UnicodeString Fn))
{
	callback_FileReceived = func;
}
//---------------------------------------------------------------------------

void TZModem::setCallback_FileTransferred(void (*func)(UnicodeString Fn, int Tag))
{
	callback_FileTransferred = func;
}
//---------------------------------------------------------------------------

void TZModem::setCallback_WaitForReceivingTimeOut(void (*func)(void))
{
	callback_WaitForReceivingTimeOut = func;
}
//---------------------------------------------------------------------------

void TZModem::setCallback_Error(void (*func)(void))
{
	callback_Error = func;
}
//---------------------------------------------------------------------------

void TZModem::setLog(void (*func)(UnicodeString Text))
{
	AddToLog = func;
}
//---------------------------------------------------------------------------

void TZModem::SetReceivedDataPath(UnicodeString Path)
{
	nrZModem1->DefaultPath = Path;
}
//---------------------------------------------------------------------------

tZModemState_ TZModem::GetState(void)
{
	return State;
}
//---------------------------------------------------------------------------

void TZModem::AddToSendQueue(UnicodeString FileName, int Tag, bool DeleteAfterSend, bool ResponseFile)
{
	tFileSendData newItem;
	newItem.FileName = FileName;
	newItem.Tag = Tag;
	newItem.State = fssReady;
	newItem.ResponseFile = ResponseFile;
	newItem.DeleteAfterSend = DeleteAfterSend;

	int res_idx = - 1;
	for (int idx = 0; idx < FileSendData.size(); idx++)
		if ((FileSendData[idx].State == fssError) || (FileSendData[idx].State == fssTransferred)) {
			res_idx = idx;
			break;
		}
	if (res_idx != -1) FileSendData[res_idx] = newItem; else FileSendData.push_back(newItem);

	bool log = false;
	if (AddToLog) {
		if (GetConfig()->ExLOG) log = true;
		else log = AnalyzeFileName(FileName, NULL, NULL) != tCmdFile;
	}
	if (log) AddToLog("���� [" + FileName + ", " + IntToStr(Tag) + "] ��������� � ������ �� ��������");
}
//---------------------------------------------------------------------------

bool TZModem::SendFile(UnicodeString FileName, int Tag)
{
	if (State == zmsReadyToSend) {

		bSend->Enabled=false;
		State = zmsSending;
	   #ifdef DEBUG
		if (AddToLog) AddToLog("State = Sending");
  	   #endif

		bBreak->Enabled=true;

		Edit1->Text= FileName;
		Label5->Caption="Sending ...";

		nrZModem1->Send(FileName);
		SendTick = GetTickCount();
		Label4->Caption=nrZModem1->FileName;

		bool log = false;
		if (AddToLog) {
			if (GetConfig()->ExLOG) log = true;
			else log = AnalyzeFileName(FileName, NULL, NULL) != tCmdFile;
		}
		if (log) AddToLog("�������� �����: " + FileName);

		return true;
	} else return false;
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1BeforeReceived(TObject *Sender, UnicodeString &FileName, bool &flSkip)
{
	bSend->Enabled=false;

	Label5->Caption="Receiving ...";
	State = zmsReceiving;
	#ifdef DEBUG
	if (AddToLog) AddToLog("State = Receiving");
	#endif

	ReceiveFileName = FileName;

	Label4->Caption= FileName;

	if (callback_TransmissionProgress)
		callback_TransmissionProgress(tsStart, FileName, 0, 0, 0);

	bool log = false;
	if (AddToLog) {
		if (GetConfig()->ExLOG) log = true;
		else log = AnalyzeFileName(FileName, NULL, NULL) != tCmdFile;
	}
	if (log) AddToLog("����� �����: " + FileName);

	SendTick = GetTickCount();
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1AfterFileReceived(TObject *Sender, UnicodeString FileName)
{
	if (callback_TransmissionProgress)
		callback_TransmissionProgress(tsEnd, FileName, -1, 100, 0);
	if (callback_FileReceived) callback_FileReceived(FileName);

	bool log = false;
	if (AddToLog) {
		if (GetConfig()->ExLOG) log = true;
		else log = AnalyzeFileName(FileName, NULL, NULL) != tCmdFile;
	}
	if (log) AddToLog("����: " + FileName + ", ������");

	if (PCMode == pcmMaster) {
/*		if (FileSendData[FileSendData_idx].ResponseFile)
		{
			if (AddToLog) AddToLog("State = Wait");
			State = zmsWait;
		}
		else
		{ */
			#ifdef DEBUG
			if (AddToLog) AddToLog("State = ReadyToSend");
			#endif
			State = zmsReadyToSend;
//		}
	}

	if (PCMode == pcmSlave) {

		#ifdef DEBUG
		if (AddToLog) AddToLog("State = ReadyToSend");
		#endif
		State = zmsReadyToSend;

	}
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1AfterFileSent(TObject *Sender, UnicodeString FileName, bool flComplete)
{
	if ((FileSendData_idx != - 1) && (FileSendData_idx < (int)FileSendData.size())) {

		if (callback_TransmissionProgress)
			callback_TransmissionProgress(tsEnd, FileSendData[FileSendData_idx].FileName, FileSendData[FileSendData_idx].Tag, 100, 0);

		if (callback_FileTransferred) callback_FileTransferred(FileSendData[FileSendData_idx].FileName, FileSendData[FileSendData_idx].Tag);

		bool log = false;
		if (AddToLog) {
			if (GetConfig()->ExLOG) log = true;
			else log = AnalyzeFileName(FileName, NULL, NULL) != tCmdFile;
		}
		if (log) AddToLog("����: " + FileSendData[FileSendData_idx].FileName + ", �������");

		if (PCMode == pcmMaster)
			if (FileSendData[FileSendData_idx].ResponseFile) {

				if (log) if (AddToLog) AddToLog("� ����� ��������� ����");
				WaitForReceivingTime = GetTickCount();

				#ifdef DEBUG
				if (AddToLog) AddToLog("State = Wait");
				#endif
				State = zmsWait;
			} else
			{
				#ifdef DEBUG
				if (AddToLog) AddToLog("State = Ready To Send");
				#endif
				State = zmsReadyToSend;
			}

		FileSendData[FileSendData_idx].State = fssTransferred;
		if (FileSendData[FileSendData_idx].DeleteAfterSend) DeleteFile(FileSendData[FileSendData_idx].FileName);
		FileSendData_idx = - 1;
	}

	Label5->Caption="Ready";
	if (PCMode == pcmSlave)
	{
		State = zmsWait;
		#ifdef DEBUG
		if (AddToLog) AddToLog("State = Wait");
		#endif
	}
	bSend->Enabled=true;
}
//---------------------------------------------------------------------------

void TZModem::Error(UnicodeString ErrorText)
{
	if ((FileSendData_idx >= 0) && (FileSendData_idx < (int)FileSendData.size())) {

		bool log = false;
		if (AddToLog) {
			if (GetConfig()->ExLOG) log = true;
			else log = AnalyzeFileName(FileSendData[FileSendData_idx].FileName, NULL, NULL) != tCmdFile;
		}
		if (log) AddToLog(Format("������ (%s) �������� ����� %s, ������� %d �� %d ", ARRAYOFCONST((ErrorText, FileSendData[FileSendData_idx].FileName, zmMaxError - FileSendData[FileSendData_idx].ErrorCount + 1, zmMaxError))));

		FileSendData[FileSendData_idx].ErrorCount--;
		if (FileSendData[FileSendData_idx].ErrorCount <= 0) {

			if (callback_TransmissionProgress) {

				callback_TransmissionProgress(tsError, FileSendData[FileSendData_idx].FileName, FileSendData[FileSendData_idx].Tag, 0, 0);
				FileSendData[FileSendData_idx].State = fssError;
			}

			if (callback_Error) callback_Error();
		}
		FileSendData_idx = - 1;
	}
	bBreakClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1TimeOut(TObject *Sender)
{
	Error("Time Out");
	if (GetConfig()->ExLOG) if (AddToLog) AddToLog("ZModem Time Out");
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1FatalError(TObject *Sender, DWORD ErrorCode, DWORD Detail, UnicodeString ErrorMsg, bool &RaiseException)
{
	Error("Fatal Error");
}
//---------------------------------------------------------------------------

void __fastcall TZModem::nrZModem1CRCError(TObject *Sender)
{
	Error("CRC Error");
}
//---------------------------------------------------------------------------

void __fastcall TZModem::WorkTimerTimer(TObject *Sender)
{
	if (PCMode == pcmMaster) {

		if ((ZModem->GetState() == zmsReadyToSend) && (FileSendData_idx == - 1))
			for (int idx = 0; idx < (int)FileSendData.size(); idx++)
				if (FileSendData[idx].State == fssReady)
					if (ZModem->SendFile(FileSendData[idx].FileName, FileSendData[idx].Tag)) {

						FileSendData_idx = idx;
						break;
					}


		if (State == zmsWait) {

			if (GetTickCount() - WaitForReceivingTime >= 5000) {

				if (AddToLog) AddToLog("��������� ����� �������� ��������� �����");
				#ifdef DEBUG
				if (AddToLog) AddToLog("State = Ready To Send");
				#endif
				State = zmsReadyToSend;
				if (callback_WaitForReceivingTimeOut) callback_WaitForReceivingTimeOut();
			}
		}
	}


	if (PCMode == pcmSlave) {
		if ((ZModem->GetState() == zmsReadyToSend) /*&& (FileSendData_idx == - 1)*/) {

//			bool SendFlag = false;
			for (int idx = 0; idx < (int)FileSendData.size(); idx++)
				if (FileSendData[idx].State == fssReady)
					if (ZModem->SendFile(FileSendData[idx].FileName, FileSendData[idx].Tag)) {

						FileSendData_idx = idx;
						//SendFlag = true;
						break;
					}
//			if (!SendFlag) State = zmsWait;
		}
	}

}
//---------------------------------------------------------------------------

