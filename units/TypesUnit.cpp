﻿//---------------------------------------------------------------------------

#pragma hdrstop

#include "TypesUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//const bool ExLOG = true;

const UnicodeString CmdFileName = "cmddata_";
const UnicodeString SDFileName = "SDdata_xml.cmd";
const UnicodeString BPDFileName = "BPDdata_zip.cmd";
const UnicodeString txtWaitingDecision = "Нет решения";
const UnicodeString txtAccept = "Передавать"; // Годный инцидент нужно его выгружать
const UnicodeString txtTransferred = "+ Передан"; // Файл выгружен в web службу
const UnicodeString txtDecline = "Удалить"; // "Не отсылать";
const UnicodeString txtNotTransferred = "+ Удален"; // "[✓] Не передан";
const UnicodeString txtProcessing = "Обработка..."; // Файл принят на выгрузку с ПК LOCA на ПК ЕКАСУИ

//UnicodeString txtUnloaded = "Выгружается"; // Файл принят на ПК ЕКАСУИ и будет выгружен в web службу

UnicodeString VideoCODEtoText(UnicodeString* defect) {

	if (defect->Compare("90004000457") == 0) return "Выход подошвы рельса из реборд подкладок";
	if (defect->Compare("90004000389") == 0) return "Дефектные клеммы";
	if (defect->Compare("90004000402") == 0) return "Дефектные подкладки";
	if (defect->Compare("90004015853") == 0) return "Клемма под подошвой рельса";
	if (defect->Compare("90004000400") == 0) return "Наддернутые костыли";
	if (defect->Compare("90004000384") == 0) return "Отсутствие гаек на закладных болтах";
	if (defect->Compare("90004000395") == 0) return "Отсутствие гаек на клеммных болтах";
	if (defect->Compare("90004000409") == 0) return "Отсутствие или повреждение подрельсовой резины";
	if (defect->Compare("90004003539") == 0) return "Отсутствует скрепление";
	if (defect->Compare("90004000394") == 0) return "Отсутствуют клеммы";
	if (defect->Compare("90004000385") == 0) return "Отсутствуют закладные болты";
	if (defect->Compare("90004000397") == 0) return "Отсутствуют клеммные болты";
	if (defect->Compare("90004000405") == 0) return "Отсутствуют подкладки";
	if (defect->Compare("90004000401") == 0) return "Отсутствующие или изломанные костыли";
	if (defect->Compare("90004000478") == 0) return "Отсутствующие или изломанные шурупы";
	if (defect->Compare("90004015004") == 0) return "Дефектная деревянная шпала";
	if (defect->Compare("90004015005") == 0) return "Дефектная железобетонная шпала";
	if (defect->Compare("90004000369") == 0) return "Негодная деревянная шпала";
	if (defect->Compare("90004000374") == 0) return "Негодная железобетонная шпала";
	if (defect->Compare("90004000370") == 0) return "Отклонение от эпюрных значений укладки деревянных шпал";
	if (defect->Compare("90004000375") == 0) return "Отклонения от эпюрных значений укладки железобетонных шпал";
	if (defect->Compare("90004012058") == 0) return "Вертикальная ступенька в стыке";
	if (defect->Compare("90004012059") == 0) return "Горизонтальная ступенька в стыке";
	if (defect->Compare("90004000474") == 0) return "Изломанная или дефектная стыковая накладка";
	if (defect->Compare("90004015840") == 0) return "Нулевые зазоры";
	if (defect->Compare("90004000465") == 0) return "Отсутствие стыковых болтов";
	if (defect->Compare("90004000477") == 0) return "Отсутствует стыковая накладка";
	if (defect->Compare("90004000509") == 0) return "Отсутствующие или неисправные элементы изолирующего стыка";
	if (defect->Compare("90004012062") == 0) return "Превышение конструктивной величины стыкового зазора";
	if (defect->Compare("90004012002") == 0) return "Дефекты и повреждения подошвы рельса";
	if (defect->Compare("90004012004") == 0) return "Излом рельса";
	if (defect->Compare("90004012001") == 0) return "Поверхностный дефект рельса";
	if (defect->Compare("90004012008") == 0) return "Поперечные трещины и изломы головки рельса";
	if (defect->Compare("90004000482") == 0) return "Выплеск";
	if (defect->Compare("90004000484") == 0) return "Недостаточное количество балласта в шпальном ящике";
	if (defect->Compare("00000000000") == 0) return "Ненормативные подвижки бесстыкового пути";
	if (defect->Compare("90004015367") == 0) return "Отсутствует/нечитаемая маркировка маячных шпал";

	return *defect;
}

cConfig* Config_;

cConfig* GetConfig(void) { return Config_; }

UnicodeString NextState(UnicodeString State) {

/*	switch (State) {

		case isWaitingDecision: return isAccept;
		case isAccept: return isDecline;
		case isDecline: return isWaitingDecision;
	} */

	if (State.Compare(txtWaitingDecision) == 0) return txtAccept;
	if (State.Compare(txtAccept) == 0) return txtDecline;
	if (State.Compare(txtDecline) == 0) return txtWaitingDecision;
	if (State.Compare(txtTransferred) == 0) return txtTransferred;
	if (State.Compare(txtNotTransferred) == 0) return txtNotTransferred;
	return txtWaitingDecision;

//	if (State.Compare(txtTransferDecline)) return ;
}



UnicodeString MakeNameUnique(UnicodeString FileName) {


	if (FileName[FileName.Length() - 4] != ']')



		return ExtractFilePath(FileName) + StringReplace(ExtractFileName(FileName),ExtractFileExt(FileName), "",TReplaceFlags()) + "[1]" + ExtractFileExt(FileName);
		else {

			int closeIdx = -1;
			int openIdx = -1;
			for (int idx = FileName.Length(); idx > 0; idx--) {
				if (FileName[idx] == ']') closeIdx = idx;
				if (FileName[idx] == '[') { openIdx = idx; break; }
			}
			if ((closeIdx != -1) && (openIdx != -1)) {

				int Number = 0;
				if (TryStrToInt(FileName.SubString(openIdx + 1, closeIdx - openIdx - 1), Number)) {

					Number++;
					FileName.Delete(openIdx, closeIdx - openIdx + 1);
					UnicodeString InsertStr = "[" + IntToStr(Number) + "]";
					FileName.Insert(InsertStr, openIdx);
					return FileName;
				} return FileName;
			} return FileName;
		}
}


UnicodeString AddTagToName(UnicodeString FileName, int Tag) {

	return Format("%s%s.%d%s", ARRAYOFCONST((ExtractFilePath(FileName), StringReplace(ExtractFileName(FileName),ExtractFileExt(FileName),"",TReplaceFlags()), Tag, ExtractFileExt(FileName))));
}

tFileType AnalyzeFileName(UnicodeString FileName, UnicodeString* ResFileName, int* Tag) {

	UnicodeString FN_Tag = StringReplace(ExtractFileName(FileName),ExtractFileExt(FileName),"",TReplaceFlags());
	if (Tag) TryStrToInt(ExtractFileExt(FN_Tag).SubString(2, ExtractFileExt(FN_Tag).Length() - 1), *Tag);
	if (ResFileName) *ResFileName = ExtractFilePath(FileName) + StringReplace(ExtractFileName(FN_Tag),ExtractFileExt(FN_Tag),"",TReplaceFlags()) + ExtractFileExt(FileName);

	if (FileName.Pos(CmdFileName) != 0) return tCmdFile;
	if (FileName.Pos(SDFileName) != 0) return tSDFile;
	if (FileName.Pos(BPDFileName) != 0) return tBPDFile;
	return tIncidentFile;
}




