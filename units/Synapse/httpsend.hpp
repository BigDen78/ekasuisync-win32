﻿// CodeGear C++Builder
// Copyright (c) 1995, 2013 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'httpsend.pas' rev: 26.00 (Windows)

#ifndef HttpsendHPP
#define HttpsendHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <blcksock.hpp>	// Pascal unit
#include <synautil.hpp>	// Pascal unit
#include <synaip.hpp>	// Pascal unit
#include <synacode.hpp>	// Pascal unit
#include <synsock.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Httpsend
{
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TTransferEncoding : unsigned char { TE_UNKNOWN, TE_IDENTITY, TE_CHUNKED };

class DELPHICLASS THTTPSend;
#pragma pack(push,4)
class PASCALIMPLEMENTATION THTTPSend : public Blcksock::TSynaClient
{
	typedef Blcksock::TSynaClient inherited;
	
protected:
	Blcksock::TTCPBlockSocket* FSock;
	TTransferEncoding FTransferEncoding;
	System::UnicodeString FAliveHost;
	System::UnicodeString FAlivePort;
	System::Classes::TStringList* FHeaders;
	System::Classes::TMemoryStream* FDocument;
	System::UnicodeString FMimeType;
	System::UnicodeString FProtocol;
	bool FKeepAlive;
	int FKeepAliveTimeout;
	bool FStatus100;
	System::UnicodeString FProxyHost;
	System::UnicodeString FProxyPort;
	System::UnicodeString FProxyUser;
	System::UnicodeString FProxyPass;
	int FResultCode;
	System::UnicodeString FResultString;
	System::UnicodeString FUserAgent;
	System::Classes::TStringList* FCookies;
	int FDownloadSize;
	int FUploadSize;
	int FRangeStart;
	int FRangeEnd;
	bool FAddPortNumberToHost;
	bool __fastcall ReadUnknown(void);
	bool __fastcall ReadIdentity(int Size);
	bool __fastcall ReadChunked(void);
	void __fastcall ParseCookies(void);
	System::AnsiString __fastcall PrepareHeaders(void);
	bool __fastcall InternalDoConnect(bool needssl);
	bool __fastcall InternalConnect(bool needssl);
	
public:
	__fastcall THTTPSend(void);
	__fastcall virtual ~THTTPSend(void);
	void __fastcall Clear(void);
	void __fastcall DecodeStatus(const System::UnicodeString Value);
	bool __fastcall HTTPMethod(const System::UnicodeString Method, const System::UnicodeString URL);
	void __fastcall Abort(void);
	
__published:
	__property System::Classes::TStringList* Headers = {read=FHeaders};
	__property System::Classes::TStringList* Cookies = {read=FCookies};
	__property System::Classes::TMemoryStream* Document = {read=FDocument};
	__property int RangeStart = {read=FRangeStart, write=FRangeStart, nodefault};
	__property int RangeEnd = {read=FRangeEnd, write=FRangeEnd, nodefault};
	__property System::UnicodeString MimeType = {read=FMimeType, write=FMimeType};
	__property System::UnicodeString Protocol = {read=FProtocol, write=FProtocol};
	__property bool KeepAlive = {read=FKeepAlive, write=FKeepAlive, nodefault};
	__property int KeepAliveTimeout = {read=FKeepAliveTimeout, write=FKeepAliveTimeout, nodefault};
	__property bool Status100 = {read=FStatus100, write=FStatus100, nodefault};
	__property System::UnicodeString ProxyHost = {read=FProxyHost, write=FProxyHost};
	__property System::UnicodeString ProxyPort = {read=FProxyPort, write=FProxyPort};
	__property System::UnicodeString ProxyUser = {read=FProxyUser, write=FProxyUser};
	__property System::UnicodeString ProxyPass = {read=FProxyPass, write=FProxyPass};
	__property System::UnicodeString UserAgent = {read=FUserAgent, write=FUserAgent};
	__property int ResultCode = {read=FResultCode, nodefault};
	__property System::UnicodeString ResultString = {read=FResultString};
	__property int DownloadSize = {read=FDownloadSize, nodefault};
	__property int UploadSize = {read=FUploadSize, nodefault};
	__property Blcksock::TTCPBlockSocket* Sock = {read=FSock};
	__property bool AddPortNumberToHost = {read=FAddPortNumberToHost, write=FAddPortNumberToHost, nodefault};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define cHttpProtocol L"80"
extern DELPHI_PACKAGE bool __fastcall HttpGetText(const System::UnicodeString URL, System::Classes::TStrings* const Response);
extern DELPHI_PACKAGE bool __fastcall HttpGetBinary(const System::UnicodeString URL, System::Classes::TStream* const Response);
extern DELPHI_PACKAGE bool __fastcall HttpPostBinary(const System::UnicodeString URL, System::Classes::TStream* const Data);
extern DELPHI_PACKAGE bool __fastcall HttpPostURL(const System::UnicodeString URL, const System::UnicodeString URLData, System::Classes::TStream* const Data);
extern DELPHI_PACKAGE bool __fastcall HttpPostFile(const System::UnicodeString URL, const System::UnicodeString FieldName, const System::UnicodeString FileName, System::Classes::TStream* const Data, System::Classes::TStrings* const ResultData);
}	/* namespace Httpsend */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_HTTPSEND)
using namespace Httpsend;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// HttpsendHPP
