﻿// CodeGear C++Builder
// Copyright (c) 1995, 2013 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'synautil.pas' rev: 26.00 (Windows)

#ifndef SynautilHPP
#define SynautilHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <synafpc.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Synautil
{
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<System::UnicodeString, 12> Synautil__1;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE Synautil__1 CustomMonthNames;
extern DELPHI_PACKAGE int __fastcall TimeZoneBias(void);
extern DELPHI_PACKAGE System::UnicodeString __fastcall TimeZone(void);
extern DELPHI_PACKAGE System::UnicodeString __fastcall Rfc822DateTime(System::TDateTime t);
extern DELPHI_PACKAGE System::UnicodeString __fastcall CDateTime(System::TDateTime t);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SimpleDateTime(System::TDateTime t);
extern DELPHI_PACKAGE System::UnicodeString __fastcall AnsiCDateTime(System::TDateTime t);
extern DELPHI_PACKAGE int __fastcall GetMonthNumber(System::UnicodeString Value);
extern DELPHI_PACKAGE System::TDateTime __fastcall GetTimeFromStr(System::UnicodeString Value);
extern DELPHI_PACKAGE System::TDateTime __fastcall GetDateMDYFromStr(System::UnicodeString Value);
extern DELPHI_PACKAGE System::TDateTime __fastcall DecodeRfcDateTime(System::UnicodeString Value);
extern DELPHI_PACKAGE System::TDateTime __fastcall GetUTTime(void);
extern DELPHI_PACKAGE bool __fastcall SetUTTime(System::TDateTime Newdt);
extern DELPHI_PACKAGE unsigned __fastcall GetTick(void);
extern DELPHI_PACKAGE unsigned __fastcall TickDelta(unsigned TickOld, unsigned TickNew);
extern DELPHI_PACKAGE System::AnsiString __fastcall CodeInt(System::Word Value);
extern DELPHI_PACKAGE System::Word __fastcall DecodeInt(const System::AnsiString Value, int Index);
extern DELPHI_PACKAGE System::AnsiString __fastcall CodeLongInt(int Value);
extern DELPHI_PACKAGE int __fastcall DecodeLongInt(const System::AnsiString Value, int Index);
extern DELPHI_PACKAGE System::UnicodeString __fastcall DumpStr(const System::AnsiString Buffer);
extern DELPHI_PACKAGE System::UnicodeString __fastcall DumpExStr(const System::AnsiString Buffer);
extern DELPHI_PACKAGE void __fastcall Dump(const System::AnsiString Buffer, System::UnicodeString DumpFile);
extern DELPHI_PACKAGE void __fastcall DumpEx(const System::AnsiString Buffer, System::UnicodeString DumpFile);
extern DELPHI_PACKAGE System::UnicodeString __fastcall TrimSPLeft(const System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall TrimSPRight(const System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall TrimSP(const System::UnicodeString S);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SeparateLeft(const System::UnicodeString Value, const System::UnicodeString Delimiter);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SeparateRight(const System::UnicodeString Value, const System::UnicodeString Delimiter);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetParameter(const System::UnicodeString Value, const System::UnicodeString Parameter);
extern DELPHI_PACKAGE void __fastcall ParseParametersEx(System::UnicodeString Value, System::UnicodeString Delimiter, System::Classes::TStrings* const Parameters);
extern DELPHI_PACKAGE void __fastcall ParseParameters(System::UnicodeString Value, System::Classes::TStrings* const Parameters);
extern DELPHI_PACKAGE int __fastcall IndexByBegin(System::UnicodeString Value, System::Classes::TStrings* const List);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetEmailAddr(const System::UnicodeString Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetEmailDesc(System::UnicodeString Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall StrToHex(const System::AnsiString Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall IntToBin(int Value, System::Byte Digits);
extern DELPHI_PACKAGE int __fastcall BinToInt(const System::UnicodeString Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall ParseURL(System::UnicodeString URL, System::UnicodeString &Prot, System::UnicodeString &User, System::UnicodeString &Pass, System::UnicodeString &Host, System::UnicodeString &Port, System::UnicodeString &Path, System::UnicodeString &Para);
extern DELPHI_PACKAGE System::AnsiString __fastcall ReplaceString(System::AnsiString Value, System::AnsiString Search, System::AnsiString Replace);
extern DELPHI_PACKAGE int __fastcall RPosEx(const System::UnicodeString Sub, const System::UnicodeString Value, int From);
extern DELPHI_PACKAGE int __fastcall RPos(const System::UnicodeString Sub, const System::UnicodeString Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall FetchBin(System::UnicodeString &Value, const System::UnicodeString Delimiter);
extern DELPHI_PACKAGE System::UnicodeString __fastcall Fetch(System::UnicodeString &Value, const System::UnicodeString Delimiter);
extern DELPHI_PACKAGE System::UnicodeString __fastcall FetchEx(System::UnicodeString &Value, const System::UnicodeString Delimiter, const System::UnicodeString Quotation);
extern DELPHI_PACKAGE bool __fastcall IsBinaryString(const System::AnsiString Value);
extern DELPHI_PACKAGE int __fastcall PosCRLF(const System::AnsiString Value, System::AnsiString &Terminator);
extern DELPHI_PACKAGE void __fastcall StringsTrim(System::Classes::TStrings* const value);
extern DELPHI_PACKAGE int __fastcall PosFrom(const System::UnicodeString SubStr, const System::UnicodeString Value, int From);
extern DELPHI_PACKAGE void * __fastcall IncPoint(const void * p, int Value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetBetween(const System::UnicodeString PairBegin, const System::UnicodeString PairEnd, const System::UnicodeString Value);
extern DELPHI_PACKAGE int __fastcall CountOfChar(const System::UnicodeString Value, System::WideChar Chr);
extern DELPHI_PACKAGE System::UnicodeString __fastcall UnquoteStr(const System::UnicodeString Value, System::WideChar Quote);
extern DELPHI_PACKAGE System::UnicodeString __fastcall QuoteStr(const System::UnicodeString Value, System::WideChar Quote);
extern DELPHI_PACKAGE void __fastcall HeadersToList(System::Classes::TStrings* const Value);
extern DELPHI_PACKAGE void __fastcall ListToHeaders(System::Classes::TStrings* const Value);
extern DELPHI_PACKAGE int __fastcall SwapBytes(int Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall ReadStrFromStream(System::Classes::TStream* const Stream, int len);
extern DELPHI_PACKAGE void __fastcall WriteStrToStream(System::Classes::TStream* const Stream, System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall GetTempFile(const System::AnsiString Dir, const System::AnsiString prefix);
extern DELPHI_PACKAGE System::AnsiString __fastcall PadString(const System::AnsiString Value, int len, char Pad);
extern DELPHI_PACKAGE System::AnsiString __fastcall XorString(System::AnsiString Indata1, System::AnsiString Indata2);
extern DELPHI_PACKAGE System::UnicodeString __fastcall NormalizeHeader(System::Classes::TStrings* Value, int &Index);
extern DELPHI_PACKAGE void __fastcall SearchForLineBreak(char * &APtr, char * AEtx, /* out */ char * &ABol, /* out */ int &ALength);
extern DELPHI_PACKAGE void __fastcall SkipLineBreak(char * &APtr, char * AEtx);
extern DELPHI_PACKAGE void __fastcall SkipNullLines(char * &APtr, char * AEtx);
extern DELPHI_PACKAGE void __fastcall CopyLinesFromStreamUntilNullLine(char * &APtr, char * AEtx, System::Classes::TStrings* ALines);
extern DELPHI_PACKAGE void __fastcall CopyLinesFromStreamUntilBoundary(char * &APtr, char * AEtx, System::Classes::TStrings* ALines, const System::AnsiString ABoundary);
extern DELPHI_PACKAGE char * __fastcall SearchForBoundary(char * &APtr, char * AEtx, const System::AnsiString ABoundary);
extern DELPHI_PACKAGE char * __fastcall MatchBoundary(char * ABOL, char * AETX, const System::AnsiString ABoundary);
extern DELPHI_PACKAGE char * __fastcall MatchLastBoundary(char * ABOL, char * AETX, const System::AnsiString ABoundary);
extern DELPHI_PACKAGE System::AnsiString __fastcall BuildStringFromBuffer(char * AStx, char * AEtx);
}	/* namespace Synautil */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SYNAUTIL)
using namespace Synautil;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SynautilHPP
