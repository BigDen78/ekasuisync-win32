﻿// CodeGear C++Builder
// Copyright (c) 1995, 2013 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'blcksock.pas' rev: 26.00 (Windows)

#ifndef BlcksockHPP
#define BlcksockHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <synafpc.hpp>	// Pascal unit
#include <synsock.hpp>	// Pascal unit
#include <synautil.hpp>	// Pascal unit
#include <synacode.hpp>	// Pascal unit
#include <synaip.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Blcksock
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS ESynapseError;
#pragma pack(push,4)
class PASCALIMPLEMENTATION ESynapseError : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
private:
	int FErrorCode;
	System::UnicodeString FErrorMessage;
	
__published:
	__property int ErrorCode = {read=FErrorCode, write=FErrorCode, nodefault};
	__property System::UnicodeString ErrorMessage = {read=FErrorMessage, write=FErrorMessage};
public:
	/* Exception.Create */ inline __fastcall ESynapseError(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ESynapseError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : System::Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall ESynapseError(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ESynapseError(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ESynapseError(NativeUInt Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateResFmt */ inline __fastcall ESynapseError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall ESynapseError(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ESynapseError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ESynapseError(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ESynapseError(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ESynapseError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ESynapseError(NativeUInt Ident, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ESynapseError(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM THookSocketReason : unsigned char { HR_ResolvingBegin, HR_ResolvingEnd, HR_SocketCreate, HR_SocketClose, HR_Bind, HR_Connect, HR_CanRead, HR_CanWrite, HR_Listen, HR_Accept, HR_ReadCount, HR_WriteCount, HR_Wait, HR_Error };

typedef void __fastcall (__closure *THookSocketStatus)(System::TObject* Sender, THookSocketReason Reason, const System::UnicodeString Value);

typedef void __fastcall (__closure *THookDataFilter)(System::TObject* Sender, System::AnsiString &Value);

typedef void __fastcall (__closure *THookCreateSocket)(System::TObject* Sender);

typedef void __fastcall (__closure *THookMonitor)(System::TObject* Sender, bool Writing, const void * Buffer, int Len);

typedef void __fastcall (__closure *THookAfterConnect)(System::TObject* Sender);

typedef bool __fastcall (__closure *THookVerifyCert)(System::TObject* Sender);

typedef void __fastcall (__closure *THookHeartbeat)(System::TObject* Sender);

enum DECLSPEC_DENUM TSocketFamily : unsigned char { SF_Any, SF_IP4, SF_IP6 };

enum DECLSPEC_DENUM TSocksType : unsigned char { ST_Socks5, ST_Socks4 };

enum DECLSPEC_DENUM TSSLType : unsigned char { LT_all, LT_SSLv2, LT_SSLv3, LT_TLSv1, LT_TLSv1_1, LT_SSHv2 };

enum DECLSPEC_DENUM TSynaOptionType : unsigned char { SOT_Linger, SOT_RecvBuff, SOT_SendBuff, SOT_NonBlock, SOT_RecvTimeout, SOT_SendTimeout, SOT_Reuse, SOT_TTL, SOT_Broadcast, SOT_MulticastTTL, SOT_MulticastLoop };

class DELPHICLASS TSynaOption;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSynaOption : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	TSynaOptionType Option;
	bool Enabled;
	int Value;
public:
	/* TObject.Create */ inline __fastcall TSynaOption(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TSynaOption(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TSSLClass;

class DELPHICLASS TBlockSocket;
class PASCALIMPLEMENTATION TBlockSocket : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	THookSocketStatus FOnStatus;
	THookDataFilter FOnReadFilter;
	THookCreateSocket FOnCreateSocket;
	THookMonitor FOnMonitor;
	THookHeartbeat FOnHeartbeat;
	Synsock::TVarSin FLocalSin;
	Synsock::TVarSin FRemoteSin;
	int FTag;
	System::AnsiString FBuffer;
	bool FRaiseExcept;
	bool FNonBlockMode;
	int FMaxLineLength;
	int FMaxSendBandwidth;
	unsigned FNextSend;
	int FMaxRecvBandwidth;
	unsigned FNextRecv;
	bool FConvertLineEnd;
	bool FLastCR;
	bool FLastLF;
	bool FBinded;
	TSocketFamily FFamily;
	TSocketFamily FFamilySave;
	bool FIP6used;
	bool FPreferIP4;
	System::Classes::TList* FDelayedOptions;
	bool FInterPacketTimeout;
	Synsock::TFDSet FFDSet;
	int FRecvCounter;
	int FSendCounter;
	int FSendMaxChunk;
	bool FStopFlag;
	int FNonblockSendTimeout;
	int FHeartbeatRate;
	int __fastcall GetSizeRecvBuffer(void);
	void __fastcall SetSizeRecvBuffer(int Size);
	int __fastcall GetSizeSendBuffer(void);
	void __fastcall SetSizeSendBuffer(int Size);
	void __fastcall SetNonBlockMode(bool Value);
	void __fastcall SetTTL(int TTL);
	int __fastcall GetTTL(void);
	virtual void __fastcall SetFamily(TSocketFamily Value);
	virtual void __fastcall SetSocket(int Value);
	Synsock::TWSAData __fastcall GetWsaData(void);
	int __fastcall FamilyToAF(TSocketFamily f);
	
protected:
	int FSocket;
	int FLastError;
	System::UnicodeString FLastErrorDesc;
	System::TObject* FOwner;
	void __fastcall SetDelayedOption(TSynaOption* const Value);
	void __fastcall DelayedOption(TSynaOption* const Value);
	void __fastcall ProcessDelayedOptions(void);
	void __fastcall InternalCreateSocket(const Synsock::TVarSin &Sin);
	void __fastcall SetSin(Synsock::TVarSin &Sin, System::UnicodeString IP, System::UnicodeString Port);
	System::UnicodeString __fastcall GetSinIP(const Synsock::TVarSin &Sin);
	int __fastcall GetSinPort(const Synsock::TVarSin &Sin);
	void __fastcall DoStatus(THookSocketReason Reason, const System::UnicodeString Value);
	void __fastcall DoReadFilter(void * Buffer, int &Len);
	void __fastcall DoMonitor(bool Writing, const void * Buffer, int Len);
	void __fastcall DoCreateSocket(void);
	void __fastcall DoHeartbeat(void);
	void __fastcall LimitBandwidth(int Length, int MaxB, unsigned &Next);
	void __fastcall SetBandwidth(int Value);
	bool __fastcall TestStopFlag(void);
	virtual void __fastcall InternalSendStream(System::Classes::TStream* const Stream, bool WithSize, bool Indy);
	virtual bool __fastcall InternalCanRead(int Timeout);
	
public:
	__fastcall TBlockSocket(void);
	__fastcall TBlockSocket(System::UnicodeString Stub);
	__fastcall virtual ~TBlockSocket(void);
	void __fastcall CreateSocket(void);
	void __fastcall CreateSocketByName(const System::UnicodeString Value);
	virtual void __fastcall CloseSocket(void);
	virtual void __fastcall AbortSocket(void);
	void __fastcall Bind(System::UnicodeString IP, System::UnicodeString Port);
	virtual void __fastcall Connect(System::UnicodeString IP, System::UnicodeString Port);
	virtual void __fastcall Listen(void);
	virtual int __fastcall Accept(void);
	virtual int __fastcall SendBuffer(void * Buffer, int Length);
	virtual void __fastcall SendByte(System::Byte Data);
	virtual void __fastcall SendString(System::AnsiString Data);
	virtual void __fastcall SendInteger(int Data);
	virtual void __fastcall SendBlock(const System::AnsiString Data);
	virtual void __fastcall SendStreamRaw(System::Classes::TStream* const Stream);
	virtual void __fastcall SendStream(System::Classes::TStream* const Stream);
	virtual void __fastcall SendStreamIndy(System::Classes::TStream* const Stream);
	virtual int __fastcall RecvBuffer(void * Buffer, int Length);
	virtual int __fastcall RecvBufferEx(void * Buffer, int Len, int Timeout);
	virtual System::AnsiString __fastcall RecvBufferStr(int Len, int Timeout);
	virtual System::Byte __fastcall RecvByte(int Timeout);
	virtual int __fastcall RecvInteger(int Timeout);
	virtual System::AnsiString __fastcall RecvString(int Timeout);
	virtual System::AnsiString __fastcall RecvTerminated(int Timeout, const System::AnsiString Terminator);
	virtual System::AnsiString __fastcall RecvPacket(int Timeout);
	virtual System::AnsiString __fastcall RecvBlock(int Timeout);
	virtual void __fastcall RecvStreamRaw(System::Classes::TStream* const Stream, int Timeout);
	void __fastcall RecvStreamSize(System::Classes::TStream* const Stream, int Timeout, int Size);
	virtual void __fastcall RecvStream(System::Classes::TStream* const Stream, int Timeout);
	virtual void __fastcall RecvStreamIndy(System::Classes::TStream* const Stream, int Timeout);
	virtual int __fastcall PeekBuffer(void * Buffer, int Length);
	virtual System::Byte __fastcall PeekByte(int Timeout);
	virtual int __fastcall WaitingData(void);
	int __fastcall WaitingDataEx(void);
	void __fastcall Purge(void);
	void __fastcall SetLinger(bool Enable, int Linger);
	void __fastcall GetSinLocal(void);
	void __fastcall GetSinRemote(void);
	void __fastcall GetSins(void);
	void __fastcall ResetLastError(void);
	virtual int __fastcall SockCheck(int SockResult);
	void __fastcall ExceptCheck(void);
	System::UnicodeString __fastcall LocalName(void);
	void __fastcall ResolveNameToIP(System::UnicodeString Name, System::Classes::TStrings* const IPList);
	System::UnicodeString __fastcall ResolveName(System::UnicodeString Name);
	System::UnicodeString __fastcall ResolveIPToName(System::UnicodeString IP);
	System::Word __fastcall ResolvePort(System::UnicodeString Port);
	void __fastcall SetRemoteSin(System::UnicodeString IP, System::UnicodeString Port);
	virtual System::UnicodeString __fastcall GetLocalSinIP(void);
	virtual System::UnicodeString __fastcall GetRemoteSinIP(void);
	virtual int __fastcall GetLocalSinPort(void);
	virtual int __fastcall GetRemoteSinPort(void);
	virtual bool __fastcall CanRead(int Timeout);
	virtual bool __fastcall CanReadEx(int Timeout);
	virtual bool __fastcall CanWrite(int Timeout);
	virtual int __fastcall SendBufferTo(void * Buffer, int Length);
	virtual int __fastcall RecvBufferFrom(void * Buffer, int Length);
	bool __fastcall GroupCanRead(System::Classes::TList* const SocketList, int Timeout, System::Classes::TList* const CanReadList);
	void __fastcall EnableReuse(bool Value);
	void __fastcall SetTimeout(int Timeout);
	void __fastcall SetSendTimeout(int Timeout);
	void __fastcall SetRecvTimeout(int Timeout);
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
	__property Synsock::TWSAData WSAData = {read=GetWsaData};
	__property Synsock::TFDSet FDset = {read=FFDSet};
	__property Synsock::TVarSin LocalSin = {read=FLocalSin, write=FLocalSin};
	__property Synsock::TVarSin RemoteSin = {read=FRemoteSin, write=FRemoteSin};
	__property int Socket = {read=FSocket, write=SetSocket, nodefault};
	__property int LastError = {read=FLastError, nodefault};
	__property System::UnicodeString LastErrorDesc = {read=FLastErrorDesc};
	__property System::AnsiString LineBuffer = {read=FBuffer, write=FBuffer};
	__property int SizeRecvBuffer = {read=GetSizeRecvBuffer, write=SetSizeRecvBuffer, nodefault};
	__property int SizeSendBuffer = {read=GetSizeSendBuffer, write=SetSizeSendBuffer, nodefault};
	__property bool NonBlockMode = {read=FNonBlockMode, write=SetNonBlockMode, nodefault};
	__property int TTL = {read=GetTTL, write=SetTTL, nodefault};
	__property bool IP6used = {read=FIP6used, nodefault};
	__property int RecvCounter = {read=FRecvCounter, nodefault};
	__property int SendCounter = {read=FSendCounter, nodefault};
	
__published:
	__classmethod System::UnicodeString __fastcall GetErrorDesc(int ErrorCode);
	virtual System::UnicodeString __fastcall GetErrorDescEx(void);
	__property int Tag = {read=FTag, write=FTag, nodefault};
	__property bool RaiseExcept = {read=FRaiseExcept, write=FRaiseExcept, nodefault};
	__property int MaxLineLength = {read=FMaxLineLength, write=FMaxLineLength, nodefault};
	__property int MaxSendBandwidth = {read=FMaxSendBandwidth, write=FMaxSendBandwidth, nodefault};
	__property int MaxRecvBandwidth = {read=FMaxRecvBandwidth, write=FMaxRecvBandwidth, nodefault};
	__property int MaxBandwidth = {write=SetBandwidth, nodefault};
	__property bool ConvertLineEnd = {read=FConvertLineEnd, write=FConvertLineEnd, nodefault};
	__property TSocketFamily Family = {read=FFamily, write=SetFamily, nodefault};
	__property bool PreferIP4 = {read=FPreferIP4, write=FPreferIP4, nodefault};
	__property bool InterPacketTimeout = {read=FInterPacketTimeout, write=FInterPacketTimeout, nodefault};
	__property int SendMaxChunk = {read=FSendMaxChunk, write=FSendMaxChunk, nodefault};
	__property bool StopFlag = {read=FStopFlag, write=FStopFlag, nodefault};
	__property int NonblockSendTimeout = {read=FNonblockSendTimeout, write=FNonblockSendTimeout, nodefault};
	__property THookSocketStatus OnStatus = {read=FOnStatus, write=FOnStatus};
	__property THookDataFilter OnReadFilter = {read=FOnReadFilter, write=FOnReadFilter};
	__property THookCreateSocket OnCreateSocket = {read=FOnCreateSocket, write=FOnCreateSocket};
	__property THookMonitor OnMonitor = {read=FOnMonitor, write=FOnMonitor};
	__property THookHeartbeat OnHeartbeat = {read=FOnHeartbeat, write=FOnHeartbeat};
	__property int HeartbeatRate = {read=FHeartbeatRate, write=FHeartbeatRate, nodefault};
	__property System::TObject* Owner = {read=FOwner, write=FOwner};
};


class DELPHICLASS TSocksBlockSocket;
class PASCALIMPLEMENTATION TSocksBlockSocket : public TBlockSocket
{
	typedef TBlockSocket inherited;
	
protected:
	System::UnicodeString FSocksIP;
	System::UnicodeString FSocksPort;
	int FSocksTimeout;
	System::UnicodeString FSocksUsername;
	System::UnicodeString FSocksPassword;
	bool FUsingSocks;
	bool FSocksResolver;
	int FSocksLastError;
	System::UnicodeString FSocksResponseIP;
	System::UnicodeString FSocksResponsePort;
	System::UnicodeString FSocksLocalIP;
	System::UnicodeString FSocksLocalPort;
	System::UnicodeString FSocksRemoteIP;
	System::UnicodeString FSocksRemotePort;
	bool FBypassFlag;
	TSocksType FSocksType;
	System::AnsiString __fastcall SocksCode(System::UnicodeString IP, System::UnicodeString Port);
	int __fastcall SocksDecode(System::AnsiString Value);
	
public:
	__fastcall TSocksBlockSocket(void);
	bool __fastcall SocksOpen(void);
	bool __fastcall SocksRequest(System::Byte Cmd, const System::UnicodeString IP, const System::UnicodeString Port);
	bool __fastcall SocksResponse(void);
	__property bool UsingSocks = {read=FUsingSocks, nodefault};
	__property int SocksLastError = {read=FSocksLastError, nodefault};
	
__published:
	__property System::UnicodeString SocksIP = {read=FSocksIP, write=FSocksIP};
	__property System::UnicodeString SocksPort = {read=FSocksPort, write=FSocksPort};
	__property System::UnicodeString SocksUsername = {read=FSocksUsername, write=FSocksUsername};
	__property System::UnicodeString SocksPassword = {read=FSocksPassword, write=FSocksPassword};
	__property int SocksTimeout = {read=FSocksTimeout, write=FSocksTimeout, nodefault};
	__property bool SocksResolver = {read=FSocksResolver, write=FSocksResolver, nodefault};
	__property TSocksType SocksType = {read=FSocksType, write=FSocksType, nodefault};
public:
	/* TBlockSocket.CreateAlternate */ inline __fastcall TSocksBlockSocket(System::UnicodeString Stub) : TBlockSocket(Stub) { }
	/* TBlockSocket.Destroy */ inline __fastcall virtual ~TSocksBlockSocket(void) { }
	
};


class DELPHICLASS TTCPBlockSocket;
class DELPHICLASS TCustomSSL;
class PASCALIMPLEMENTATION TTCPBlockSocket : public TSocksBlockSocket
{
	typedef TSocksBlockSocket inherited;
	
protected:
	THookAfterConnect FOnAfterConnect;
	TCustomSSL* FSSL;
	System::UnicodeString FHTTPTunnelIP;
	System::UnicodeString FHTTPTunnelPort;
	bool FHTTPTunnel;
	System::UnicodeString FHTTPTunnelRemoteIP;
	System::UnicodeString FHTTPTunnelRemotePort;
	System::UnicodeString FHTTPTunnelUser;
	System::UnicodeString FHTTPTunnelPass;
	int FHTTPTunnelTimeout;
	void __fastcall SocksDoConnect(System::UnicodeString IP, System::UnicodeString Port);
	void __fastcall HTTPTunnelDoConnect(System::UnicodeString IP, System::UnicodeString Port);
	void __fastcall DoAfterConnect(void);
	
public:
	__fastcall TTCPBlockSocket(void);
	__fastcall TTCPBlockSocket(TSSLClass SSLPlugin);
	__fastcall virtual ~TTCPBlockSocket(void);
	virtual void __fastcall CloseSocket(void);
	virtual int __fastcall WaitingData(void);
	virtual void __fastcall Listen(void);
	virtual int __fastcall Accept(void);
	virtual void __fastcall Connect(System::UnicodeString IP, System::UnicodeString Port);
	void __fastcall SSLDoConnect(void);
	void __fastcall SSLDoShutdown(void);
	bool __fastcall SSLAcceptConnection(void);
	virtual System::UnicodeString __fastcall GetLocalSinIP(void);
	virtual System::UnicodeString __fastcall GetRemoteSinIP(void);
	virtual int __fastcall GetLocalSinPort(void);
	virtual int __fastcall GetRemoteSinPort(void);
	virtual int __fastcall SendBuffer(void * Buffer, int Length);
	virtual int __fastcall RecvBuffer(void * Buffer, int Len);
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
	__property TCustomSSL* SSL = {read=FSSL};
	__property bool HTTPTunnel = {read=FHTTPTunnel, nodefault};
	
__published:
	virtual System::UnicodeString __fastcall GetErrorDescEx(void);
	__property System::UnicodeString HTTPTunnelIP = {read=FHTTPTunnelIP, write=FHTTPTunnelIP};
	__property System::UnicodeString HTTPTunnelPort = {read=FHTTPTunnelPort, write=FHTTPTunnelPort};
	__property System::UnicodeString HTTPTunnelUser = {read=FHTTPTunnelUser, write=FHTTPTunnelUser};
	__property System::UnicodeString HTTPTunnelPass = {read=FHTTPTunnelPass, write=FHTTPTunnelPass};
	__property int HTTPTunnelTimeout = {read=FHTTPTunnelTimeout, write=FHTTPTunnelTimeout, nodefault};
	__property THookAfterConnect OnAfterConnect = {read=FOnAfterConnect, write=FOnAfterConnect};
public:
	/* TBlockSocket.CreateAlternate */ inline __fastcall TTCPBlockSocket(System::UnicodeString Stub) : TSocksBlockSocket(Stub) { }
	
};


class DELPHICLASS TDgramBlockSocket;
class PASCALIMPLEMENTATION TDgramBlockSocket : public TSocksBlockSocket
{
	typedef TSocksBlockSocket inherited;
	
public:
	virtual void __fastcall Connect(System::UnicodeString IP, System::UnicodeString Port);
	virtual int __fastcall SendBuffer(void * Buffer, int Length);
	virtual int __fastcall RecvBuffer(void * Buffer, int Length);
public:
	/* TSocksBlockSocket.Create */ inline __fastcall TDgramBlockSocket(void) : TSocksBlockSocket() { }
	
public:
	/* TBlockSocket.CreateAlternate */ inline __fastcall TDgramBlockSocket(System::UnicodeString Stub) : TSocksBlockSocket(Stub) { }
	/* TBlockSocket.Destroy */ inline __fastcall virtual ~TDgramBlockSocket(void) { }
	
};


class DELPHICLASS TUDPBlockSocket;
class PASCALIMPLEMENTATION TUDPBlockSocket : public TDgramBlockSocket
{
	typedef TDgramBlockSocket inherited;
	
protected:
	TTCPBlockSocket* FSocksControlSock;
	bool __fastcall UdpAssociation(void);
	void __fastcall SetMulticastTTL(int TTL);
	int __fastcall GetMulticastTTL(void);
	
public:
	__fastcall virtual ~TUDPBlockSocket(void);
	void __fastcall EnableBroadcast(bool Value);
	virtual int __fastcall SendBufferTo(void * Buffer, int Length);
	virtual int __fastcall RecvBufferFrom(void * Buffer, int Length);
	void __fastcall AddMulticast(System::UnicodeString MCastIP);
	void __fastcall DropMulticast(System::UnicodeString MCastIP);
	void __fastcall EnableMulticastLoop(bool Value);
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
	__property int MulticastTTL = {read=GetMulticastTTL, write=SetMulticastTTL, nodefault};
public:
	/* TSocksBlockSocket.Create */ inline __fastcall TUDPBlockSocket(void) : TDgramBlockSocket() { }
	
public:
	/* TBlockSocket.CreateAlternate */ inline __fastcall TUDPBlockSocket(System::UnicodeString Stub) : TDgramBlockSocket(Stub) { }
	
};


class DELPHICLASS TICMPBlockSocket;
class PASCALIMPLEMENTATION TICMPBlockSocket : public TDgramBlockSocket
{
	typedef TDgramBlockSocket inherited;
	
public:
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
public:
	/* TSocksBlockSocket.Create */ inline __fastcall TICMPBlockSocket(void) : TDgramBlockSocket() { }
	
public:
	/* TBlockSocket.CreateAlternate */ inline __fastcall TICMPBlockSocket(System::UnicodeString Stub) : TDgramBlockSocket(Stub) { }
	/* TBlockSocket.Destroy */ inline __fastcall virtual ~TICMPBlockSocket(void) { }
	
};


class DELPHICLASS TRAWBlockSocket;
class PASCALIMPLEMENTATION TRAWBlockSocket : public TBlockSocket
{
	typedef TBlockSocket inherited;
	
public:
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
public:
	/* TBlockSocket.Create */ inline __fastcall TRAWBlockSocket(void) : TBlockSocket() { }
	/* TBlockSocket.CreateAlternate */ inline __fastcall TRAWBlockSocket(System::UnicodeString Stub) : TBlockSocket(Stub) { }
	/* TBlockSocket.Destroy */ inline __fastcall virtual ~TRAWBlockSocket(void) { }
	
};


class DELPHICLASS TPGMMessageBlockSocket;
class PASCALIMPLEMENTATION TPGMMessageBlockSocket : public TBlockSocket
{
	typedef TBlockSocket inherited;
	
public:
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
public:
	/* TBlockSocket.Create */ inline __fastcall TPGMMessageBlockSocket(void) : TBlockSocket() { }
	/* TBlockSocket.CreateAlternate */ inline __fastcall TPGMMessageBlockSocket(System::UnicodeString Stub) : TBlockSocket(Stub) { }
	/* TBlockSocket.Destroy */ inline __fastcall virtual ~TPGMMessageBlockSocket(void) { }
	
};


class DELPHICLASS TPGMStreamBlockSocket;
class PASCALIMPLEMENTATION TPGMStreamBlockSocket : public TBlockSocket
{
	typedef TBlockSocket inherited;
	
public:
	virtual int __fastcall GetSocketType(void);
	virtual int __fastcall GetSocketProtocol(void);
public:
	/* TBlockSocket.Create */ inline __fastcall TPGMStreamBlockSocket(void) : TBlockSocket() { }
	/* TBlockSocket.CreateAlternate */ inline __fastcall TPGMStreamBlockSocket(System::UnicodeString Stub) : TBlockSocket(Stub) { }
	/* TBlockSocket.Destroy */ inline __fastcall virtual ~TPGMStreamBlockSocket(void) { }
	
};


class PASCALIMPLEMENTATION TCustomSSL : public System::TObject
{
	typedef System::TObject inherited;
	
protected:
	THookVerifyCert FOnVerifyCert;
	TTCPBlockSocket* FSocket;
	bool FSSLEnabled;
	int FLastError;
	System::UnicodeString FLastErrorDesc;
	TSSLType FSSLType;
	System::UnicodeString FKeyPassword;
	System::UnicodeString FCiphers;
	System::UnicodeString FCertificateFile;
	System::UnicodeString FPrivateKeyFile;
	System::AnsiString FCertificate;
	System::AnsiString FPrivateKey;
	System::AnsiString FPFX;
	System::UnicodeString FPFXfile;
	System::AnsiString FCertCA;
	System::UnicodeString FCertCAFile;
	System::AnsiString FTrustCertificate;
	System::UnicodeString FTrustCertificateFile;
	bool FVerifyCert;
	System::UnicodeString FUsername;
	System::UnicodeString FPassword;
	System::UnicodeString FSSHChannelType;
	System::UnicodeString FSSHChannelArg1;
	System::UnicodeString FSSHChannelArg2;
	int FCertComplianceLevel;
	System::UnicodeString FSNIHost;
	void __fastcall ReturnError(void);
	virtual void __fastcall SetCertCAFile(const System::UnicodeString Value);
	bool __fastcall DoVerifyCert(void);
	virtual bool __fastcall CreateSelfSignedCert(System::UnicodeString Host);
	
public:
	__fastcall virtual TCustomSSL(TTCPBlockSocket* const Value);
	virtual void __fastcall Assign(TCustomSSL* const Value);
	virtual System::UnicodeString __fastcall LibVersion(void);
	virtual System::UnicodeString __fastcall LibName(void);
	virtual bool __fastcall Connect(void);
	virtual bool __fastcall Accept(void);
	virtual bool __fastcall Shutdown(void);
	virtual bool __fastcall BiShutdown(void);
	virtual int __fastcall SendBuffer(void * Buffer, int Len);
	virtual int __fastcall RecvBuffer(void * Buffer, int Len);
	virtual int __fastcall WaitingData(void);
	virtual System::UnicodeString __fastcall GetSSLVersion(void);
	virtual System::UnicodeString __fastcall GetPeerSubject(void);
	virtual int __fastcall GetPeerSerialNo(void);
	virtual System::UnicodeString __fastcall GetPeerIssuer(void);
	virtual System::UnicodeString __fastcall GetPeerName(void);
	virtual unsigned __fastcall GetPeerNameHash(void);
	virtual System::UnicodeString __fastcall GetPeerFingerprint(void);
	virtual System::UnicodeString __fastcall GetCertInfo(void);
	virtual System::UnicodeString __fastcall GetCipherName(void);
	virtual int __fastcall GetCipherBits(void);
	virtual int __fastcall GetCipherAlgBits(void);
	virtual int __fastcall GetVerifyCert(void);
	__property bool SSLEnabled = {read=FSSLEnabled, nodefault};
	__property int LastError = {read=FLastError, nodefault};
	__property System::UnicodeString LastErrorDesc = {read=FLastErrorDesc};
	
__published:
	__property TSSLType SSLType = {read=FSSLType, write=FSSLType, nodefault};
	__property System::UnicodeString KeyPassword = {read=FKeyPassword, write=FKeyPassword};
	__property System::UnicodeString Username = {read=FUsername, write=FUsername};
	__property System::UnicodeString Password = {read=FPassword, write=FPassword};
	__property System::UnicodeString Ciphers = {read=FCiphers, write=FCiphers};
	__property System::UnicodeString CertificateFile = {read=FCertificateFile, write=FCertificateFile};
	__property System::UnicodeString PrivateKeyFile = {read=FPrivateKeyFile, write=FPrivateKeyFile};
	__property System::AnsiString Certificate = {read=FCertificate, write=FCertificate};
	__property System::AnsiString PrivateKey = {read=FPrivateKey, write=FPrivateKey};
	__property System::AnsiString PFX = {read=FPFX, write=FPFX};
	__property System::UnicodeString PFXfile = {read=FPFXfile, write=FPFXfile};
	__property System::UnicodeString TrustCertificateFile = {read=FTrustCertificateFile, write=FTrustCertificateFile};
	__property System::AnsiString TrustCertificate = {read=FTrustCertificate, write=FTrustCertificate};
	__property System::AnsiString CertCA = {read=FCertCA, write=FCertCA};
	__property System::UnicodeString CertCAFile = {read=FCertCAFile, write=SetCertCAFile};
	__property bool VerifyCert = {read=FVerifyCert, write=FVerifyCert, nodefault};
	__property System::UnicodeString SSHChannelType = {read=FSSHChannelType, write=FSSHChannelType};
	__property System::UnicodeString SSHChannelArg1 = {read=FSSHChannelArg1, write=FSSHChannelArg1};
	__property System::UnicodeString SSHChannelArg2 = {read=FSSHChannelArg2, write=FSSHChannelArg2};
	__property int CertComplianceLevel = {read=FCertComplianceLevel, write=FCertComplianceLevel, nodefault};
	__property THookVerifyCert OnVerifyCert = {read=FOnVerifyCert, write=FOnVerifyCert};
	__property System::UnicodeString SNIHost = {read=FSNIHost, write=FSNIHost};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomSSL(void) { }
	
};


class DELPHICLASS TSSLNone;
class PASCALIMPLEMENTATION TSSLNone : public TCustomSSL
{
	typedef TCustomSSL inherited;
	
public:
	virtual System::UnicodeString __fastcall LibVersion(void);
	virtual System::UnicodeString __fastcall LibName(void);
public:
	/* TCustomSSL.Create */ inline __fastcall virtual TSSLNone(TTCPBlockSocket* const Value) : TCustomSSL(Value) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TSSLNone(void) { }
	
};


struct DECLSPEC_DRECORD TIPHeader
{
public:
	System::Byte VerLen;
	System::Byte TOS;
	System::Word TotalLen;
	System::Word Identifer;
	System::Word FragOffsets;
	System::Byte TTL;
	System::Byte Protocol;
	System::Word CheckSum;
	unsigned SourceIp;
	unsigned DestIp;
	unsigned Options;
};


class DELPHICLASS TSynaClient;
#pragma pack(push,4)
class PASCALIMPLEMENTATION TSynaClient : public System::TObject
{
	typedef System::TObject inherited;
	
protected:
	System::UnicodeString FTargetHost;
	System::UnicodeString FTargetPort;
	System::UnicodeString FIPInterface;
	int FTimeout;
	System::UnicodeString FUserName;
	System::UnicodeString FPassword;
	
public:
	__fastcall TSynaClient(void);
	
__published:
	__property System::UnicodeString TargetHost = {read=FTargetHost, write=FTargetHost};
	__property System::UnicodeString TargetPort = {read=FTargetPort, write=FTargetPort};
	__property System::UnicodeString IPInterface = {read=FIPInterface, write=FIPInterface};
	__property int Timeout = {read=FTimeout, write=FTimeout, nodefault};
	__property System::UnicodeString UserName = {read=FUserName, write=FUserName};
	__property System::UnicodeString Password = {read=FPassword, write=FPassword};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TSynaClient(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define SynapseRelease L"38"
#define cLocalhost L"127.0.0.1"
#define cAnyHost L"0.0.0.0"
#define cBroadcast L"255.255.255.255"
#define c6Localhost L"::1"
#define c6AnyHost L"::0"
#define c6Broadcast L"ffff::1"
static const System::WideChar cAnyPort = (System::WideChar)(0x30);
static const System::WideChar CR = (System::WideChar)(0xd);
static const System::WideChar LF = (System::WideChar)(0xa);
#define CRLF L"\r\n"
static const int c64k = int(0x10000);
extern DELPHI_PACKAGE TSSLClass SSLImplementation;
}	/* namespace Blcksock */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BLCKSOCK)
using namespace Blcksock;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BlcksockHPP
