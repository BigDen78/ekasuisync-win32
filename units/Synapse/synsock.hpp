﻿// CodeGear C++Builder
// Copyright (c) 1995, 2013 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'synsock.pas' rev: 26.00 (Windows)

#ifndef SynsockHPP
#define SynsockHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SyncObjs.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------
/* EDE 2003-02-19 */
namespace Synsock { using System::Shortint; }
#undef h_addr
#undef IOCPARM_MASK
#undef FD_SETSIZE
#undef IOC_VOID
#undef IOC_OUT
#undef IOC_IN
#undef IOC_INOUT
#undef FIONREAD
#undef FIONBIO
#undef FIOASYNC
#undef IPPROTO_IP
#undef IPPROTO_ICMP
#undef IPPROTO_IGMP
#undef IPPROTO_TCP
#undef IPPROTO_UDP
#undef IPPROTO_RAW
#undef IPPROTO_MAX
#undef INADDR_ANY
#undef INADDR_LOOPBACK
#undef INADDR_BROADCAST
#undef INADDR_NONE
#undef INVALID_SOCKET
#undef SOCKET_ERROR
#undef WSADESCRIPTION_LEN
#undef WSASYS_STATUS_LEN
#undef IP_OPTIONS
#undef IP_TOS
#undef IP_TTL
#undef IP_MULTICAST_IF
#undef IP_MULTICAST_TTL
#undef IP_MULTICAST_LOOP
#undef IP_ADD_MEMBERSHIP
#undef IP_DROP_MEMBERSHIP
#undef IP_DONTFRAGMENT
#undef IP_DEFAULT_MULTICAST_TTL
#undef IP_DEFAULT_MULTICAST_LOOP
#undef IP_MAX_MEMBERSHIPS
#undef SOL_SOCKET
#undef SO_DEBUG
#undef SO_ACCEPTCONN
#undef SO_REUSEADDR
#undef SO_KEEPALIVE
#undef SO_DONTROUTE
#undef SO_BROADCAST
#undef SO_USELOOPBACK
#undef SO_LINGER
#undef SO_OOBINLINE
#undef SO_DONTLINGER
#undef SO_SNDBUF
#undef SO_RCVBUF
#undef SO_SNDLOWAT
#undef SO_RCVLOWAT
#undef SO_SNDTIMEO
#undef SO_RCVTIMEO
#undef SO_ERROR
#undef SO_OPENTYPE
#undef SO_SYNCHRONOUS_ALERT
#undef SO_SYNCHRONOUS_NONALERT
#undef SO_MAXDG
#undef SO_MAXPATHDG
#undef SO_UPDATE_ACCEPT_CONTEXT
#undef SO_CONNECT_TIME
#undef SO_TYPE
#undef SOCK_STREAM
#undef SOCK_DGRAM
#undef SOCK_RAW
#undef SOCK_RDM
#undef SOCK_SEQPACKET
#undef TCP_NODELAY
#undef AF_UNSPEC
#undef SOMAXCONN
#undef AF_INET
#undef AF_MAX
#undef PF_UNSPEC
#undef PF_INET
#undef PF_MAX
#undef MSG_OOB
#undef MSG_PEEK
#undef WSABASEERR
#undef WSAEINTR
#undef WSAEBADF
#undef WSAEACCES
#undef WSAEFAULT
#undef WSAEINVAL
#undef WSAEMFILE
#undef WSAEWOULDBLOCK
#undef WSAEINPROGRESS
#undef WSAEALREADY
#undef WSAENOTSOCK
#undef WSAEDESTADDRREQ
#undef WSAEMSGSIZE
#undef WSAEPROTOTYPE
#undef WSAENOPROTOOPT
#undef WSAEPROTONOSUPPORT
#undef WSAESOCKTNOSUPPORT
#undef WSAEOPNOTSUPP
#undef WSAEPFNOSUPPORT
#undef WSAEAFNOSUPPORT
#undef WSAEADDRINUSE
#undef WSAEADDRNOTAVAIL
#undef WSAENETDOWN
#undef WSAENETUNREACH
#undef WSAENETRESET
#undef WSAECONNABORTED
#undef WSAECONNRESET
#undef WSAENOBUFS
#undef WSAEISCONN
#undef WSAENOTCONN
#undef WSAESHUTDOWN
#undef WSAETOOMANYREFS
#undef WSAETIMEDOUT
#undef WSAECONNREFUSED
#undef WSAELOOP
#undef WSAENAMETOOLONG
#undef WSAEHOSTDOWN
#undef WSAEHOSTUNREACH
#undef WSAENOTEMPTY
#undef WSAEPROCLIM
#undef WSAEUSERS
#undef WSAEDQUOT
#undef WSAESTALE
#undef WSAEREMOTE
#undef WSASYSNOTREADY
#undef WSAVERNOTSUPPORTED
#undef WSANOTINITIALISED
#undef WSAEDISCON
#undef WSAENOMORE
#undef WSAECANCELLED
#undef WSAEEINVALIDPROCTABLE
#undef WSAEINVALIDPROVIDER
#undef WSAEPROVIDERFAILEDINIT
#undef WSASYSCALLFAILURE
#undef WSASERVICE_NOT_FOUND
#undef WSATYPE_NOT_FOUND
#undef WSA_E_NO_MORE
#undef WSA_E_CANCELLED
#undef WSAEREFUSED
#undef WSAHOST_NOT_FOUND
#undef HOST_NOT_FOUND
#undef WSATRY_AGAIN
#undef TRY_AGAIN
#undef WSANO_RECOVERY
#undef NO_RECOVERY
#undef WSANO_DATA
#undef NO_DATA
#undef WSANO_ADDRESS
#undef ENAMETOOLONG
#undef ENOTEMPTY
#undef FD_CLR
#undef FD_ISSET
#undef FD_SET
#undef FD_ZERO
#undef NO_ADDRESS
#undef ADDR_ANY
#undef SO_GROUP_ID
#undef SO_GROUP_PRIORITY
#undef SO_MAX_MSG_SIZE
#undef SO_PROTOCOL_INFOA
#undef SO_PROTOCOL_INFOW
#undef SO_PROTOCOL_INFO
#undef PVD_CONFIG
#undef AF_INET6
#undef PF_INET6

namespace Synsock
{
//-- type declarations -------------------------------------------------------
typedef System::Word u_short;

typedef int u_int;

typedef int u_long;

typedef int *pu_long;

typedef System::Word *pu_short;

typedef int TSocket;

typedef int TAddrFamily;

typedef void * TMemory;

struct TFDSet;
typedef TFDSet *PFDSet;

struct DECLSPEC_DRECORD TFDSet
{
public:
	int fd_count;
	System::StaticArray<int, 64> fd_array;
};


struct TTimeVal;
typedef TTimeVal *PTimeVal;

struct DECLSPEC_DRECORD TTimeVal
{
public:
	int tv_sec;
	int tv_usec;
};


struct TInAddr;
typedef TInAddr *PInAddr;

struct DECLSPEC_DRECORD TInAddr
{
	#pragma pack(push,1)
	union
	{
		struct 
		{
			int S_addr;
		};
		struct 
		{
			System::StaticArray<System::Byte, 4> S_bytes;
		};
		
	};
	#pragma pack(pop)
};


struct TSockAddrIn;
typedef TSockAddrIn *PSockAddrIn;

struct DECLSPEC_DRECORD TSockAddrIn
{
	#pragma pack(push,1)
	union
	{
		struct 
		{
			System::Word sa_family;
			System::StaticArray<System::Byte, 14> sa_data;
		};
		struct 
		{
			System::Word sin_family;
			System::Word sin_port;
			TInAddr sin_addr;
			System::StaticArray<System::Byte, 8> sin_zero;
		};
		
	};
	#pragma pack(pop)
};


struct DECLSPEC_DRECORD TIP_mreq
{
public:
	TInAddr imr_multiaddr;
	TInAddr imr_interface;
};


struct TInAddr6;
typedef TInAddr6 *PInAddr6;

struct DECLSPEC_DRECORD TInAddr6
{
	#pragma pack(push,1)
	union
	{
		struct 
		{
			System::StaticArray<int, 4> u6_addr32;
		};
		struct 
		{
			System::StaticArray<System::Word, 8> u6_addr16;
		};
		struct 
		{
			System::StaticArray<System::Byte, 16> u6_addr8;
		};
		struct 
		{
			System::StaticArray<System::Byte, 16> S6_addr;
		};
		
	};
	#pragma pack(pop)
};


struct TSockAddrIn6;
typedef TSockAddrIn6 *PSockAddrIn6;

struct DECLSPEC_DRECORD TSockAddrIn6
{
public:
	System::Word sin6_family;
	System::Word sin6_port;
	int sin6_flowinfo;
	TInAddr6 sin6_addr;
	int sin6_scope_id;
};


struct DECLSPEC_DRECORD TIPv6_mreq
{
public:
	TInAddr6 ipv6mr_multiaddr;
	int ipv6mr_interface;
	int padding;
};


struct THostEnt;
typedef THostEnt *PHostEnt;

struct DECLSPEC_DRECORD THostEnt
{
public:
	char *h_name;
	char * *h_aliases;
	short h_addrtype;
	short h_length;
	#pragma pack(push,1)
	union
	{
		struct 
		{
			PInAddr *h_addr;
		};
		struct 
		{
			char * *h_addr_list;
		};
		
	};
	#pragma pack(pop)
};


struct TNetEnt;
typedef TNetEnt *PNetEnt;

struct DECLSPEC_DRECORD TNetEnt
{
public:
	char *n_name;
	char * *n_aliases;
	short n_addrtype;
	int n_net;
};


struct TServEnt;
typedef TServEnt *PServEnt;

struct DECLSPEC_DRECORD TServEnt
{
public:
	char *s_name;
	char * *s_aliases;
	short s_port;
	char *s_proto;
};


struct TProtoEnt;
typedef TProtoEnt *PProtoEnt;

struct DECLSPEC_DRECORD TProtoEnt
{
public:
	char *p_name;
	char * *p_aliases;
	short p_proto;
};


typedef TSockAddrIn *PSockAddr;

typedef TSockAddrIn TSockAddr;

struct TSockProto;
typedef TSockProto *PSockProto;

struct DECLSPEC_DRECORD TSockProto
{
public:
	System::Word sp_family;
	System::Word sp_protocol;
};


struct TAddrInfo;
typedef TAddrInfo *PAddrInfo;

struct DECLSPEC_DRECORD TAddrInfo
{
public:
	int ai_flags;
	int ai_family;
	int ai_socktype;
	int ai_protocol;
	int ai_addrlen;
	char *ai_canonname;
	TSockAddrIn *ai_addr;
	TAddrInfo *ai_next;
};


struct TLinger;
typedef TLinger *PLinger;

struct DECLSPEC_DRECORD TLinger
{
public:
	System::Word l_onoff;
	System::Word l_linger;
};


struct TWSAData;
typedef TWSAData *PWSAData;

struct DECLSPEC_DRECORD TWSAData
{
public:
	System::Word wVersion;
	System::Word wHighVersion;
	System::StaticArray<char, 257> szDescription;
	System::StaticArray<char, 129> szSystemStatus;
	System::Word iMaxSockets;
	System::Word iMaxUdpDg;
	char *lpVendorInfo;
};


typedef int __stdcall (*TWSAStartup)(System::Word wVersionRequired, TWSAData &WSData);

typedef int __stdcall (*TWSACleanup)(void);

typedef int __stdcall (*TWSAGetLastError)(void);

typedef PServEnt __stdcall (*TGetServByName)(char * name, char * proto);

typedef PServEnt __stdcall (*TGetServByPort)(int port, char * proto);

typedef PProtoEnt __stdcall (*TGetProtoByName)(char * name);

typedef PProtoEnt __stdcall (*TGetProtoByNumber)(int proto);

typedef PHostEnt __stdcall (*TGetHostByName)(char * name);

typedef PHostEnt __stdcall (*TGetHostByAddr)(void * addr, int len, int Struc);

typedef int __stdcall (*TGetHostName)(char * name, int len);

typedef int __stdcall (*TShutdown)(int s, int how);

typedef int __stdcall (*TSetSockOpt)(int s, int level, int optname, char * optval, int optlen);

typedef int __stdcall (*TGetSockOpt)(int s, int level, int optname, char * optval, int &optlen);

typedef int __stdcall (*TSendTo)(int s, const void *Buf, int len, int flags, PSockAddr addrto, int tolen);

typedef int __stdcall (*TSend)(int s, const void *Buf, int len, int flags);

typedef int __stdcall (*TRecv)(int s, void *Buf, int len, int flags);

typedef int __stdcall (*TRecvFrom)(int s, void *Buf, int len, int flags, PSockAddr from, int &fromlen);

typedef System::Word __stdcall (*Tntohs)(System::Word netshort);

typedef int __stdcall (*Tntohl)(int netlong);

typedef int __stdcall (*TListen)(int s, int backlog);

typedef int __stdcall (*TIoctlSocket)(int s, unsigned cmd, int &arg);

typedef char * __stdcall (*TInet_ntoa)(TInAddr inaddr);

typedef int __stdcall (*TInet_addr)(char * cp);

typedef System::Word __stdcall (*Thtons)(System::Word hostshort);

typedef int __stdcall (*Thtonl)(int hostlong);

typedef int __stdcall (*TGetSockName)(int s, PSockAddr name, int &namelen);

typedef int __stdcall (*TGetPeerName)(int s, PSockAddr name, int &namelen);

typedef int __stdcall (*TConnect)(int s, PSockAddr name, int namelen);

typedef int __stdcall (*TCloseSocket)(int s);

typedef int __stdcall (*TBind)(int s, PSockAddr addr, int namelen);

typedef int __stdcall (*TAccept)(int s, PSockAddr addr, int &addrlen);

typedef int __stdcall (*TTSocket)(int af, int Struc, int Protocol);

typedef int __stdcall (*TSelect)(int nfds, PFDSet readfds, PFDSet writefds, PFDSet exceptfds, PTimeVal timeout);

typedef int __stdcall (*TGetAddrInfo)(char * NodeName, char * ServName, PAddrInfo Hints, PAddrInfo &Addrinfo);

typedef void __stdcall (*TFreeAddrInfo)(PAddrInfo ai);

typedef int __stdcall (*TGetNameInfo)(PSockAddr addr, int namelen, char * host, unsigned hostlen, char * serv, unsigned servlen, int flags);

typedef BOOL __stdcall (*T__WSAFDIsSet)(int s, TFDSet &FDSet);

typedef int __stdcall (*TWSAIoctl)(int s, unsigned dwIoControlCode, void * lpvInBuffer, unsigned cbInBuffer, void * lpvOutBuffer, unsigned cbOutBuffer, PDWORD lpcbBytesReturned, void * lpOverlapped, void * lpCompletionRoutine);

#pragma pack(push,1)
struct DECLSPEC_DRECORD TVarSin
{
	union
	{
		struct 
		{
			System::Word sin_family;
			union
			{
				struct 
				{
					System::Word sin6_port;
					int sin6_flowinfo;
					TInAddr6 sin6_addr;
					int sin6_scope_id;
				};
				struct 
				{
					System::Word sin_port;
					TInAddr sin_addr;
					System::StaticArray<System::Byte, 8> sin_zero;
				};
				
			};
		};
		struct 
		{
			System::Word AddressFamily;
		};
		
	};
};
#pragma pack(pop)


//-- var, const, procedure ---------------------------------------------------
static const System::Word WinsockLevel = System::Word(0x202);
#define DLLStackName L"ws2_32.dll"
#define DLLwship6 L"wship6.dll"
#define cLocalhost L"127.0.0.1"
#define cAnyHost L"0.0.0.0"
#define cBroadcast L"255.255.255.255"
#define c6Localhost L"::1"
#define c6AnyHost L"::0"
#define c6Broadcast L"ffff::1"
static const System::WideChar cAnyPort = (System::WideChar)(0x30);
static const System::Int8 FD_SETSIZE = System::Int8(0x40);
static const int FIONREAD = int(0x4004667f);
static const unsigned FIONBIO = unsigned(0x8004667e);
static const unsigned FIOASYNC = unsigned(0x8004667d);
static const System::Int8 IPPROTO_IP = System::Int8(0x0);
static const System::Int8 IPPROTO_ICMP = System::Int8(0x1);
static const System::Int8 IPPROTO_IGMP = System::Int8(0x2);
static const System::Int8 IPPROTO_TCP = System::Int8(0x6);
static const System::Int8 IPPROTO_UDP = System::Int8(0x11);
static const System::Int8 IPPROTO_IPV6 = System::Int8(0x29);
static const System::Int8 IPPROTO_ICMPV6 = System::Int8(0x3a);
static const System::Int8 IPPROTO_RM = System::Int8(0x71);
static const System::Byte IPPROTO_RAW = System::Byte(0xff);
static const System::Word IPPROTO_MAX = System::Word(0x100);
static const System::Int8 INADDR_ANY = System::Int8(0x0);
static const int INADDR_LOOPBACK = int(0x7f000001);
static const unsigned INADDR_BROADCAST = unsigned(0xffffffff);
static const unsigned INADDR_NONE = unsigned(0xffffffff);
static const System::Int8 ADDR_ANY = System::Int8(0x0);
static const int INVALID_SOCKET = int(-1);
static const System::Int8 SOCKET_ERROR = System::Int8(-1);
static const System::Int8 IP_OPTIONS = System::Int8(0x1);
//static const System::Int8 IP_HDRINCL = System::Int8(0x2);
static const System::Int8 IP_TOS = System::Int8(0x3);
static const System::Int8 IP_TTL = System::Int8(0x4);
static const System::Int8 IP_MULTICAST_IF = System::Int8(0x9);
static const System::Int8 IP_MULTICAST_TTL = System::Int8(0xa);
static const System::Int8 IP_MULTICAST_LOOP = System::Int8(0xb);
static const System::Int8 IP_ADD_MEMBERSHIP = System::Int8(0xc);
static const System::Int8 IP_DROP_MEMBERSHIP = System::Int8(0xd);
static const System::Int8 IP_DONTFRAGMENT = System::Int8(0xe);
static const System::Int8 IP_DEFAULT_MULTICAST_TTL = System::Int8(0x1);
static const System::Int8 IP_DEFAULT_MULTICAST_LOOP = System::Int8(0x1);
static const System::Int8 IP_MAX_MEMBERSHIPS = System::Int8(0x14);
static const System::Word SOL_SOCKET = System::Word(0xffff);
static const System::Int8 SO_DEBUG = System::Int8(0x1);
static const System::Int8 SO_ACCEPTCONN = System::Int8(0x2);
static const System::Int8 SO_REUSEADDR = System::Int8(0x4);
static const System::Int8 SO_KEEPALIVE = System::Int8(0x8);
static const System::Int8 SO_DONTROUTE = System::Int8(0x10);
static const System::Int8 SO_BROADCAST = System::Int8(0x20);
static const System::Int8 SO_USELOOPBACK = System::Int8(0x40);
static const System::Byte SO_LINGER = System::Byte(0x80);
static const System::Word SO_OOBINLINE = System::Word(0x100);
static const System::Word SO_DONTLINGER = System::Word(0xff7f);
static const System::Word SO_SNDBUF = System::Word(0x1001);
static const System::Word SO_RCVBUF = System::Word(0x1002);
static const System::Word SO_SNDLOWAT = System::Word(0x1003);
static const System::Word SO_RCVLOWAT = System::Word(0x1004);
static const System::Word SO_SNDTIMEO = System::Word(0x1005);
static const System::Word SO_RCVTIMEO = System::Word(0x1006);
static const System::Word SO_ERROR = System::Word(0x1007);
static const System::Word SO_TYPE = System::Word(0x1008);
static const System::Word SO_GROUP_ID = System::Word(0x2001);
static const System::Word SO_GROUP_PRIORITY = System::Word(0x2002);
static const System::Word SO_MAX_MSG_SIZE = System::Word(0x2003);
static const System::Word SO_PROTOCOL_INFOA = System::Word(0x2004);
static const System::Word SO_PROTOCOL_INFOW = System::Word(0x2005);
static const System::Word SO_PROTOCOL_INFO = System::Word(0x2004);
static const System::Word PVD_CONFIG = System::Word(0x3001);
static const System::Word SO_OPENTYPE = System::Word(0x7008);
static const System::Int8 SO_SYNCHRONOUS_ALERT = System::Int8(0x10);
static const System::Int8 SO_SYNCHRONOUS_NONALERT = System::Int8(0x20);
static const System::Word SO_MAXDG = System::Word(0x7009);
static const System::Word SO_MAXPATHDG = System::Word(0x700a);
static const System::Word SO_UPDATE_ACCEPT_CONTEXT = System::Word(0x700b);
static const System::Word SO_CONNECT_TIME = System::Word(0x700c);
static const int SOMAXCONN = int(0x7fffffff);
//static const System::Int8 IPV6_UNICAST_HOPS = System::Int8(0x8);
//static const System::Int8 IPV6_MULTICAST_IF = System::Int8(0x9);
//static const System::Int8 IPV6_MULTICAST_HOPS = System::Int8(0xa);
//static const System::Int8 IPV6_MULTICAST_LOOP = System::Int8(0xb);
//static const System::Int8 IPV6_JOIN_GROUP = System::Int8(0xc);
//static const System::Int8 IPV6_LEAVE_GROUP = System::Int8(0xd);
static const System::Int8 MSG_NOSIGNAL = System::Int8(0x0);
//static const System::Word NI_MAXHOST = System::Word(0x401);
//static const System::Int8 NI_MAXSERV = System::Int8(0x20);
//static const System::Int8 NI_NOFQDN = System::Int8(0x1);
//static const System::Int8 NI_NUMERICHOST = System::Int8(0x2);
//static const System::Int8 NI_NAMEREQD = System::Int8(0x4);
//static const System::Int8 NI_NUMERICSERV = System::Int8(0x8);
//static const System::Int8 NI_DGRAM = System::Int8(0x10);
static const System::Int8 SOCK_STREAM = System::Int8(0x1);
static const System::Int8 SOCK_DGRAM = System::Int8(0x2);
static const System::Int8 SOCK_RAW = System::Int8(0x3);
static const System::Int8 SOCK_RDM = System::Int8(0x4);
static const System::Int8 SOCK_SEQPACKET = System::Int8(0x5);
static const System::Int8 TCP_NODELAY = System::Int8(0x1);
static const System::Int8 AF_UNSPEC = System::Int8(0x0);
static const System::Int8 AF_INET = System::Int8(0x2);
static const System::Int8 AF_INET6 = System::Int8(0x17);
static const System::Int8 AF_MAX = System::Int8(0x18);
static const System::Int8 PF_UNSPEC = System::Int8(0x0);
static const System::Int8 PF_INET = System::Int8(0x2);
static const System::Int8 PF_INET6 = System::Int8(0x17);
static const System::Int8 PF_MAX = System::Int8(0x18);
//static const System::Int8 AI_PASSIVE = System::Int8(0x1);
//static const System::Int8 AI_CANONNAME = System::Int8(0x2);
//static const System::Int8 AI_NUMERICHOST = System::Int8(0x4);
static const System::Int8 MSG_OOB = System::Int8(0x1);
static const System::Int8 MSG_PEEK = System::Int8(0x2);
static const System::Word WSABASEERR = System::Word(0x2710);
static const System::Word WSAEINTR = System::Word(0x2714);
static const System::Word WSAEBADF = System::Word(0x2719);
static const System::Word WSAEACCES = System::Word(0x271d);
static const System::Word WSAEFAULT = System::Word(0x271e);
static const System::Word WSAEINVAL = System::Word(0x2726);
static const System::Word WSAEMFILE = System::Word(0x2728);
static const System::Word WSAEWOULDBLOCK = System::Word(0x2733);
static const System::Word WSAEINPROGRESS = System::Word(0x2734);
static const System::Word WSAEALREADY = System::Word(0x2735);
static const System::Word WSAENOTSOCK = System::Word(0x2736);
static const System::Word WSAEDESTADDRREQ = System::Word(0x2737);
static const System::Word WSAEMSGSIZE = System::Word(0x2738);
static const System::Word WSAEPROTOTYPE = System::Word(0x2739);
static const System::Word WSAENOPROTOOPT = System::Word(0x273a);
static const System::Word WSAEPROTONOSUPPORT = System::Word(0x273b);
static const System::Word WSAESOCKTNOSUPPORT = System::Word(0x273c);
static const System::Word WSAEOPNOTSUPP = System::Word(0x273d);
static const System::Word WSAEPFNOSUPPORT = System::Word(0x273e);
static const System::Word WSAEAFNOSUPPORT = System::Word(0x273f);
static const System::Word WSAEADDRINUSE = System::Word(0x2740);
static const System::Word WSAEADDRNOTAVAIL = System::Word(0x2741);
static const System::Word WSAENETDOWN = System::Word(0x2742);
static const System::Word WSAENETUNREACH = System::Word(0x2743);
static const System::Word WSAENETRESET = System::Word(0x2744);
static const System::Word WSAECONNABORTED = System::Word(0x2745);
static const System::Word WSAECONNRESET = System::Word(0x2746);
static const System::Word WSAENOBUFS = System::Word(0x2747);
static const System::Word WSAEISCONN = System::Word(0x2748);
static const System::Word WSAENOTCONN = System::Word(0x2749);
static const System::Word WSAESHUTDOWN = System::Word(0x274a);
static const System::Word WSAETOOMANYREFS = System::Word(0x274b);
static const System::Word WSAETIMEDOUT = System::Word(0x274c);
static const System::Word WSAECONNREFUSED = System::Word(0x274d);
static const System::Word WSAELOOP = System::Word(0x274e);
static const System::Word WSAENAMETOOLONG = System::Word(0x274f);
static const System::Word WSAEHOSTDOWN = System::Word(0x2750);
static const System::Word WSAEHOSTUNREACH = System::Word(0x2751);
static const System::Word WSAENOTEMPTY = System::Word(0x2752);
static const System::Word WSAEPROCLIM = System::Word(0x2753);
static const System::Word WSAEUSERS = System::Word(0x2754);
static const System::Word WSAEDQUOT = System::Word(0x2755);
static const System::Word WSAESTALE = System::Word(0x2756);
static const System::Word WSAEREMOTE = System::Word(0x2757);
static const System::Word WSASYSNOTREADY = System::Word(0x276b);
static const System::Word WSAVERNOTSUPPORTED = System::Word(0x276c);
static const System::Word WSANOTINITIALISED = System::Word(0x276d);
static const System::Word WSAEDISCON = System::Word(0x2775);
static const System::Word WSAENOMORE = System::Word(0x2776);
static const System::Word WSAECANCELLED = System::Word(0x2777);
static const System::Word WSAEEINVALIDPROCTABLE = System::Word(0x2778);
static const System::Word WSAEINVALIDPROVIDER = System::Word(0x2779);
static const System::Word WSAEPROVIDERFAILEDINIT = System::Word(0x277a);
static const System::Word WSASYSCALLFAILURE = System::Word(0x277b);
static const System::Word WSASERVICE_NOT_FOUND = System::Word(0x277c);
static const System::Word WSATYPE_NOT_FOUND = System::Word(0x277d);
static const System::Word WSA_E_NO_MORE = System::Word(0x277e);
static const System::Word WSA_E_CANCELLED = System::Word(0x277f);
static const System::Word WSAEREFUSED = System::Word(0x2780);
static const System::Word WSAHOST_NOT_FOUND = System::Word(0x2af9);
static const System::Word HOST_NOT_FOUND = System::Word(0x2af9);
static const System::Word WSATRY_AGAIN = System::Word(0x2afa);
static const System::Word TRY_AGAIN = System::Word(0x2afa);
static const System::Word WSANO_RECOVERY = System::Word(0x2afb);
static const System::Word NO_RECOVERY = System::Word(0x2afb);
static const System::Word WSANO_DATA = System::Word(0x2afc);
static const System::Word NO_DATA = System::Word(0x2afc);
static const System::Word WSANO_ADDRESS = System::Word(0x2afc);
static const System::Word NO_ADDRESS = System::Word(0x2afc);
static const System::Word EWOULDBLOCK = System::Word(0x2733);
static const System::Word EINPROGRESS = System::Word(0x2734);
static const System::Word EALREADY = System::Word(0x2735);
static const System::Word ENOTSOCK = System::Word(0x2736);
static const System::Word EDESTADDRREQ = System::Word(0x2737);
static const System::Word EMSGSIZE = System::Word(0x2738);
static const System::Word EPROTOTYPE = System::Word(0x2739);
static const System::Word ENOPROTOOPT = System::Word(0x273a);
static const System::Word EPROTONOSUPPORT = System::Word(0x273b);
static const System::Word ESOCKTNOSUPPORT = System::Word(0x273c);
static const System::Word EOPNOTSUPP = System::Word(0x273d);
static const System::Word EPFNOSUPPORT = System::Word(0x273e);
static const System::Word EAFNOSUPPORT = System::Word(0x273f);
static const System::Word EADDRINUSE = System::Word(0x2740);
static const System::Word EADDRNOTAVAIL = System::Word(0x2741);
static const System::Word ENETDOWN = System::Word(0x2742);
static const System::Word ENETUNREACH = System::Word(0x2743);
static const System::Word ENETRESET = System::Word(0x2744);
static const System::Word ECONNABORTED = System::Word(0x2745);
static const System::Word ECONNRESET = System::Word(0x2746);
static const System::Word ENOBUFS = System::Word(0x2747);
static const System::Word EISCONN = System::Word(0x2748);
static const System::Word ENOTCONN = System::Word(0x2749);
static const System::Word ESHUTDOWN = System::Word(0x274a);
static const System::Word ETOOMANYREFS = System::Word(0x274b);
static const System::Word ETIMEDOUT = System::Word(0x274c);
static const System::Word ECONNREFUSED = System::Word(0x274d);
static const System::Word ELOOP = System::Word(0x274e);
static const System::Word ENAMETOOLONG = System::Word(0x274f);
static const System::Word EHOSTDOWN = System::Word(0x2750);
static const System::Word EHOSTUNREACH = System::Word(0x2751);
static const System::Word ENOTEMPTY = System::Word(0x2752);
static const System::Word EPROCLIM = System::Word(0x2753);
static const System::Word EUSERS = System::Word(0x2754);
static const System::Word EDQUOT = System::Word(0x2755);
static const System::Word ESTALE = System::Word(0x2756);
static const System::Word EREMOTE = System::Word(0x2757);
static const System::Int8 EAI_ADDRFAMILY = System::Int8(0x1);
//static const System::Int8 EAI_AGAIN = System::Int8(0x2);
//static const System::Int8 EAI_BADFLAGS = System::Int8(0x3);
//static const System::Int8 EAI_FAIL = System::Int8(0x4);
//static const System::Int8 EAI_FAMILY = System::Int8(0x5);
//static const System::Int8 EAI_MEMORY = System::Int8(0x6);
//static const System::Int8 EAI_NODATA = System::Int8(0x7);
//static const System::Int8 EAI_NONAME = System::Int8(0x8);
//static const System::Int8 EAI_SERVICE = System::Int8(0x9);
//static const System::Int8 EAI_SOCKTYPE = System::Int8(0xa);
static const System::Int8 EAI_SYSTEM = System::Int8(0xb);
static const System::Word WSADESCRIPTION_LEN = System::Word(0x100);
static const System::Byte WSASYS_STATUS_LEN = System::Byte(0x80);
extern DELPHI_PACKAGE TInAddr6 in6addr_any;
extern DELPHI_PACKAGE TInAddr6 in6addr_loopback;
extern DELPHI_PACKAGE TWSAStartup WSAStartup;
extern DELPHI_PACKAGE TWSACleanup WSACleanup;
extern DELPHI_PACKAGE TWSAGetLastError WSAGetLastError;
extern DELPHI_PACKAGE TGetServByName GetServByName;
extern DELPHI_PACKAGE TGetServByPort GetServByPort;
extern DELPHI_PACKAGE TGetProtoByName GetProtoByName;
extern DELPHI_PACKAGE TGetProtoByNumber GetProtoByNumber;
extern DELPHI_PACKAGE TGetHostByName GetHostByName;
extern DELPHI_PACKAGE TGetHostByAddr GetHostByAddr;
extern DELPHI_PACKAGE TGetHostName ssGetHostName;
extern DELPHI_PACKAGE TShutdown Shutdown;
extern DELPHI_PACKAGE TSetSockOpt SetSockOpt;
extern DELPHI_PACKAGE TGetSockOpt GetSockOpt;
extern DELPHI_PACKAGE TSendTo ssSendTo;
extern DELPHI_PACKAGE TSend ssSend;
extern DELPHI_PACKAGE TRecv ssRecv;
extern DELPHI_PACKAGE TRecvFrom ssRecvFrom;
extern DELPHI_PACKAGE Tntohs ntohs;
extern DELPHI_PACKAGE Tntohl ntohl;
extern DELPHI_PACKAGE TListen Listen;
extern DELPHI_PACKAGE TIoctlSocket IoctlSocket;
extern DELPHI_PACKAGE TInet_ntoa Inet_ntoa;
extern DELPHI_PACKAGE TInet_addr Inet_addr;
extern DELPHI_PACKAGE Thtons htons;
extern DELPHI_PACKAGE Thtonl htonl;
extern DELPHI_PACKAGE TGetSockName ssGetSockName;
extern DELPHI_PACKAGE TGetPeerName ssGetPeerName;
extern DELPHI_PACKAGE TConnect ssConnect;
extern DELPHI_PACKAGE TCloseSocket CloseSocket;
extern DELPHI_PACKAGE TBind ssBind;
extern DELPHI_PACKAGE TAccept ssAccept;
extern DELPHI_PACKAGE TTSocket Socket;
extern DELPHI_PACKAGE TSelect Select;
extern DELPHI_PACKAGE TGetAddrInfo GetAddrInfo;
extern DELPHI_PACKAGE TFreeAddrInfo FreeAddrInfo;
extern DELPHI_PACKAGE TGetNameInfo GetNameInfo;
extern DELPHI_PACKAGE T__WSAFDIsSet __WSAFDIsSet;
extern DELPHI_PACKAGE TWSAIoctl WSAIoctl;
extern DELPHI_PACKAGE System::Syncobjs::TCriticalSection* SynSockCS;
extern DELPHI_PACKAGE bool SockEnhancedApi;
extern DELPHI_PACKAGE bool SockWship6Api;
extern DELPHI_PACKAGE bool __fastcall IN6_IS_ADDR_UNSPECIFIED(const PInAddr6 a);
extern DELPHI_PACKAGE bool __fastcall IN6_IS_ADDR_LOOPBACK(const PInAddr6 a);
extern DELPHI_PACKAGE bool __fastcall IN6_IS_ADDR_LINKLOCAL(const PInAddr6 a);
extern DELPHI_PACKAGE bool __fastcall IN6_IS_ADDR_SITELOCAL(const PInAddr6 a);
extern DELPHI_PACKAGE bool __fastcall IN6_IS_ADDR_MULTICAST(const PInAddr6 a);
extern DELPHI_PACKAGE bool __fastcall IN6_ADDR_EQUAL(const PInAddr6 a, const PInAddr6 b);
extern DELPHI_PACKAGE void __fastcall SET_IN6_IF_ADDR_ANY(const PInAddr6 a);
extern DELPHI_PACKAGE void __fastcall SET_LOOPBACK_ADDR6(const PInAddr6 a);
extern DELPHI_PACKAGE void __fastcall FD_CLR(int Socket, TFDSet &FDSet);
extern DELPHI_PACKAGE bool __fastcall FD_ISSET(int Socket, TFDSet &FDSet);
extern DELPHI_PACKAGE void __fastcall FD_SET(int Socket, TFDSet &FDSet);
extern DELPHI_PACKAGE void __fastcall FD_ZERO(TFDSet &FDSet);
extern DELPHI_PACKAGE int __fastcall SizeOfVarSin(const TVarSin &sin);
extern DELPHI_PACKAGE int __fastcall Bind(int s, const TVarSin &addr);
extern DELPHI_PACKAGE int __fastcall Connect(int s, const TVarSin &name);
extern DELPHI_PACKAGE int __fastcall GetSockName(int s, TVarSin &name);
extern DELPHI_PACKAGE int __fastcall GetPeerName(int s, TVarSin &name);
extern DELPHI_PACKAGE System::AnsiString __fastcall GetHostName(void);
extern DELPHI_PACKAGE int __fastcall Send(int s, void * Buf, int len, int flags);
extern DELPHI_PACKAGE int __fastcall Recv(int s, void * Buf, int len, int flags);
extern DELPHI_PACKAGE int __fastcall SendTo(int s, void * Buf, int len, int flags, const TVarSin &addrto);
extern DELPHI_PACKAGE int __fastcall RecvFrom(int s, void * Buf, int len, int flags, TVarSin &from);
extern DELPHI_PACKAGE int __fastcall Accept(int s, TVarSin &addr);
extern DELPHI_PACKAGE bool __fastcall IsNewApi(int Family);
extern DELPHI_PACKAGE int __fastcall SetVarSin(TVarSin &Sin, System::AnsiString IP, System::AnsiString Port, int Family, int SockProtocol, int SockType, bool PreferIP4);
extern DELPHI_PACKAGE System::AnsiString __fastcall GetSinIP(const TVarSin &Sin);
extern DELPHI_PACKAGE int __fastcall GetSinPort(const TVarSin &Sin);
extern DELPHI_PACKAGE void __fastcall ResolveNameToIP(System::AnsiString Name, int Family, int SockProtocol, int SockType, System::Classes::TStrings* const IPList);
extern DELPHI_PACKAGE System::Word __fastcall ResolvePort(System::AnsiString Port, int Family, int SockProtocol, int SockType);
extern DELPHI_PACKAGE System::AnsiString __fastcall ResolveIPToName(System::AnsiString IP, int Family, int SockProtocol, int SockType);
extern DELPHI_PACKAGE bool __fastcall InitSocketInterface(System::UnicodeString stack);
extern DELPHI_PACKAGE bool __fastcall DestroySocketInterface(void);
}	/* namespace Synsock */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SYNSOCK)
using namespace Synsock;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SynsockHPP
