﻿// CodeGear C++Builder
// Copyright (c) 1995, 2013 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'synaip.pas' rev: 26.00 (Windows)

#ifndef SynaipHPP
#define SynaipHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <synautil.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Synaip
{
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<System::Byte, 16> TIp6Bytes;

typedef System::StaticArray<System::Word, 8> TIp6Words;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool __fastcall IsIP(const System::UnicodeString Value);
extern DELPHI_PACKAGE bool __fastcall IsIP6(const System::UnicodeString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall IPToID(System::UnicodeString Host);
extern DELPHI_PACKAGE int __fastcall StrToIp(System::UnicodeString value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall IpToStr(int value);
extern DELPHI_PACKAGE System::AnsiString __fastcall ExpandIP6(System::AnsiString Value);
extern DELPHI_PACKAGE TIp6Bytes __fastcall StrToIp6(System::UnicodeString value);
extern DELPHI_PACKAGE System::UnicodeString __fastcall Ip6ToStr(const TIp6Bytes &value);
extern DELPHI_PACKAGE System::AnsiString __fastcall ReverseIP(System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall ReverseIP6(System::AnsiString Value);
}	/* namespace Synaip */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SYNAIP)
using namespace Synaip;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SynaipHPP
