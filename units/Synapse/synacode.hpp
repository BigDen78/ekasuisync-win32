﻿// CodeGear C++Builder
// Copyright (c) 1995, 2013 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'synacode.pas' rev: 26.00 (Windows)

#ifndef SynacodeHPP
#define SynacodeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Synacode
{
//-- type declarations -------------------------------------------------------
typedef System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)> TSpecials;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TSpecials SpecialChar;
extern DELPHI_PACKAGE TSpecials NonAsciiChar;
extern DELPHI_PACKAGE TSpecials URLFullSpecialChar;
extern DELPHI_PACKAGE TSpecials URLSpecialChar;
#define TableBase64 L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345"\
	L"6789+/="
#define TableBase64mod L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345"\
	L"6789+,="
#define TableUU L"`!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWX"\
	L"YZ[\\]^_"
#define TableXX L"+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrst"\
	L"uvwxyz"
#define ReTablebase64 L"@@@@@@@@@@>@@@?456789:;<=@@@@@@@\u0000\u0001\u0002\u0003\u0004"\
	L"\u0005\u0006\a\b\t\n\v\f\r\u000e\u000f\u0010\u0011\u0012\u0013"\
	L"\u0014\u0015\u0016\u0017\u0018\u0019@@@@@@\u001a\u001b\u001c"\
	L"\u001d\u001e\u001f !\"#$%&'()*+,-./0123@@@@@@"
#define ReTableUU L"\u0001\u0002\u0003\u0004\u0005\u0006\a\b\t\n\v\f\r\u000e\u000f"\
	L"\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019"\
	L"\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&'()*+,-./01234"\
	L"56789:;<=>?\u0000@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
#define ReTableXX L"@@@@@@@@@@\u0000@\u0001@@\u0002\u0003\u0004\u0005\u0006\a\b"\
	L"\t\n\v@@@@@@@\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014"\
	L"\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e"\
	L"\u001f !\"#$%@@@@@@&'()*+,-./0123456789:;<=>?@@@@@@"
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeTriplet(const System::AnsiString Value, char Delimiter);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeQuotedPrintable(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeURL(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeTriplet(const System::AnsiString Value, char Delimiter, const TSpecials &Specials);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeQuotedPrintable(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeSafeQuotedPrintable(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeURLElement(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeURL(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall Decode4to3(const System::AnsiString Value, const System::AnsiString Table);
extern DELPHI_PACKAGE System::AnsiString __fastcall Decode4to3Ex(const System::AnsiString Value, const System::AnsiString Table);
extern DELPHI_PACKAGE System::AnsiString __fastcall Encode3to4(const System::AnsiString Value, const System::AnsiString Table);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeBase64(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeBase64(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeBase64mod(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeBase64mod(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeUU(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall EncodeUU(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeXX(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall DecodeYEnc(const System::AnsiString Value);
extern DELPHI_PACKAGE int __fastcall UpdateCrc32(System::Byte Value, int Crc32);
extern DELPHI_PACKAGE int __fastcall Crc32(const System::AnsiString Value);
extern DELPHI_PACKAGE System::Word __fastcall UpdateCrc16(System::Byte Value, System::Word Crc16);
extern DELPHI_PACKAGE System::Word __fastcall Crc16(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall MD5(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall HMAC_MD5(System::AnsiString Text, System::AnsiString Key);
extern DELPHI_PACKAGE System::AnsiString __fastcall MD5LongHash(const System::AnsiString Value, int Len);
extern DELPHI_PACKAGE System::AnsiString __fastcall SHA1(const System::AnsiString Value);
extern DELPHI_PACKAGE System::AnsiString __fastcall HMAC_SHA1(System::AnsiString Text, System::AnsiString Key);
extern DELPHI_PACKAGE System::AnsiString __fastcall SHA1LongHash(const System::AnsiString Value, int Len);
extern DELPHI_PACKAGE System::AnsiString __fastcall MD4(const System::AnsiString Value);
}	/* namespace Synacode */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_SYNACODE)
using namespace Synacode;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// SynacodeHPP
