//---------------------------------------------------------------------------

#ifndef DataBaseUnitH
#define DataBaseUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Data.DB.hpp>
#include <System.IOUtils.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Phys.SQLite.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.ExprFuncs.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
#include <Windows.h>

#include "TypesUnit.h"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.AppEvnts.hpp>
#include <DateUtils.hpp>

struct FilesListItem {

	int Tag;
	int id;
};

struct tProcessingFilesDateItem {

	UnicodeString fn;
	int Param1;
	int Param2;
};

//---------------------------------------------------------------------------
class TDataBase : public TForm
{
__published:	// IDE-managed Components
	TDBGrid *DBGrid1;
	TFDConnection *Connection;
	TFDTable *FilesTable;
	TFDTable *IncidentsTable;
	TDataSource *FilesDataSource;
	TDataSource *IncidentsDataSource;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDQuery *FilesQuery;
	TFDPhysSQLiteDriverLink *FDPhysSQLiteDriverLink;
	TFDQuery *IncidentsQuery;
	TFDQuery *IncidentsTableQuery;
	TFDTable *PUT_GLTable;
	TFDQuery *UpdatePUTGLQuery;
	TMemo *Memo1;
	void __fastcall FormCreate(TObject *Sender);
	//void __fastcall DBGrid2MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	//void __fastcall DBGrid2MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	//void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);

private:	// User declarations
	void (*AddToLog)(UnicodeString Text);

public:		// User declarations

	__fastcall TDataBase(TComponent* Owner);

	TCriticalSection* cs;
	bool XMLFileExists(UnicodeString FileName);
	int AddXMLFile(UnicodeString FileName,
				   tIncidentSource Type,
				   tXMLFileState State,
				   bool* OKFlag,
				   int Tag = - 1);

	int AddIncident(int XMLFile_id,
					UnicodeString State,
					tIncidentSource Source,
					int NodeIndex,
					TDateTime dt,
					UnicodeString Text,
					UnicodeString Comment,
					UnicodeString SpeedLimit,
					tPathSide Side,
					UnicodeString Coord,
					UnicodeString PathID,
					UnicodeString PathText,
					tPicType PicType,
					TStream* Picture);


	void Connect(UnicodeString DataBaseFileName);
	void CloseConnection(void);
//	int SearchReadyFiles();
	bool GetReadyLOCAFile(UnicodeString* SendFileName, int* FileId);
	bool SetFileState(int File_Id, tXMLFileState State, UnicodeString* XMLFileName);
	void SetIncidentsState_isTransferred(int File_Id);
	void SetIncidentTableDisplayMode(tIncidentDisplayMode1 Mode1, tIncidentDisplayMode2 Mode2);
	void CheckAndFixDataIntegrity(void); // �������� ������� �� ����� ������ ��������� � ��

	std::vector <FilesListItem> FilesList;
	std::vector < tProcessingFilesDateItem > ProcessingFilesList;

	bool GetReadyEKASUIFile(UnicodeString* SendFileName, tIncidentSource* Source, int* RecId);
	void GetFilesWithState(tXMLFileState State);
//	void SetEKASUIFileState(int id, tXMLFileState State);

	bool GetLOCAProcessingFiles(void);
	bool GetEKASUIProcessingFiles(void);

	void setLog(void (*func)(UnicodeString Text));
};

//---------------------------------------------------------------------------
extern PACKAGE TDataBase *DataBase;
//extern bool ExLOG;
//---------------------------------------------------------------------------
#endif
