//---------------------------------------------------------------------------

#pragma hdrstop

#include "EKASUIDataUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

bool ProxyHttpPostURL(/*const */UnicodeString URL, TStream* SendData, /*const */TStream* Data)
{
	if ((!SendData) || (!Data)) return false;

	THTTPSend* HTTP = new THTTPSend();
//  try
	SendData->Position = 0;
	HTTP->ProxyHost = "";
	HTTP->ProxyPort = "";
	HTTP->Document->CopyFrom(SendData, SendData->Size);
	HTTP->Headers->Add("Accept-Encoding: gzip,deflate");
	HTTP->Protocol = "1.1";
	HTTP->MimeType = "text/xml;charset=UTF-8"; // ContentType
//    IdHTTP->Request->Accept = "";

	bool Result = HTTP->HTTPMethod("POST", URL);
	if (Result) {
		Data->CopyFrom(HTTP->Document, 0);
		Data->Position = 0;
	}

//  except
	delete HTTP;
	return Result;
}

WebServiceEKASUI::WebServiceEKASUI(System::Classes::TComponent* AOwner_): TThread(true)
{
	cs = new TCriticalSection();
//	IdHTTP = new TIdHTTP();
//	IdOpenSSL = new TIdSSLIOHandlerSocketOpenSSL;

//	ClientSocket = new TClientSocket(AOwner_);
//	ClientSocket->OnError = SocketErrorEvent; // __closure

	AOwner = AOwner_;
	AddToLog = NULL;
	//Log = new TStringList;

//	IdHTTP->IOHandler = IdOpenSSL;
//	IdHTTP->Request->AcceptEncoding = "gzip,deflate";
//	IdHTTP->Request->ContentType = "text/xml;charset=UTF-8";
//	IdHTTP->Request->Accept = "";
//	IdHTTP->ProtocolVersion = pv1_1;

	DebugMode = false;
	EndWorkFlag = false;
	EndWorkOkFlag = false;
}

__fastcall WebServiceEKASUI::~WebServiceEKASUI()
{
	delete cs;
//	delete IdHTTP;
//	delete IdOpenSSL;
//	delete ClientSocket;
	//delete Log;
	Terminate();
}

void WebServiceEKASUI::setLog(void (*func)(UnicodeString Text))
{
	AddToLog = func;
}

void WebServiceEKASUI::setDebugMode(bool State)
{
	DebugMode = State;
}

bool __fastcall WebServiceEKASUI::AnalyzeResponse(System::Classes::TComponent* AOwner, TStream* Data, int* Header, int* Count, int* Error)
{
	if ((!Header) || (!Count) || (!Error)) return false;

	*Header = - 1;
	*Count = - 1;
	*Error = - 1;
//	bool OKFlag = true;

	Data->Position = 0;
	TStringList* tmp1 = new TStringList();
	tmp1->LoadFromStream(Data);

	UnicodeString tmp2 = tmp1->Text;
//	AddToLog("Analyze Response Text: " + tmp2);

//	tmp2 = "sdf'kdsf;l dlfkgj df g<Header>0</Header> <Count>1</Count> <Error></Error>  </Response>";

	int pos = tmp2.Pos("<Header>");
	if (pos != 0) {

		tmp2.Delete(1, pos + 7);
		pos = tmp2.Pos("</Header>");
		if (!TryStrToInt(tmp2.SubString(1, pos - 1), *Header)) *Header = -1;

		pos = tmp2.Pos("<Count>");
		if (pos != 0) {

			tmp2.Delete(1, pos + 6);
			pos = tmp2.Pos("</Count>");

			if (!TryStrToInt(tmp2.SubString(1, pos - 1), *Count)) *Count = -1;

			pos = tmp2.Pos("<Error>");
			if (pos != 0) {

				tmp2.Delete(1, pos + 6);
				pos = tmp2.Pos("</Error>");


				if (!TryStrToInt(tmp2.SubString(1, pos - 1), *Error)) *Error = -1;

			} else return false;

		} else return false;


	} else return false;

	delete tmp1;
	return true;
}

void WebServiceEKASUI::StartWork(void)
{
	Start();
	EndWorkFlag = false;
	EndWorkOkFlag = false;
}

void WebServiceEKASUI::EndWork(void)
{
	EndWorkFlag = true;
}
bool WebServiceEKASUI::EndWorkOK(void)
{
	return EndWorkOkFlag;
}

void __fastcall WebServiceEKASUI::Execute()
{
	bool DebugState = false;

	while (!Terminated) //���� ����� �� ����� �������, ��������� ����
	{

		if (EndWorkFlag) break;

		for (unsigned int it = 0; it < TData.size(); it++)
		{
			if (EndWorkFlag) break;

			if (TData[it].State == asReadyToProcessed) // && (!TData[it].CanDelete))
			{

				//AddToLog("�������� � web ������ - DataType =" + IntToStr(TData[it].DataType));

				bool ErrorFlag = false;

				AnsiString url;
				UnicodeString txt;
				//AddToLog("DataType = " + IntToStr((int)TData[it].DataType));
				switch (TData[it].DataType) {
					case aGetEtbDm          : url = "http://10.23.197.34:7080/services/GetEtbDm"; txt = "������ � web ������ GetEtbDm"; break;
					case aGetBPD            : url = "http://10.23.197.34:7080/services/getLightBPD"; txt = "������ � web ������ getLightBPD"; break;
					case aGetDefect         : url = "http://10.23.197.34:7080/services/GetDefect"; txt = "������ � web ������ GetDefect"; break;
					case aSendVideoIncidents: url = "http://10.23.197.34:7080/services/VideoIncidents"; txt = "�������� ��������� � web ������"; break;  //VideoIncidents
					case aSendDefect        : url = "http://10.23.197.34:7080/services/SendDefect"; txt = "�������� ��������� � web ������"; break; //  SendDefect
					case aPing			    : url = "http://10.23.197.34:7080/services/GetDefect"; txt = "�������� ���������� � web �������� ������"; break; //  SendDefect
					case aURL               : url = TData[it].FileName; txt = "�������� ������ � " + url; break; //  SendDefect
				}

				#ifdef DEBUGWEBSERVICES
				//	TData[it].DataType = aNotSet;
				//	if (DebugState) url = "http://requestbin.fullcontact.com/z3cjurz3____555";
				//			  else
				url = "http://requestbin.fullcontact.com/1hr354o1";
				//	DebugState = !DebugState;
				//	if (TData[it].DataType == aPing) url = "http://requestbin.fullcontact.com";
				//	txt = "������ � �������� web ������";
				#endif

				try
					{
						if ((TData[it].DataType == aSendVideoIncidents) || (TData[it].DataType == aSendDefect)) {

							AddToLog(txt + " (" + url + ")" + ", ����: " + ExtractFileName(TData[it].FileName));
							TData[it].ReceivedData = new TStringStream();

							if (ProxyHttpPostURL(url, TData[it].SendData, TData[it].ReceivedData)) {
								TStringList* tmp3 = new TStringList();
								tmp3->LoadFromStream(TData[it].ReceivedData);

								if (GetConfig()->ExLOG) AddToLog("����� �� web ������: " + tmp3->Text);

								try
									{
										#ifndef DEBUGWEBSERVICES
										if (AnalyzeResponse(AOwner, TData[it].ReceivedData, &TData[it].Header, &TData[it].Count, &TData[it].Error))
										#else
										if (tmp3->Text.Pos("ok") != 0)
										#endif
										{

											#ifndef DEBUGWEBSERVICES
											if (DebugMode) AddToLog(Format("����� web ������ - Header: %d; Count: %d; Error: %d", ARRAYOFCONST((TData[it].Header, TData[it].Count, TData[it].Error))));
											if ((TData[it].Header != 0) || (TData[it].Count != 0)) {
											#endif

												AddToLog("�������� ������������� �� web ������: ������ �������");
												TData[it].AnalyzeResponse = true;
												TData[it].State = asProcessed;
											#ifndef DEBUGWEBSERVICES
											}
											#endif
										} else ErrorFlag = true;
									}
								catch(EIdException &E)
									{
										if (GetConfig()->ExLOG) AddToLog("Exception in AnalyzeResponse");
										ErrorFlag = true;
									}

							} else ErrorFlag = true;
						}
						else
						if ((TData[it].DataType == aGetEtbDm) || (TData[it].DataType == aGetBPD)) {

							TData[it].ReceivedData = new TStringStream();
							TStringList* tmp = new TStringList();
							TData[it].SendData->Position = 0;
							tmp->LoadFromStream(TData[it].SendData);
							AddToLog(txt + " (" + url + ")" + ", ����� �������: " + tmp->Text);
							TData[it].SendData->Position = 0;

							if (ProxyHttpPostURL(url, TData[it].SendData, TData[it].ReceivedData)) {

								if (GetConfig()->ExLOG) AddToLog(Format("����� web ������ - ������ ������ %d ����", ARRAYOFCONST((TData[it].ReceivedData->Size))));

								if (TData[it].DataType == aGetEtbDm) {

									tmp->Clear();
									tmp->LoadFromStream(TData[it].ReceivedData);
									int XMLTest = tmp->Text.Pos("<OutFile><device>");
									if (GetConfig()->ExLOG) AddToLog(Format("����� GetEtbDm ������ 17 �������� - %s [pos: %d]", ARRAYOFCONST((tmp->Text.SubString(1, 17), XMLTest))));
									if (XMLTest != 0) TData[it].State = asProcessed;
												 else ErrorFlag = true;
								}
								else
								{
									__int64 ZipId = 0;
									TData[it].ReceivedData->Position = 0;
									TData[it].ReceivedData->ReadData(ZipId, 4);
									TData[it].ReceivedData->Position = 0;
									if (GetConfig()->ExLOG) AddToLog(Format("����� GetLightBPD %s ���� (%d)", ARRAYOFCONST((IntToHex(ZipId, 24), (int)(ZipId == 0x04034B50)))));
									if (ZipId == 0x04034B50) TData[it].State = asProcessed;
														else ErrorFlag = true;
								};
							} else ErrorFlag = true;
							delete tmp;
						}
						else
						if (TData[it].DataType == aPing) {

							if (GetConfig()->ExLOG) AddToLog(txt);

							//ClientSocket->Port = 7080;
							//ClientSocket->Host = url;
							//ClientSocket->Active = true;
							//ClientSocket->Socket->Connect(0);
							//ClientSocket->Open();
							//IdHTTP->ProxyParams->ProxyPort = 7080;

							//TMemoryStream* tmp1 = new TMemoryStream();
							//TMemoryStream* tmp2 = new TMemoryStream();
							TData[it].ReceivedData = new TStringStream();
							ProxyHttpPostURL(url, TData[it].SendData, TData[it].ReceivedData); //???

							if (TData[it].ReceivedData->Size != 0) {

								TStringList* tmp3 = new TStringList();
								tmp3->LoadFromStream(TData[it].ReceivedData);
								if (GetConfig()->ExLOG) AddToLog("����� �� web ������: " + tmp3->Text);

								if (UpperCase(tmp3->Text).Pos("ERROR") == 0) {

									TData[it].Tag = 1; // (int)IdHTTP->Connected();
									if (GetConfig()->ExLOG) AddToLog("������� ����� �� web ������ - ����� ����");
								} else {

									TData[it].Tag = 0;
									if (GetConfig()->ExLOG) AddToLog("������� ����� �� web ������ - ����� ���");

								}
								delete tmp3;
							} else {

									TData[it].Tag = 0;
									if (GetConfig()->ExLOG) AddToLog("������� ����� �� web ������ - ����� ���");

								}

							//delete tmp1;
							//delete tmp2;


//							IdHTTP->Disconnect();
							//IdHTTP-> Connect(url);
//							IdHTTP->Disconnect();
						}
						else
						if (TData[it].DataType == aURL) {

							if (GetConfig()->ExLOG) AddToLog(txt);

							TData[it].ReceivedData = new TStringStream();
							if (ProxyHttpPostURL(url, TData[it].SendData, TData[it].ReceivedData)) {

								TData[it].State = asProcessed;
								if (TData[it].ReceivedData->Size != 0) {

									TStringList* tmp3 = new TStringList();
									tmp3->LoadFromStream(TData[it].ReceivedData);
									if (GetConfig()->ExLOG) AddToLog("����� �� web ������: " + tmp3->Text);
									delete tmp3;
								}
							}
						}
					}
				catch(EIdException &E)
					{

					  /*	if (TData[it].DataType == aPing) {

							TData[it].Tag = 0;
							TData[it].State = asProcessed;
							if (GetConfig()->ExLOG) AddToLog("������� ����� �� web ������ - ����� ���");
						} else
						{
							AddToLog(Format("������ (%s), ������� %d �� %d ", ARRAYOFCONST((E.Message, MaxErrorCount - TData[it].ErrorCount + 1, MaxErrorCount))));
							TData[it].ErrorCount--;
							if (TData[it].ErrorCount == 0) {

								TData[it].State = tActionState::asError;
								break;

							} else { Sleep(500); break; };
						} */
					}

				if (ErrorFlag) {


					TStringList* tmp3 = new TStringList();
					tmp3->LoadFromStream(TData[it].ReceivedData);

					if (tmp3->Text.Length() != 0) AddToLog(Format("������ ��� ������ � web ������� (%s), ������� %d �� %d ", ARRAYOFCONST((tmp3->Text.SubString(1, 512), MaxErrorCount - TData[it].ErrorCount + 1, MaxErrorCount))));
											 else AddToLog(Format("������ ��� ������ � web �������, ������� %d �� %d ", ARRAYOFCONST((MaxErrorCount - TData[it].ErrorCount + 1, MaxErrorCount))));
					delete tmp3;
					TData[it].ErrorCount--;
					if (TData[it].ErrorCount == 0) {

						TData[it].State = tActionState::asError;
						break;

					} else { Sleep(500); break; };
				}
				Sleep(500);
			}
		}
		Sleep(500);

		// �������� ������������ �����
		cs->Enter();
		for (int it = TData.size() - 1; it >= 0; it--)
			if (TData[it].CanDelete)
			{
				cs->Enter();
				if (TData[it].SendData) delete (TMemoryStream*)TData[it].SendData; //???
				if (TData[it].ReceivedData) delete (TStringStream*)TData[it].ReceivedData;
				TData.erase(TData.begin() + it);  //???
				cs->Leave();
			}
		cs->Leave();
	}
	EndWorkOkFlag = true;
}

tTransactionData* WebServiceEKASUI::GetFirstProcessedItem(void)
{
	cs->Enter();
	for (unsigned int idx = 0; idx < TData.size(); idx++)
		if (((TData[idx].State == asProcessed) || (TData[idx].State == tActionState::asError)) && (!TData[idx].CanDelete))  {

			cs->Leave();
			return &TData[idx];
		}
	cs->Leave();
	return NULL;
}

bool WebServiceEKASUI::DeleteFirstProcessedItem()
{
	cs->Enter();
	bool Res = false;
	for (unsigned int idx = 0; idx < TData.size(); idx++)
		if (((TData[idx].State == asProcessed) || (TData[idx].State == tActionState::asError)) && (!TData[idx].CanDelete))
		{
			TData[idx].CanDelete = true;
			Res = true;
			break;
		}
	cs->Leave();
	return Res;
/*
	std::vector<tTransactionData>  :: iterator it;

	for (it = TData.begin(); it < TData.end() ; it++)

		if (it->State == asProcessed) {
			cs->Enter();

			if (it->SendData) delete it->SendData;
			if (it->ReceivedData) delete it->ReceivedData;
			TData.erase(it);
			cs->Leave();
			return true;
		}
	return false; */
}

/*TStringStream**/ void WebServiceEKASUI::GetEtbDm(int TypeSD, int Doroga)
{
/*
	tTransactionData newItem;
	newItem.DataType = aGetEtbDm;
	newItem.State = asReadyToProcessed;

	TStringStream* tmp = new TStringStream();
	tmp->WriteString("<?xml version=\u00221.0\u0022 encoding=\u0022UTF-8\u0022?>");
	tmp->WriteString("<SDrequest> ������!!!");
	tmp->WriteString("<TypeSD>" + IntToStr(TypeSD) + "</TypeSD>");
	tmp->WriteString("<Doroga>" + IntToStr(Doroga) + "</Doroga>");
	tmp->WriteString("</SDrequest>");

	newItem.SendData = tmp;

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();

*/
	tTransactionData newItem;
	newItem.DataType = aGetEtbDm;
	newItem.ErrorCount = MaxErrorCount;
	newItem.State = asReadyToProcessed;
	newItem.Tag = Doroga + TypeSD * 1000;

	UnicodeString xml_text;
	xml_text = "<?xml version=\u00221.0\u0022 encoding=\u0022UTF-8\u0022?>";
	xml_text = xml_text + "<SDrequest>";
	xml_text = xml_text + "<TypeSD>" + IntToStr(TypeSD) + "</TypeSD>";
	xml_text = xml_text + "<Doroga>" + IntToStr(Doroga) + "</Doroga>";
	xml_text = xml_text + "</SDrequest>";

	TStringStream* tmp = new TStringStream();
	tmp->WriteString(xml_text);
	newItem.SendData = tmp;

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
};

/*TMemoryStream* */ void WebServiceEKASUI::GetBPD(int Doroga)
{
	tTransactionData newItem;
	newItem.DataType = aGetBPD;
	newItem.State = asReadyToProcessed;
	newItem.ErrorCount = MaxErrorCount;
	newItem.Tag = Doroga;

	UnicodeString xml_text;
	xml_text = "<?xml version=\u00221.0\u0022 encoding=\u0022UTF-8\u0022?>";
	xml_text = xml_text + "<ASSETrequest>";
	UnicodeString tmp_text = IntToStr(Doroga);
	if (tmp_text.Length() < 2) tmp_text = "0" + tmp_text;
	xml_text = xml_text + "<SiteID>" + tmp_text + "</SiteID>";
	xml_text = xml_text + "</ASSETrequest>";

	TStringStream* tmp = new TStringStream();
	tmp->WriteString(xml_text);
	newItem.SendData = tmp;

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
};
/*
TStringStream* WebServiceEKASUI::GetDefect(int Doroga, int pathType, int pathID, int startKM, int startM, int endKM, int endM)
{
	tTransactionData newItem;
	newItem.ErrorCount = MaxErrorCount;
	newItem.DataType = aGetDefect;

	UnicodeString xml_text;
	xml_text = "<?xml version=\u00221.0\u0022 encoding=\u0022UTF-8\u0022?>";
	xml_text = xml_text + "<Defectrequest>";
	UnicodeString tmp_text = IntToStr(Doroga);
	if (tmp_text.Length() < 2) tmp_text = "0" + tmp_text;
	xml_text = xml_text + "SiteID=\u0022" + tmp_text + "\u0022 ";
	xml_text = xml_text + "pathType=\u0022" + IntToStr(pathType) + "\u0022 ";
	xml_text = xml_text + "pathID=\u0022" + IntToStr(pathID) + "\u0022 ";
	xml_text = xml_text + "startKM=\u0022" + IntToStr(startKM) + "\u0022 ";
	xml_text = xml_text + "startM=\u0022" + IntToStr(startM) + "\u0022 ";
	xml_text = xml_text + "endKM=\u0022" + IntToStr(endKM) + "\u0022 ";
	xml_text = xml_text + "endM=\u0022" + IntToStr(endM) + "\u0022>";
	xml_text = xml_text + "</Defectrequest>";

	TStringStream* tmp = new TStringStream();
	tmp->WriteString(xml_text);
	newItem.SendData = tmp;
//	newItem.SendData = new TStringList;
//	newItem.SendData->Text = xml_text;

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
};
*/
bool WebServiceEKASUI::SendVideoIncidents(UnicodeString FileName, int Tag)
{
	cs->Enter();
	bool ExitFlag = false;
	for (unsigned int it = 0; it < TData.size(); it++)
		if (TData[it].FileName.Compare(FileName) == 0) {
			ExitFlag = true;
			break;
		}
	cs->Leave();
	if (ExitFlag) return false;

	tTransactionData newItem;
	newItem.ErrorCount = MaxErrorCount;
	newItem.DataType = aSendVideoIncidents;
	newItem.State = asReadyToProcessed;
	newItem.FileName = FileName;
	newItem.Tag = Tag;

	try
		{
			TMemoryStream *Source = new TMemoryStream();
			Source->LoadFromFile(FileName);
			newItem.SendData = Source;
		}
	catch(EIdException &E)
		{
			return false;
		}

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
	return true;
};

bool WebServiceEKASUI::SendDefect(UnicodeString FileName, int Tag)
{
	cs->Enter();
	bool ExitFlag = false;
	for (unsigned int it = 0; it < TData.size(); it++)
		if (TData[it].FileName.Compare(FileName) == 0) {
			ExitFlag = true;
			break;
		}
	cs->Leave();
	if (ExitFlag) return false;

	tTransactionData newItem;
	newItem.ErrorCount = MaxErrorCount;
	newItem.DataType = aSendDefect;
	newItem.State = asReadyToProcessed;
	newItem.FileName = FileName;
	newItem.Tag = Tag;

	try
		{
			TMemoryStream *Source = new TMemoryStream();
			Source->LoadFromFile(FileName);
			newItem.SendData = Source;
		}
	catch(EIdException &E)
		{
			return false;
		}

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
	return true;
};

void WebServiceEKASUI::Ping(void)
{
	tTransactionData newItem;
	newItem.ErrorCount = 1;
	newItem.DataType = aPing;
	newItem.State = asReadyToProcessed;
	newItem.SendData = new TStringStream();

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
};

void WebServiceEKASUI::URL(UnicodeString Url)
{
	tTransactionData newItem;
	newItem.ErrorCount = 1;
	newItem.DataType = aURL;
	newItem.State = asReadyToProcessed;
	newItem.SendData = new TStringStream();
	newItem.FileName = Url;

	cs->Enter();
	TData.push_back(newItem);
	cs->Leave();
};


void __fastcall WebServiceEKASUI::SocketErrorEvent(System::TObject* Sender, TCustomWinSocket* Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
	cs->Enter();
	cs->Leave();
}




