//---------------------------------------------------------------------------

#ifndef XMLPerserUnitH
#define XMLPerserUnitH

#include <System.Classes.hpp>
#include <XMLDOC.hpp>
#include <System.Classes.hpp>
#include <fstream>
#include <Idcodermime.HPP>
//#include <Vcl.Imaging.pngimage.hpp>
#include "TypesUnit.h";


//#include "Soap.EncdDecd.hpp"

//#include <System.NetEncoding.hpp>

//---------------------------------------------------------------------------

class tXMLParser {

	private:

	TComponent* OwnerPtr;

	public:

//	TStringList* TestOut;
	UnicodeString loadFile;
	UnicodeString stateForNewFiles;

	tIncidentList Items;

	tXMLParser(TComponent* OwnerPtr_): loadFile(""), stateForNewFiles(txtWaitingDecision) {

		OwnerPtr = OwnerPtr_;
	};


	bool LoadFile(UnicodeString fn, bool LoadPic, bool PicAsStream);
};

#endif
