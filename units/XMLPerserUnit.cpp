//---------------------------------------------------------------------------

#pragma hdrstop

#include "XMLPerserUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

/*
tXMLParser::tXMLParser(TComponent* OwnerPtr_) {

}
*/
//---------------------��������-------------------------------------------

UnicodeString GetAttr(IXMLNode *nodElement, UnicodeString AttributeName) {

	if (!nodElement->GetAttribute(AttributeName).IsNull()) return nodElement->GetAttribute(AttributeName);
												else return "";
}

bool AttrValExists(IXMLNode *nodElement, UnicodeString AttributeName) {

	if (!nodElement->GetAttribute(AttributeName).IsNull()) return true;
													  else return false;
}

bool tXMLParser::LoadFile(UnicodeString fn, bool LoadPic, bool PicAsStream) {

	if (!FileExists(fn)) return false;
	loadFile = fn;

	TXMLDocument* XMLDoc;
	IXMLNode* RootNode;
	XMLDoc = new TXMLDocument(OwnerPtr);

//	XMLDoc->Name = "XMLDoc";
	XMLDoc->FileName = fn;
	XMLDoc->LoadFromFile(fn);
	XMLDoc->Active = true;
	RootNode = XMLDoc->DocumentElement;

	if (RootNode->ChildNodes->Count == 0) { // ����: ��� + ���������

		tIncidentData data;
		data.state = stateForNewFiles;
		data.source = isUltrasound;
		data.filename = fn;
		data.NodeIndex = 0;
		data.decodeTime = GetAttr(RootNode, "decodeTime"); // decodeTime/time - ����� �����������
		data.pathID = GetAttr(RootNode, "pathID"); // pathID ="110000123053" 	��� ������� �������������� (���� �IDPUT� � ������� ������ �PUT_ST�, �PUT_GL�)
		data.pathText = GetAttr(RootNode, "pathText"); // pathText="I" 	����� ���� �� ������ (���� NPUT �� ������ ������ PUT_GL, PUT_ST)
		data.thread = 0;
		if (AttrValExists(RootNode, "thread")) data.thread = StrToInt(GetAttr(RootNode, "thread")); // thread="2"	����, ����������� ���������� (0 � �����, 1 � ������, 2 � ���)
		data.posKM = GetAttr(RootNode, "posKM"); // posKM="83" 	���������� �� �����������
		data.posM = GetAttr(RootNode, "posM"); // posM="660" 	���������� � �����������
		data.defect = GetAttr(RootNode, "DefPic");  // DefPic/defectID = "��.56.3"	��� ������� �������/��� �� ������
//		if (AttrValExists(RootNode, "thread")) data.DefType = GetAttr(RootNode, "DefType");  // DefType = "0"	��� ������� (0 - ��, 1 - ���, 2 - ����������)
		data.speedLimitID = GetAttr(RootNode, "speedLimitID");  // speedLimitID="25" 	����������� �������� � ����������� �� ����������� (����� ����������� ���������� �������)
		data.comment = GetAttr(RootNode, "comment");  // comment = "����������"	��������� �����������

		data.PicType = ptNONE;
		data.Pic = NULL;
		data.PNGPic = NULL;
		data.JPEGPic = NULL;
		if (LoadPic)
			if (AttrValExists(RootNode, "Pic")) {

				TIdDecoderMIME *Decoder;
				Decoder = new TIdDecoderMIME(OwnerPtr);


				//TMemoryStream* dsttmp;
				//dsttmp = new TMemoryStream();
				data.Pic = new TMemoryStream();
				Decoder->DecodeStream(GetAttr(RootNode, "Pic"), data.Pic);
				data.PicType = ptPNG;
				if (!PicAsStream) {

					if (data.Pic->Size != 0) {

						data.PNGPic = new TPngImage();
						data.Pic->Position = 0;
						data.PNGPic->LoadFromStream(data.Pic);

					} else data.PicType = ptNONE;

					delete data.Pic;
				}
				delete Decoder;
			};

		Items.push_back(data);
	}
	else     // ����: �����
	{
		RootNode = RootNode->ChildNodes->Nodes[0];
		UnicodeString pathID_ = GetAttr(RootNode, "pathID"); // pathID ="110000123053" 	��� ������� �������������� (���� �IDPUT� � ������� ������ �PUT_ST�, �PUT_GL�)
		UnicodeString pathText_ = GetAttr(RootNode, "pathText"); // pathText="I" 	����� ���� �� ������ (���� NPUT �� ������ ������ PUT_GL, PUT_ST)
		TDateTime decodeDate_ = GetAttr(RootNode, "decodeDate");

		IXMLNode *nodElement;
		int n = RootNode->ChildNodes->Count;

		for (int i = 0; i < n; i++)
		{
			nodElement = RootNode->ChildNodes->Nodes[i];
			if (nodElement->NodeName == "incident") {

				tIncidentData data;
				data.state = stateForNewFiles;
				data.source = isVideo;
				data.filename = fn;
				data.NodeIndex = i;

				try {

					data.decodeTime = decodeDate_ + GetAttr(nodElement, "time"); // time - ����� �����������

				} catch (...) { data.decodeTime = decodeDate_; };

				data.pathID = pathID_; // GetAttr(nodElement, "pathID"); // pathID ="110000123053" 	��� ������� �������������� (���� �IDPUT� � ������� ������ �PUT_ST�, �PUT_GL�)
				data.pathText = pathText_; // GetAttr(nodElement, "pathText"); // pathText="I" 	����� ���� �� ������ (���� NPUT �� ������ ������ PUT_GL, PUT_ST)

				data.thread = 0;
				if (AttrValExists(nodElement, "thread")) data.thread = StrToInt(GetAttr(nodElement, "thread")); // thread="2"	����, ����������� ���������� (0 � �����, 1 � ������, 2 � ���)
				data.posKM = GetAttr(nodElement, "posKM"); // posKM="83" 	���������� �� �����������
				data.posM = GetAttr(nodElement, "posM"); // posM="660" 	���������� � �����������
				data.defect = GetAttr(nodElement, "defectID");  // defectID = "��.56.3"	��� ������� �������/��� �� ������
				data.speedLimitID = GetAttr(nodElement, "speedLimitID");  // speedLimitID="25" 	����������� �������� � ����������� �� ����������� (����� ����������� ���������� �������)
				data.comment = GetAttr(nodElement, "comment");  // comment = "����������"	��������� �����������

				if (LoadPic)
					if (AttrValExists(nodElement, "Pic")) {

						TIdDecoderMIME *Decoder;
						Decoder = new TIdDecoderMIME(OwnerPtr);

						//TMemoryStream* dsttmp;
						//dsttmp = new TMemoryStream();
						data.Pic = new TMemoryStream();
						Decoder->DecodeStream(GetAttr(nodElement, "Pic"), data.Pic);

						data.PicType = ptJFIF;
						if (!PicAsStream) {
							if (data.Pic->Size != 0) {

								data.JPEGPic = new TJPEGImage();
								data.Pic->Position = 0;
								data.JPEGPic->LoadFromStream(data.Pic);

							} else data.PicType = ptNONE;

							delete data.Pic;
						}
						delete Decoder;
					}

				Items.push_back(data);
			}
		}

	}
	delete XMLDoc;
	XMLDoc = NULL;
	return true;
}
