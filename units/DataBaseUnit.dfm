object DataBase: TDataBase
  Left = 0
  Top = 0
  Caption = 'Data Base Form'
  ClientHeight = 582
  ClientWidth = 681
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 184
    Width = 681
    Height = 398
    Align = alBottom
    DataSource = IncidentsDataSource
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 681
    Height = 184
    Align = alClient
    TabOrder = 1
  end
  object Connection: TFDConnection
    Params.Strings = (
      'Database=D:\WORK\'#1045#1050#1040#1057#1059#1048'\ekasuisync-win32\database\idata.sqlite'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 56
    Top = 40
  end
  object FilesTable: TFDTable
    IndexFieldNames = 'id'
    Connection = Connection
    UpdateOptions.UpdateTableName = 'Files'
    TableName = 'Files'
    Left = 136
    Top = 40
  end
  object IncidentsTable: TFDTable
    IndexFieldNames = 'id'
    Connection = Connection
    UpdateOptions.UpdateTableName = 'Incidents'
    TableName = 'Incidents'
    Left = 232
    Top = 40
  end
  object FilesDataSource: TDataSource
    DataSet = FilesTable
    Left = 152
    Top = 224
  end
  object IncidentsDataSource: TDataSource
    DataSet = IncidentsTableQuery
    Left = 280
    Top = 224
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 600
    Top = 488
  end
  object FilesQuery: TFDQuery
    Connection = Connection
    Left = 96
    Top = 120
  end
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 464
    Top = 480
  end
  object IncidentsQuery: TFDQuery
    Connection = Connection
    Left = 280
    Top = 312
  end
  object IncidentsTableQuery: TFDQuery
    Connection = Connection
    UpdateOptions.AssignedValues = [uvUpdateMode, uvCountUpdatedRecords]
    UpdateOptions.CountUpdatedRecords = False
    UpdateOptions.UpdateTableName = 'Incidents'
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'SELECT      id,'
      '  File_id,'
      ''
      'State,'
      ''
      ' CASE Source'
      '  WHEN 0 THEN '#39#1059#1047#1050'+'#1052#39
      '  WHEN 1 THEN '#39#1042#1080#1076#1077#1086#39
      ' END AS Source,'
      ''
      '    NodeIndex,'
      '    DateTime,'
      ''
      ' CASE Source'
      '  WHEN 0 THEN Text'
      
        '  WHEN 1 THEN (SELECT Name FROM VideoDefNames WHERE (def_id = Te' +
        'xt))'
      ' END AS Text,'
      ''
      '    Comment,'
      '    SpeedLimit,'
      ''
      ''
      'CASE '
      '  WHEN Side THEN '#39#1051#1077#1074#1072#1103#39
      '  ELSE '#39#1055#1088#1072#1074#1072#1103#39' END AS Side,'
      ''
      '    Coord,'
      '    '
      
        '   (SELECT Name FROM PUT_GL WHERE (EKASUI_id = PathID)) AS PathI' +
        'D,'
      ''
      '    PathText,'
      '    PicType,'
      '    Picture,'
      ''
      
        '   (SELECT FileName_wo_Path FROM Files WHERE (id = File_id)) AS ' +
        'File_Name_,'
      ''
      'CASE'
      #9'WHEN InProcessing'
      #9#9'THEN '#39#1054#1073#1088#1072#1073#1086#1090#1082#1072'...'#39
      #9#9' ELSE State'
      #9'END   AS State_'
      ''
      'FROM Incidents'
      'ORDER BY id')
    Left = 184
    Top = 120
  end
  object PUT_GLTable: TFDTable
    IndexFieldNames = 'id'
    Connection = Connection
    UpdateOptions.UpdateTableName = 'PUT_GL'
    TableName = 'PUT_GL'
    Left = 104
    Top = 312
  end
  object UpdatePUTGLQuery: TFDQuery
    Connection = Connection
    Left = 304
    Top = 120
  end
end
