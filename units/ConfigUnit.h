//---------------------------------------------------------------------------

#ifndef ConfigUnitH
#define ConfigUnitH
//---------------------------------------------------------------------------

#include "IniFiles.hpp"
#include "Registry.hpp"
#include <Vcl.Forms.hpp>

enum tSendMode {

	smAuto = 0,
	smManual = 1
};

enum tEKASUISendMode {

	esmOff = 0,
	esmAuto = 1,
	esmManual = 2
};

enum tViewImagesMode {
	imOff = 0,
	imOnOrigin = 1,
	imOnManual = 2
};

enum tProgramMode {
	pmLOCA = 0, // LOCA ��
	pmEKASUI = 1 // EKASUI ��
};

enum tIncidentDisplayMode1 {

	idmAllSource = 0,
	idmUltrasoundSource = 1,
	idmVideoSource = 2
};

enum tIncidentDisplayMode2 {

	idmAll = 0, // ���
	idmNotTransmitted = 1, // �� ����������
	idmTransmitted = 2, // �� ����������
	idmThisDay = 3 // �� �������
	// �� ������ ??

};

const ColumnCount = 13;

class cConfig {

  public:

	UnicodeString COMportName;
	UnicodeString SearchPath;
	UnicodeString DataBasePath;
	bool SearchOn; // ����� ������
	tProgramMode ProgramMode;
	bool DebugMode; // ����� �������
	bool ExLOG; // ����� �������
	tSendMode SendMode_forUZK; // �������� �������� ���+� ����������: ����� ��������� / �������
	tSendMode SendMode_forVideo; // �������� �������� ����� ����������: ����� ��������� / �������
	tEKASUISendMode EKASUISendMode; // ����� �������� ������ � ������: ���� / ���� / ������
//	bool EnableSendToEKASUI; // ����� �������� ������ � ������: ���� / ���� / ������  ������ ��� ������� ������

//	TPoint PicViewFormPos;
//	TSize PicViewFormSize;

	tIncidentDisplayMode1 IncidentDisplayMode1;
	tIncidentDisplayMode2 IncidentDisplayMode2;
	int ColumnsWidth[ColumnCount];
	bool SkipData;
	bool StartTrayMinimized;
	bool DoNotCloseGotoToTray;
	int IncidentSaveTimeInDay;

	bool XMLEditPathState_On;
	UnicodeString XMLEditPathType;
	UnicodeString XMLEditPathID;
	UnicodeString XMLEditPathText;
	bool XMLEditNSIverState_On;
	UnicodeString XMLEditNSIver;

	tViewImagesMode ViewImagesMode; // �������� �����������
	int ViewImages_Width;

//	cConfig(UnicodeString PortName, UnicodeString WorkMode_ = "");
	cConfig();
	~cConfig();

	void Store(void);
	void Load(void);
	TCustomIniFile* OpenIniFileInstance();

};

#endif
