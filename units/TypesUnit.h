﻿//---------------------------------------------------------------------------

#ifndef TypesUnitH
#define TypesUnitH

#include <vector>
#include <System.Classes.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Dialogs.hpp>
#include "ConfigUnit.h"

/*
#pragma pack(push, 1)

typedef unsigned char tHeadID[8];

const tHeadID HeadID = {0x52, 0x53, 0x50, 0x42, 0x52, 0x41, 0x46, 0x44};         // Идентификатор  [RSPBRAFD]

typedef struct  {
	int LookedOffset;
	System::DynamicArray<System::Byte> LockedBookmark;

} TDBGridBM;

typedef enum {

   dtFile = 0x01,
   dtMessage = 0x02

} tDataType;

typedef struct  {

	unsigned int FullSize; // Размер блока данных - заголовок + файл
	tHeadID id;
	tDataType type;
	int Data;
	WCHAR FileName[255];
	int HashCode;
	WCHAR Hash[255];
	unsigned int Size; // Размер последующих данных / пересылаемого файла

} DataHeader;

#pragma pack(pop)
*/
/*
typedef enum {

		 zmsNoConnection = 0,
		 zmsTestConnection = 1,
		 zmsReady_Idly = 2,
		 zmsSend = 3,
		 zmsReceive = 4

	   } tZModemState;
*/

enum tFileType {

	tCmdFile = 0,
	tIncidentFile = 1,
	tSDFile = 2,
	tBPDFile = 3
};

typedef enum {

		 zmsClosed = 0, //
		 zmsReadyToSend = 2,
		 zmsSending = 3,
		 zmsWait = 4,
		 zmsReceiving = 5

	   } tZModemState_;

typedef enum {

		 tsStart = 0,
		 tsEnd = 1,
		 tsInProcess = 2,
		 tsError = 3

	   } tTransmissionState;

typedef enum {

		 psLeft = 0,
		 psRight = 1

	   } tPathSide;

typedef enum {

				 // Состояние xml файла па ПК LOCA

		 LOCA_fsWaitingDecision = 0, // Ожидает решения
		 LOCA_fsDecline = 1,         // Выгрузка отменена
		 LOCA_fsProcessing = 3,      // "Обрабатывается" - Файл принят на выгрузку с ПК LOCA на ПК ЕКАСУИ
		 LOCA_fsUnloading = 4,       // "Выгружается" - Файл принят на ПК ЕКАСУИ и будет выгружен в web службу
		 LOCA_fsTransferred = 2,     // Выгружен

				 // Состояние xml файла па ПК ЕКАСУИ

		 EKASUI_fsReadyToSend = 5,    // Файл находится на ПК ЕКАСУИ
		 EKASUI_fsProcessing = 6,     // Файл выгружается в web службу ЕКАСУИ
		 EKASUI_fsTransferred = 7,    // Файл выгружен ЕКАСУИ
		 EKASUI_fsClosed = 8          // Информация о том что файл выгружен в ЕКАСУИ передана на LOCA ПК

	   } tXMLFileState;


/*
typedef enum {

		 isWaitingDecision = 0, // Ожидает решения
		 isDecline = 1, // Отклонен
		 isTransferred = 2 // Выгружен

	   } tIncidentFileState;
*/

//bool ExLOG = false;
//extern const bool ExLOG;

extern const UnicodeString CmdFileName;
extern const UnicodeString SDFileName;
extern const UnicodeString BPDFileName;

extern const UnicodeString txtWaitingDecision;
extern const UnicodeString txtAccept;
//UnicodeString txtUnloaded = "Выгружается"; // Файл принят на ПК ЕКАСУИ и будет выгружен в web службу
extern const UnicodeString txtTransferred;
extern const UnicodeString txtDecline;
extern const UnicodeString txtNotTransferred;

extern const UnicodeString txtProcessing;

// ✓ ✔ ☑ ☐ ☒

//UnicodeString txtTransferDecline = "Передан";

/*
		 txtWaitingDecision = "Нет решения";
		 txtAccept = "Отсылать";
		 txtDecline = "Не отсылать";
		 txtTransferred = "Идет передача";
		 txtTransferDecline = "Передан";
*/

typedef enum {

		 isUltrasound = 0,
		 isVideo = 1

	   } tIncidentSource;

typedef enum {

		 ptNONE = 0,
		 ptPNG = 1,
		 ptJFIF = 2

	   } tPicType;

typedef struct /*tIncidentData*/ {

	UnicodeString state; // Состояние
	UnicodeString filename; // Имя файла

	int NodeIndex; // Номер ветки в XML файле
	tIncidentSource source; // Тип данных УЗК/видео
	UnicodeString decodeTime; // decodeTime/time - время расшифровки
	UnicodeString pathID; // pathID ="110000123053" 	Код объекта инфраструктуры (поле «IDPUT» в наборах данных «PUT_ST», «PUT_GL»)
	UnicodeString pathText; // pathText="I" 	Номер пути из ЕКАСУИ (поле NPUT из набора данных PUT_GL, PUT_ST)
	int thread; // thread="2"	Нить, статический справочник (0 – левая, 1 – правая, 2 – обе)
	UnicodeString posKM; // posKM="83" 	Координата КМ отступления
	UnicodeString posM; // posM="660" 	Координата М отступления
	UnicodeString defect; // DefPic/defectID = "ДС.56.3"	Код рисунка дефекта/Код из ЕКАСУИ
//	int DefType; // DefType = "0"	Тип дефекта (0 - ДР, 1 - ОДР, 2 - преддефект)
	UnicodeString speedLimitID; // speedLimitID="25" 	Ограничение скорости в зависимости от отступления (может указываться оператором вручную)
	UnicodeString comment; // comment = "примечание"	Свободный окмментарий
	TPngImage* PNGPic;
	TJPEGImage* JPEGPic;
	TMemoryStream* Pic;
	tPicType PicType;

//	tIncidentData( Pic = NULL; ) { }
//	~tIncidentData() { if (Pic) delete Pic }

} tIncidentData;

typedef std::vector <tIncidentData> tIncidentList;

UnicodeString VideoCODEtoText(UnicodeString* defect);

cConfig* GetConfig(void);

UnicodeString NextState(UnicodeString State);

UnicodeString MakeNameUnique(UnicodeString FileName);
UnicodeString AddTagToName(UnicodeString FileName, int Tag);
tFileType AnalyzeFileName(UnicodeString FileName, UnicodeString* ResFileName, int* Tag);

//---------------------------------------------------------------------------
#endif
