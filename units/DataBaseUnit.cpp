//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DataBaseUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDataBase *DataBase;

//---------------------------------------------------------------------------
__fastcall TDataBase::TDataBase(TComponent* Owner)
	: TForm(Owner)
{
}

//---------------------------------------------------------------------------

void TDataBase::Connect(UnicodeString DataBaseFileName)
{
	Connection->Params->Strings[0] = "Database=" + DataBaseFileName;
//	Connection->ConnectionString = "Database=" + DataBaseFileName + ";DriverID=SQLite";

	Connection->Connected = true;
	IncidentsTableQuery->Active = true;
	FilesTable->Active = true;
//	IncidentsTable->Active = true;
	PUT_GLTable->Active = true;
}
//---------------------------------------------------------------------------

void TDataBase::CloseConnection(void)
{
	IncidentsTableQuery->Active = false;
	FilesTable->Active = false;
	PUT_GLTable->Active = false;
	Connection->Connected = false;
}
//---------------------------------------------------------------------------

bool TDataBase::XMLFileExists(UnicodeString FileName)
{
	FilesQuery->Close();
	FilesQuery->Open("SELECT * FROM Files WHERE FileName = '" + FileName + "'"); // ��������� �� ����������� ������
	return (!FilesQuery->Eof);
}
//---------------------------------------------------------------------------

int TDataBase::AddXMLFile(UnicodeString FileName,
						  tIncidentSource Type,
						  tXMLFileState State,
						  bool* OKFlag,
						  int Tag)
{
	FilesQuery->Close();
	FilesQuery->Open("SELECT * FROM Files WHERE FileName = '" + FileName + "'"); // ��������� �� ����������� ������

	if (FilesQuery->Eof) {

		if (AddToLog) AddToLog("�������� ������� ����� � ���� - ����� ����� ���");
		cs->Enter();

	//	System::TDateTime FileDateTime;
	//	FileAge(FileName, FileDateTime);
		FilesQuery->Close();
		FilesQuery->SQL->Text = Format("INSERT INTO Files ('FileName', 'FileName_wo_Path', 'Type', 'State', 'Modify', 'ModifyFileName', 'Tag') VALUES ('%s', '%s', '%d', '%d', 'False', '', '%d');", ARRAYOFCONST((FileName, ExtractFileName(FileName), (int)Type, (int)State, Tag)));
		FilesQuery->ExecSQL();
		FilesQuery->Open("SELECT id FROM Files WHERE rowid=last_insert_rowid();");
		FilesQuery->Last();
		int id = FilesQuery->FieldByName("id")->AsInteger;
		FilesQuery->Close();

		if (AddToLog) AddToLog("���� ���������� "  + FileName + " �������� � �������");
		if (OKFlag) *OKFlag = true;

		cs->Leave();
		return id;
	} else {
		if (AddToLog) AddToLog("�������� ������� ����� � ���� - ���� ���� ����");
		if (OKFlag) *OKFlag = false;
		return -1;
	}
}
//---------------------------------------------------------------------------

int TDataBase::AddIncident(int XMLFile_id,
						   UnicodeString State,
						   tIncidentSource Source,
						   int NodeIndex,
						   TDateTime dt,
						   UnicodeString Text,
						   UnicodeString Comment,
						   UnicodeString SpeedLimit,
						   tPathSide Side,
						   UnicodeString Coord,
						   UnicodeString PathID,
						   UnicodeString PathText,
						   tPicType PicType,
						   TStream* Picture)
{
	cs->Enter();

	IncidentsQuery->Close();
	IncidentsQuery->SQL->Text = Format("INSERT INTO Incidents ('File_id', 'State', 'Source', 'NodeIndex', 'Text', 'Comment', 'SpeedLimit', 'Side', 'Coord', 'PathID', 'PathText', 'PicType', 'AddDate') VALUES ('%d', '%s', '%d', '%d', '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%d', '%s');",
							  ARRAYOFCONST((XMLFile_id,
											State,
											(int)Source,
											NodeIndex,
											Text,
											Comment,
											SpeedLimit,
											Side,
											Coord,
											PathID,
											PathText,
											(int)PicType,
											DateToStr(Now()))));

//	FilesQuery->SQL->Text = Format("INSERT INTO Files ('FileName', 'FileName_wo_Path', 'Type', 'State', 'Modify', 'ModifyFileName', ) VALUES ('%s', '%s', '%d', '%d', 'False', '', '%s');", ARRAYOFCONST((FileName, ExtractFileName(FileName), (int)Type, (int)State, DateTimeToStr(Now() /*FileDateTime*/))));

	IncidentsQuery->ExecSQL();
	IncidentsQuery->Close();
	IncidentsQuery->Open("SELECT id FROM Incidents WHERE rowid=last_insert_rowid();");
	IncidentsQuery->Last();

	int id = IncidentsQuery->FieldByName("id")->AsInteger;
	IncidentsQuery->Close();
	IncidentsQuery->Open("SELECT * FROM Incidents WHERE id = " + IntToStr(id));
	IncidentsQuery->Edit();
	IncidentsQuery->FieldByName("DateTime")->AsDateTime = dt;
	if (Picture) ((TBlobField*)IncidentsQuery->FieldByName("Picture"))->LoadFromStream(Picture);
	IncidentsQuery->Post();

	cs->Leave();

	return id;
}

bool TDataBase::GetReadyLOCAFile(UnicodeString* SendFileName, int* FileId) {

	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files WHERE State = " + IntToStr(LOCA_fsWaitingDecision)); // ��������� �� ����������� ������
	FilesQuery->First();

	bool ExitFlag = false;
	for (int xml_idx_ = 0; xml_idx_ < FilesQuery->RecordCount; xml_idx_++) { // �� ����������� �����: State = isWaitingDecision

		int xml_idx = FilesQuery->FieldByName("id")->AsInteger;
		if (FileId) *FileId = xml_idx;

//		UnicodeString tmp = "SELECT * FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + ") AND (State = " + txtWaitingDecision + ");";
//		IncidentsQuery->Open(tmp); // ���������� � �� �������� �������� ...
		IncidentsQuery->Open("SELECT * FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + ") AND (State = '" + txtWaitingDecision + "');"); // ���������� � �� �������� �������� ...
		if (IncidentsQuery->RecordCount == 0) {  // ... ��� �����!!! ���� �����, ��������� ���

			IncidentsQuery->Close();
			IncidentsQuery->Open("SELECT Count(id) FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + ");"); // ��� ���������� ����� xml_idx ...
//			int i_count = IncidentsQuery->RecordCount; // ... �� ����������
			int All_I_cnt = IncidentsQuery->Fields->Fields[0]->AsInteger;
			IncidentsQuery->Close();

			IncidentsQuery->Open("SELECT Count(id) FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + ") AND (State = '" + txtDecline + "');"); // �� ����������� ����������
			int NoUnload_I_cnt = IncidentsQuery->Fields->Fields[0]->AsInteger;
			IncidentsQuery->Close();

			// �������������� XML - ������ ��� ����� XML
			if ((FilesQuery->FieldByName("FileName")->AsString.Pos("video") != 0) && (GetConfig()->XMLEditPathState_On))  {

				TXMLDocument* XMLDoc_;
				XMLDoc_ = new TXMLDocument(this);

				UnicodeString fn = FilesQuery->FieldByName("FileName")->AsString;

				XMLDoc_->Name = "XMLDoc_";
				XMLDoc_->FileName = fn;
				XMLDoc_->LoadFromFile(fn);
				XMLDoc_->Active = true;

				IXMLNode* WorkNode_ = XMLDoc_->DocumentElement->ChildNodes->Nodes[0];
				WorkNode_->SetAttribute("pathType", GetConfig()->XMLEditPathType);
				WorkNode_->SetAttribute("pathID", GetConfig()->XMLEditPathID);
				WorkNode_->SetAttribute("pathText", GetConfig()->XMLEditPathText);

				XMLDoc_->SaveToFile(fn);
				delete XMLDoc_;
			}

			if ((FilesQuery->FieldByName("FileName")->AsString.Pos("video") != 0) && (GetConfig()->XMLEditNSIverState_On))  {

				TXMLDocument* XMLDoc_;
				XMLDoc_ = new TXMLDocument(this);

				UnicodeString fn = FilesQuery->FieldByName("FileName")->AsString;

				XMLDoc_->Name = "XMLDoc_";
				XMLDoc_->FileName = fn;
				XMLDoc_->LoadFromFile(fn);
				XMLDoc_->Active = true;

//				IXMLNode* WorkNode_ = XMLDoc_->DocumentElement->ChildNodes->Nodes[0];
				XMLDoc_->DocumentElement->SetAttribute("NSIver", GetConfig()->XMLEditNSIver);

				XMLDoc_->SaveToFile(fn);
				delete XMLDoc_;
			}


			if (NoUnload_I_cnt != 0) { // ���� �� ����������� ����������

				if (NoUnload_I_cnt == All_I_cnt) { /* ��� �� ������  */

					// ��������� ��������� ���������� - �������� ��������
				  /*	IncidentsQuery->Open("SELECT * FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + " AND (State = " + txtDecline + "));"); // ��� �� ����������� ���������� ����� xml_idx
					IncidentsQuery->First();
					for (int Incident_idx = 0; Incident_idx < IncidentsQuery->RecordCount; Incident_idx++) {

						IncidentsQuery->FieldByName("State")->AsString = txtTransferDecline;
						IncidentsQuery->Next();
					}     */
					FilesQuery->Edit();
					FilesQuery->FieldByName("State")->AsInteger = LOCA_fsDecline;
					FilesQuery->Post();
					SetFileState(xml_idx, LOCA_fsDecline, NULL);
					SetIncidentsState_isTransferred(xml_idx);


				} else {
							// �� ��� �� ������

					IncidentsQuery->Open("SELECT NodeIndex FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + ") AND (State = '" + txtDecline + "');"); // �� ����������� ����������
					IncidentsQuery->First();

					std::vector <int> DeclineList; // ������ (������ �����) �� ����������� �����������
					for (int Incident_idx = 0; Incident_idx < NoUnload_I_cnt; Incident_idx++) {

						DeclineList.push_back(IncidentsQuery->FieldByName("NodeIndex")->AsInteger);
						IncidentsQuery->Next();
					}
							// �������� �� xml �� ����������� �����������
					TXMLDocument* XMLDoc;
					//IXMLNode* RootNode;
					XMLDoc = new TXMLDocument(this);

					UnicodeString fn = FilesQuery->FieldByName("FileName")->AsString;

					XMLDoc->Name = "XMLDoc";
					XMLDoc->FileName = fn;
					XMLDoc->LoadFromFile(fn);
					XMLDoc->Active = true;

					IXMLNode* WorkNode = XMLDoc->DocumentElement->ChildNodes->Nodes[0];
					for (int i = DeclineList.size() - 1; i >= 0; i--)
						WorkNode/*XMLDoc->DocumentElement*/->ChildNodes->Delete(DeclineList[i]);

					UnicodeString ModifyFileName = TPath::GetFileNameWithoutExtension(fn) + "_edited_" + TPath::GetExtension(fn);
					XMLDoc->SaveToFile(ModifyFileName);
					delete XMLDoc;
					*SendFileName = ModifyFileName;
					FilesQuery->Edit();
					FilesQuery->FieldByName("Modify")->AsBoolean = true;
					FilesQuery->FieldByName("ModifyFileName")->AsString = ModifyFileName;
					FilesQuery->Post();
					ExitFlag = true;
				}
			}
			else { // ���� ����������� �������

				*SendFileName = FilesQuery->FieldByName("FileName")->AsString;
				ExitFlag = true;

			};

			if (ExitFlag) {
			 /*
				// ��������� ��������� ���������� - ��������
				IncidentsQuery->Open("SELECT * FROM Incidents WHERE (File_id = " + IntToStr(xml_idx) + " AND (State = " + txtAccept + "));"); // ��� ����������� ���������� ����� xml_idx
				IncidentsQuery->First();
				for (int Incident_idx = 0; Incident_idx < IncidentsQuery->RecordCount; Incident_idx++) {

					IncidentsQuery->FieldByName("State")->AsString = txtTransferred;
					IncidentsQuery->Next();
				}

				FilesQuery->FieldByName("State")->AsString = txtTransferred; */
				cs->Leave();
				return true;
			}
		}
		FilesQuery->Next();
	}

	cs->Leave();

	return false;
}
//------------------------------------------------------------------------------

bool TDataBase::GetReadyEKASUIFile(UnicodeString* SendFileName, tIncidentSource* Source, int* RecId) {

//	UnicodeString tmp1 = *SendFileName;
//	int tmp2 = ;
//	int tmp3
	if (GetConfig()->ExLOG) if (AddToLog) AddToLog(Format("ENTER GetReadyEKASUIFile - params: %s, %d, %d ", ARRAYOFCONST((*SendFileName, (int)(*Source), *RecId))));

	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files WHERE State = " + IntToStr(EKASUI_fsReadyToSend)); // ��������� �� ����������� ������
	FilesQuery->First();

	if (GetConfig()->ExLOG) if (AddToLog) AddToLog("Make SELECT");

	if (!FilesQuery->Eof) {

		*SendFileName = FilesQuery->FieldByName("FileName")->AsString;
		*RecId = FilesQuery->FieldByName("id")->AsInteger;
		*Source = (tIncidentSource)FilesQuery->FieldByName("Type")->AsInteger;

		if (GetConfig()->ExLOG) if (AddToLog) AddToLog(Format("Get DATA: %s, %d, %d ", ARRAYOFCONST((*SendFileName, (int)(*Source), *RecId))));
		cs->Leave();
		return true;
	}
	else {

		if (GetConfig()->ExLOG) if (AddToLog) AddToLog("NO DATA");
		cs->Leave();
		return false;
	}
}
//------------------------------------------------------------------------------

bool TDataBase::GetLOCAProcessingFiles(void) {

	if (GetConfig()->ExLOG) if (AddToLog) AddToLog("Get EKASUI Processing Files");
	ProcessingFilesList.clear();

	bool Res = false;
	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files WHERE State = " + IntToStr(LOCA_fsProcessing)); // ��������� �� ����������� ������
	FilesQuery->First();

	while (!FilesQuery->Eof) {

		tProcessingFilesDateItem tmp;
		tmp.fn = FilesQuery->FieldByName("FileName")->AsString;
		tmp.Param1 = FilesQuery->FieldByName("id")->AsInteger;
		tmp.Param2 = FilesQuery->FieldByName("Type")->AsInteger;

		ProcessingFilesList.push_back(tmp);
		Res = true;
		if (GetConfig()->ExLOG) if (AddToLog) AddToLog(Format("fn: %s, id: %d, Type: %d ", ARRAYOFCONST((tmp.fn, tmp.Param1, tmp.Param2))));
		FilesQuery->Next();
	}
	cs->Leave();
	return Res;
}
//------------------------------------------------------------------------------

bool TDataBase::GetEKASUIProcessingFiles(void) {

	if (GetConfig()->ExLOG) if (AddToLog) AddToLog("Get EKASUI Processing Files");
	ProcessingFilesList.clear();

	bool Res = false;
	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files WHERE State = " + IntToStr(EKASUI_fsProcessing)); // ��������� �� ����������� ������
	FilesQuery->First();

	while (!FilesQuery->Eof) {

		tProcessingFilesDateItem tmp;
		tmp.fn = FilesQuery->FieldByName("FileName")->AsString;
		tmp.Param1 = FilesQuery->FieldByName("id")->AsInteger;
		tmp.Param2 = FilesQuery->FieldByName("Type")->AsInteger;

		ProcessingFilesList.push_back(tmp);
		Res = true;
		if (GetConfig()->ExLOG) if (AddToLog) AddToLog(Format("fn: %s, id: %d, Type: %d ", ARRAYOFCONST((tmp.fn, tmp.Param1, tmp.Param2))));
		FilesQuery->Next();
	}
	cs->Leave();
	return Res;
}

/*
void TDataBase::SetEKASUIFileState(int id, tXMLFileState State) {

	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files WHERE id = " + IntToStr(id));

	if (!FilesQuery->Eof) {

		FilesQuery->First();
		FilesQuery->Edit();
		FilesQuery->FieldByName("State")->AsInteger = (int)State;
		FilesQuery->Post();
	}
	cs->Leave();
}  */
//------------------------------------------------------------------------------
bool TDataBase::SetFileState(int File_Id, tXMLFileState State, UnicodeString* XMLFileName) {

	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files WHERE id = " + IntToStr(File_Id));

	bool OKFlag = false;
	if (!FilesQuery->Eof) {

		FilesQuery->First();
		FilesQuery->Edit();
		FilesQuery->FieldByName("State")->AsInteger = (int)State;
		if ((State == LOCA_fsTransferred) || (State == EKASUI_fsClosed)) FilesQuery->FieldByName("UnloadingDate")->AsDateTime = Now();
		FilesQuery->Post();
		if (XMLFileName) *XMLFileName = FilesQuery->FieldByName("FileName")->AsString;
		OKFlag = true;
	}
	cs->Leave();

	if (OKFlag) {
		IncidentsTableQuery->DisableControls();
		IncidentsTableQuery->Refresh();
		IncidentsTableQuery->EnableControls();
	}

	return OKFlag;
}
//------------------------------------------------------------------------------

void TDataBase::GetFilesWithState(tXMLFileState State) {

	cs->Enter();
	FilesList.clear();
	FilesQuery->Open("SELECT * FROM Files WHERE State = " + IntToStr(State));
	FilesQuery->First();

	// if (AddToLog) AddToLog("GetFilesWithState"  + IntToStr((int)State));

	while (!FilesQuery->Eof) {

		FilesListItem newItem;
		newItem.Tag = FilesQuery->FieldByName("Tag")->AsInteger;
		newItem.id = FilesQuery->FieldByName("id")->AsInteger;
		if (GetConfig()->ExLOG) if (AddToLog) AddToLog(" - Tag: "  + IntToStr(FilesQuery->FieldByName("Tag")->AsInteger) + "; id: "  + IntToStr(FilesQuery->FieldByName("id")->AsInteger));
		FilesList.push_back(newItem);
		FilesQuery->Next();
	}

	cs->Leave();
}
//------------------------------------------------------------------------------

void TDataBase::SetIncidentsState_isTransferred(int File_Id) {

	cs->Enter();

	IncidentsQuery->Open("SELECT * FROM Incidents WHERE (File_id = " + IntToStr(File_Id) + ") AND (State = '" + txtAccept + "');"); // ���������� � �������� �������� ...
	IncidentsQuery->First();
	for (int Incident_idx = 0; Incident_idx < IncidentsQuery->RecordCount; Incident_idx++) {

		IncidentsQuery->Edit();
		IncidentsQuery->FieldByName("State")->AsString = txtTransferred;
		IncidentsQuery->Post();
		IncidentsQuery->Next();
	}

	IncidentsQuery->Open("SELECT * FROM Incidents WHERE (File_id = " + IntToStr(File_Id) + ") AND (State = '" + txtDecline + "');"); // ���������� � ���������� �������� ...
	IncidentsQuery->First();
	for (int Incident_idx = 0; Incident_idx < IncidentsQuery->RecordCount; Incident_idx++) {

		IncidentsQuery->Edit();
		IncidentsQuery->FieldByName("State")->AsString = txtNotTransferred;
		IncidentsQuery->Post();
		IncidentsQuery->Next();
	}

	IncidentsTableQuery->DisableControls();
	IncidentsTableQuery->Refresh();
	IncidentsTableQuery->EnableControls();

	cs->Leave();
}
//------------------------------------------------------------------------------

void TDataBase::CheckAndFixDataIntegrity(void) {

	cs->Enter();

	FilesQuery->Open("SELECT * FROM Files"); // ��������� ���� ������

	bool Res = false;
	while (!FilesQuery->Eof) {

		// �������� �� ���� ������� ���� ����������� �������������� �� xml �����
		if ((!FileExists(AddTagToName(FilesQuery->FieldByName("FileName")->AsString, FilesQuery->FieldByName("id")->AsInteger))) &&
			(!FileExists(  FilesQuery->FieldByName("FileName")->AsString))) {

			IncidentsQuery->ExecSQL("DELETE FROM Incidents WHERE File_id = " + IntToStr(FilesQuery->FieldByName("id")->AsInteger));

			FilesQuery->Edit();
			FilesQuery->FieldByName("Type")->AsInteger = 9999;
			FilesQuery->Post();

			Res = true;
		}


		// �������� �� ���� ������� ���� ��������� �� ����� ��������
/*		if (((FilesQuery->FieldByName("State")->AsInteger == LOCA_fsTransferred) || (FilesQuery->FieldByName("State")->AsInteger == EKASUI_fsClosed)) &&
			 (DaysBetween((TDate)Now(), FilesQuery->FieldByName("UnloadingDate")->AsDateTime) > GetConfig()->IncidentSaveTimeInDay)) {

			IncidentsQuery->ExecSQL("DELETE FROM Incidents WHERE File_id = " + IntToStr(FilesQuery->FieldByName("id")->AsInteger));

			FilesQuery->Edit();
			FilesQuery->FieldByName("Type")->AsInteger = 9999;
			FilesQuery->Post();

			Res = true;
		}*/
		FilesQuery->Next();
	}

	FilesQuery->Close();
	FilesQuery->ExecSQL("DELETE FROM Files WHERE Type = 9999");


	if (Res) {


		IncidentsTableQuery->Refresh();
		FilesTable->Refresh();
//		IncidentsTableQuery->Active = true;
//		FilesTable->Active = true;
	}

	cs->Leave();
}
//------------------------------------------------------------------------------

  /*
SELECT

	id,

	CASE
		WHEN ((SELECT State FROM Files WHERE id = 1) = 2) THEN '���������...'
		 ELSE '123.'
	END   AS State_

FROM Incidents
*/

void TDataBase::SetIncidentTableDisplayMode(tIncidentDisplayMode1 Mode1, tIncidentDisplayMode2 Mode2) {

	UnicodeString SQL;
	SQL =       " SELECT      id,                                                             ";
	SQL = SQL + "   File_id,                                                                  "
			  + " State,                                                                      "
			  + "  CASE Source                                                                "
			  + "   WHEN 0 THEN '���+�'                                                       "
			  + "   WHEN 1 THEN '�����'                                                       "
			  + "  END AS Source,                                                             "
			  + "     NodeIndex,                                                              "
			  + "     DateTime,                                                               "
			  + "  CASE Source                                                                "
			  + "   WHEN 0 THEN Text                                                          "
			  + "   WHEN 1 THEN (SELECT Name FROM VideoDefNames WHERE (def_id = Text))        "
			  + "  END AS Text,                                                               "
			  + "     Comment,                                                                "
			  + "     SpeedLimit,                                                             "
			  + " CASE                                                                        "
			  + "   WHEN Side THEN '�����'                                                    "
			  + "   ELSE '������' END AS Side,                                                "
			  + "     Coord,                                                                  "
			  + "    (SELECT Name FROM PUT_GL WHERE (EKASUI_id = PathID)) AS PathID,          "
			  + "     PathText,                                                               "
			  + "     PicType,                                                                "
			  + "     Picture,                                                                "
			  + "    (SELECT FileName_wo_Path FROM Files WHERE (id = File_id)) AS File_Name_, "
			  + " CASE                                                                        "
			  + "   WHEN (((SELECT State FROM Files WHERE id = File_id) = " + IntToStr(LOCA_fsProcessing) + ")" +
			  + "      OR ((SELECT State FROM Files WHERE id = File_id) = " + IntToStr(LOCA_fsUnloading) + "))" +
			  + " 		THEN '" + txtProcessing + "'"
			  + " 		 ELSE State                                                           "
			  + " 	END AS CalcState                                                          "
			  + " FROM Incidents                                                              ";


	bool WHEREinSQLText = false;
	switch (Mode2) {

			case idmAll: {
							break;
						 }
 case idmNotTransmitted: {
							SQL = SQL + " WHERE State = '" + txtWaitingDecision + "' OR State = '" + txtAccept + "' OR State = '" + txtDecline + "'";
							WHEREinSQLText = true;
							break;
						 }
	case idmTransmitted: {
							SQL = SQL + " WHERE State = '" + txtTransferred + "' OR State = '" + txtNotTransferred + "'";
							WHEREinSQLText = true;
							break;
						 }
		case idmThisDay: {
							SQL = SQL + " WHERE AddDate = '" + DateToStr((TDate)Now()) + "'";
							WHEREinSQLText = true;
							break;
						 }
	}


	switch (Mode1) {

		   case idmAllSource: {
								   break;
							  }
	case idmUltrasoundSource: {
								   if (WHEREinSQLText) SQL = SQL + " AND Source = 0";
												  else SQL = SQL + " WHERE Source = 0";
								   break;
							  }
		 case idmVideoSource: {
								   if (WHEREinSQLText) SQL = SQL + " AND Source = 1";
												  else SQL = SQL + " WHERE Source = 1";
								   break;
							  }
	}


	SQL = SQL + " ORDER BY id ";

	cs->Enter();
	IncidentsTableQuery->Close();
	IncidentsTableQuery->SQL->Text = SQL;
	IncidentsTableQuery->DisableControls();
	IncidentsTableQuery->Open("");
//	IncidentsTableQuery->Refresh();
	IncidentsTableQuery->EnableControls();
	cs->Leave();
}
//------------------------------------------------------------------------------

void __fastcall TDataBase::FormCreate(TObject *Sender)
{
	cs = new TCriticalSection();
	AddToLog = NULL;
}
//---------------------------------------------------------------------------


void __fastcall TDataBase::FormDestroy(TObject *Sender)
{
	delete cs;
}
//---------------------------------------------------------------------------

void TDataBase::setLog(void (*func)(UnicodeString Text))
{
	AddToLog = func;
}
//---------------------------------------------------------------------------


//void __fastcall TDataBase::DBGrid2MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
//		  int X, int Y)
//{
//	ShowMessage("~");
/*
	if (Shift.Contains(ssShift) && (MouseDown_BM.Length != 0)) {

		MouseDown_BM = IncidentsTable->Bookmark;
		if (IncidentsTable->CompareBookmarks(MouseDown_BM, MouseDown_BM) < 0) {

			IncidentsTable->GotoBookmark(MouseDown_BM);
			IncidentsTable->Next();
			DBGrid2->SelectedRows->CurrentRowSelected = true;


		} else
		if (IncidentsTable->CompareBookmarks(MouseDown_BM, MouseDown_BM) > 0) {

		}
		IncidentsTable->FreeBookmark(MouseUp_BM);

	}
*/
//}
//---------------------------------------------------------------------------

//void __fastcall TDataBase::DBGrid2MouseDown(TObject *Sender, TMouseButton Button,
//		  TShiftState Shift, int X, int Y)
//{

//
//TBookmarkStr
//	TBookmarkStr ;
//	TBookmarkStr MouseUp_BM;

//	ShowMessage("~");
/*
	if (Shift.Contains(ssShift)) {

		MouseDown_BM = IncidentsTable->Bookmark;
		IncidentsTable->FreeBookmark(MouseUp_BM);

	}
*/
//}
//---------------------------------------------------------------------------
/*
TDBGridBM DBGrSaveBookMark(TCustomDBGrid* DbGrid)
{
	TDBGridBM tmp;
	tmp.LookedOffset = DbGrid->DataLink->ActiveRecord - (DbGrid->DataLink->RecordCount / 2);
	   THackDBGrid(DbGrid).DataLink.DataSet.MoveBy(-LookedOffset);
	   LockedBookmark := THackDBGrid(DbGrid).DataLink.DataSet.Bookmark;
	   THackDBGrid(DbGrid).DataLink.DataSet.MoveBy(LookedOffset);
  end;
}

//**********************************************************
void DBGrRestoreBookMark(DbGrid : TCustomDbGrid; var DBGridBM : TDBGridBM );
begin
  THackDBGrid(DbGrid).DataLink.DataSet.GotoBookmark(DBGridBM.LockedBookmark);
  THackDBGrid(DbGrid).DataLink.DataSet.MoveBy(DBGridBM.LookedOffset);
end;
*/


//void __fastcall TDataBase::Button1Click(TObject *Sender)
//{
//	IncidentsTable->fi

//	IncidentsTable->First()

//	DBGrid2->fi
//IncidentsTable->i
//	DBGrid2->SelectedIndex
//	DBGrid2->sele
//	IncidentsTable->First();
//	DBGrid2->SelectedRows->CurrentRowSelected = true;
//	IncidentsTable->Next();
//	DBGrid2->SelectedRows->CurrentRowSelected = true;
//	IncidentsTable->Next();
//	DBGrid2->SelectedRows->CurrentRowSelected = true;
//	IncidentsTable->Next();
//	DBGrid2->SelectedRows->CurrentRowSelected = true;
//	IncidentsTable->Next();
//	DBGrid2->SelectedRows->CurrentRowSelected = true;
//	IncidentsTable->Next();
//}


// http://www.infocity.kiev.ua/prog/delphi/content/delphi030.phtml

// ���������� ������ BDGrid
// https://rutube.ru/video/e6a2c92aee6267cafeacc621a356dc27/
// http://www.freepascal.ru/forum/viewtopic.php?f=5&t=10061
/*
void TDataBase::SetFileState_isTransferred(int File_Id) {

	Memo1->Lines->Add("Set File "  + IntToStr(File_Id) + " State isTransferred");


	cs->Enter();


	FilesQuery->Open("SELECT * FROM Files WHERE id = " + IntToStr(File_Id)); // ��������� ������ �����
	FilesQuery->First();
	FilesQuery->Edit();
	FilesQuery->FieldByName("State")->AsInteger = fsTransferred;
	FilesQuery->Post();

	IncidentsQuery->Open("SELECT * FROM Incidents WHERE (File_id = " + IntToStr(File_Id) + ") AND (State = '" + txtAccept + "');"); // ���������� � �������� �������� ...
	IncidentsQuery->First();
	for (int Incident_idx = 0; Incident_idx < IncidentsQuery->RecordCount; Incident_idx++) {

		IncidentsQuery->Edit();
		IncidentsQuery->FieldByName("State")->AsString = txtTransferred;
		IncidentsQuery->Post();
		IncidentsQuery->Next();
	}

	IncidentsTableQuery->DisableControls();
	IncidentsTableQuery->Refresh();
	IncidentsTableQuery->EnableControls();

	cs->Leave();
}
*/


// IncidentsTableQuery
/* 14.05.2019

SELECT      id,
   (SELECT FileName_wo_Path FROM Files WHERE (id = File_id)) AS File_id,
   State,

 CASE Source
  WHEN 0 THEN '���+�'
  WHEN 1 THEN '�����'
 END AS Source,

    NodeIndex,
    DateTime,

 CASE Source
  WHEN 0 THEN Text
  WHEN 1 THEN (SELECT Name FROM VideoDefNames WHERE (def_id = Text))
 END AS Text,

    Comment,
    SpeedLimit,


CASE
  WHEN Side THEN '�����'
  ELSE '������' END AS Side,

    Coord,

   (SELECT Name FROM PUT_GL WHERE (EKASUI_id = PathID)) AS PathID,

    PathText,
    PicType,
	Picture

FROM Incidents
ORDER BY id

*/

// IncidentsTableQuery
/*
SELECT      id,
	File_id,
   State,

 CASE State
  WHEN 0 THEN '��� �������'
  WHEN 1 THEN '��������'
  WHEN 2 THEN '�� ��������'
  WHEN 3 THEN '���� ��������'
  WHEN 4 THEN '�������'
 END AS State2,

 CASE Source
  WHEN 0 THEN '���+���������'
  WHEN 1 THEN '�����'
 END AS Source,

    NodeIndex,
    DateTime,

 CASE Source
  WHEN 0 THEN Text
  WHEN 1 THEN (SELECT Name FROM VideoDefNames WHERE (def_id = Text))
 END AS Text,

    Comment,
    SpeedLimit,


CASE
  WHEN Side THEN '�����'
  ELSE '������' END AS Side,

    Coord,

   (SELECT Name FROM PUT_GL WHERE (EKASUI_id = PathID)) AS PathID,

	PathText,
	PicType,
	Picture

FROM Incidents
ORDER BY id

*/

/*
CASE
		 WHEN InProcessing
			THEN '���������...'
		 ELSE State
	END   AS State,
*/


//	Connection->Connected = false;
//  FilesTable->Active = true;
//	IncidentsTable->Active = true;
//	ShowMessage(Connection->ConnectionName);
//	ShowMessage(Connection->ConnectionString);
//{ u"Database=C:\\Portable\\SQLiteStudio\\idata;DriverID=SQLite" }
//	ShowMessage(Connection->Params->Text);
//	ShowMessage(Connection->ConnectionName);


	   /*
SELECT

	id,
	File_Id,
	(SELECT FState FROM Files WHERE (f_id = File_Id)) AS FileState123,

	CASE
		WHEN (((SELECT State FROM Files WHERE id = File_Id) = 4)  OR
			  ((SELECT State FROM Files WHERE id = File_Id) = 3)) THEN '���������...'
		 ELSE '123.'
	END   AS State_

FROM Incidents */
