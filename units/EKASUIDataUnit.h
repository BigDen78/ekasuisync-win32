//---------------------------------------------------------------------------

#ifndef EKASUIDataUnitH
#define EKASUIDataUnitH

#include <Classes.hpp>
#include <vector>
//#include <IdHTTP.hpp>
#include <IdSSL.hpp>
//#include <IdSSLOpenSSL.hpp>
#include <Xml.XMLDoc.hpp>
#include <System.Win.ScktComp.hpp>
#include "TypesUnit.h"
#include "httpsend.hpp"


//#define DEBUGWEBSERVICES

//---------------------------------------------------------------------------

const
	MaxErrorCount = 5;

const static int RRid[16] = {   1, // �����������
							   10, // ���������������
							   17, // ����������
							   24, // �����������
							   28, // ��������
							   51, // ������-����������
							   58, // ���-���������
							   61, // �����������
							   63, // ������������
							   76, // ������������
							   80, // ����-���������
							   83, // �������-���������
							   88, // ������������
							   92, // ��������-���������
							   94, // �������������
							   96  // ���������������
							};

typedef enum {

	aNotSet = -1,
	aGetEtbDm = 0,
	aGetBPD = 1,
	aGetDefect = 2,
	aSendVideoIncidents = 3,
	aSendDefect = 4,
	aPing = 5,
	aURL = 6

} tAction;

enum tActionState {

	asNotSet = -1,
	asReadyToProcessed = 0,
	asProcessed = 1,
	asError = 2
};

struct tTransactionData {

	tTransactionData(): State(asNotSet),
						DataType(aNotSet),
						SendData(NULL),
						ReceivedData(NULL),
						ErrorCount(MaxErrorCount),
						Header(0),
						Count(0),
						Error(0),
						AnalyzeResponse(false),
						FileName(""),
						Tag(0),
						CanDelete(false) {};

	tActionState State; // ������ ����������?
	tAction DataType;
	TStream* SendData;
	TStringStream* ReceivedData;
	int Header;
	int Count;
	int Error;
	bool AnalyzeResponse;
	int ErrorCount;
	UnicodeString FileName;
	int Tag;
	bool CanDelete;
};

class WebServiceEKASUI: public TThread {

  private:

	TCriticalSection* cs;
//	TIdHTTP* IdHTTP;
//	TIdSSLIOHandlerSocketOpenSSL *IdOpenSSL;
//	TClientSocket* ClientSocket;

	//TStrings* log;
	System::Classes::TComponent* AOwner;
	std::vector<tTransactionData> TData;
	bool EndWorkFlag;
	bool EndWorkOkFlag;
	bool DebugMode;

	void __fastcall Execute();
	void (*AddToLog)(UnicodeString Text);
	void __fastcall COMport_cbfunck(bool State);
	void __fastcall COMport_cbfunck2(bool State, int SummSIZE, int SIZE);
	bool __fastcall AnalyzeResponse(System::Classes::TComponent* AOwner, TStream* Data, int* Header, int* Count, int* Error);
	void __fastcall SocketErrorEvent(System::TObject* Sender, TCustomWinSocket* Socket, TErrorEvent ErrorEvent, int &ErrorCode);

  public:

	WebServiceEKASUI(System::Classes::TComponent* AOwner_);
	__fastcall virtual ~WebServiceEKASUI();

	void StartWork(void);
	void EndWork(void);
	bool EndWorkOK(void);

	//void setLog(TStrings* log_);
	void setLog(void (*func)(UnicodeString Text));
	void setDebugMode(bool State);

	void GetEtbDm(int TypeSD, int Doroga);
	void GetBPD(int Doroga);
//	TStringStream* GetDefect(int Doroga, int pathType, int pathID, int startKM, int startM, int endKM, int endM);
	bool SendVideoIncidents(UnicodeString FileName, int Tag);
	bool SendDefect(UnicodeString FileName, int Tag);
	void Ping(void);
	void URL(UnicodeString Url);

	tTransactionData* GetFirstProcessedItem(void);
	bool DeleteFirstProcessedItem();

};

#endif
