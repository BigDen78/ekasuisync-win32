//---------------------------------------------------------------------------

#ifndef ZipFilesUnitH
#define ZipFilesUnitH

#include <System.Zip.hpp>



const static BPDFileCount = 9;

const static UnicodeString BPDFiles[BPDFileCount] = { "ADMIN.csv",
													  "DIRECTION.csv",
													  "DOROGI.csv",
													  "KRIV.csv",
													  "PUT_GL.csv",
													  "PUT_ST.csv",
													  "STAN.csv",
													  "STREL.csv",
													  "STAN_ON_UPS .csv"};


class tZipFilesManager {

	private:

	TStringList* ZipFiles;
	TStringList* ResFiles[BPDFileCount];
	TStringList* DIRECTION;
	TStringList* PUT_GL;

	UnicodeString ZipFilesPath;
	void ScanZipFiles_AddFile(TSearchRec f);
	void ScanZipFiles(UnicodeString Path);

	public:

	tZipFilesManager(void);
	~tZipFilesManager();

	bool Work(UnicodeString SrcDir, UnicodeString ResDir, UnicodeString TempDir, UnicodeString* NSIVer);

};

//---------------------------------------------------------------------------
#endif
