//---------------------------------------------------------------------------

#pragma hdrstop

#include "ConfigUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


/*
This example demonstrates the usage of Ini files in order to store
and load form configuration between sessions. Example assumes
a RadioGroup and two buttons are present on the main form.
*/
/*
cConfig::cConfig(UnicodeString PortName, UnicodeString ProgramMode_)
{
	DataBasePath = ExtractFilePath(Application->ExeName) + "database\\";
	SkipData = false;
	if (PortName.IsEmpty()) Load();
	else {

		Load();
		COMportName = PortName;
		if (UpperCase(ProgramMode_).Compare("EKASUI") == 0) ProgramMode = pmEKASUI;
			else if (UpperCase(ProgramMode_).Compare("LOCA") == 0) ProgramMode = pmLOCA;
				else ProgramMode = pmLOCA;
		SkipData = true;
	}
}
*/
cConfig::cConfig()
{
	DataBasePath = ExtractFilePath(Application->ExeName) + "database\\";
	SkipData = false;
	Load();
}

cConfig::~cConfig()
{
	Store();
}

void cConfig::Store(void)
{
	if (SkipData) return;

	/* Open an instance */
	TCustomIniFile* SettingsFile = OpenIniFileInstance();

    // Store current form properties to be used in later sessions.
	try
	{
		UnicodeString Name = "Params";
		SettingsFile->WriteBool(Name, "Search", SearchOn);
		SettingsFile->WriteBool(Name, "DebugMode", DebugMode);
		SettingsFile->WriteBool(Name, "DebugMode_ExLOG", ExLOG);

		SettingsFile->WriteBool(Name, "StartTrayMinimized", StartTrayMinimized);
		SettingsFile->WriteBool(Name, "DoNotCloseGotoToTray", DoNotCloseGotoToTray);

		SettingsFile->WriteString (Name, "COMport", COMportName);
		SettingsFile->WriteString (Name, "SearchPath", SearchPath);
		SettingsFile->WriteInteger(Name, "ProgramMode", (int)ProgramMode);
		SettingsFile->WriteInteger(Name, "SendMode_forUZK", SendMode_forUZK);
		SettingsFile->WriteInteger(Name, "SendMode_forVideo", SendMode_forVideo);
//		SettingsFile->WriteBool(Name, "EnableSendToEKASUI", EnableSendToEKASUI);
		SettingsFile->WriteInteger(Name, "EKASUISendMode", EKASUISendMode);
		SettingsFile->WriteInteger(Name, "IncidentDisplayMode1", (int)IncidentDisplayMode1);
		SettingsFile->WriteInteger(Name, "IncidentDisplayMode2", (int)IncidentDisplayMode2);
		SettingsFile->WriteInteger(Name, "ViewImages_Mode", ViewImagesMode);
		SettingsFile->WriteInteger(Name, "ViewImages_Width", ViewImages_Width);
		SettingsFile->WriteInteger(Name, "IncidentSaveTimeInDay", IncidentSaveTimeInDay);


		SettingsFile->WriteBool(Name, "XMLEditNSIverState_On", XMLEditNSIverState_On);
		SettingsFile->WriteString (Name, "XMLEditNSIver", XMLEditNSIver);

		SettingsFile->WriteBool(Name, "XMLEditPathState_On", XMLEditPathState_On);
		SettingsFile->WriteString (Name, "XMLEditPathType", XMLEditPathType);
		SettingsFile->WriteString (Name, "XMLEditPathID", XMLEditPathID);
		SettingsFile->WriteString (Name, "XMLEditPathText", XMLEditPathText);

//		SettingsFile->WriteInteger(Name, "PicViewFormPos.X", PicViewFormPos.X);
//		SettingsFile->WriteInteger(Name, "PicViewFormPos.Y", PicViewFormPos.Y);
//		SettingsFile->WriteInteger(Name, "PicViewFormSize.Width", PicViewFormSize.Width);
//		SettingsFile->WriteInteger(Name, "PicViewFormSize.Height", PicViewFormSize.Height);

		Name = "ColumnsWidth";
		for (int i = 0; i < ColumnCount; i++)
			SettingsFile->WriteInteger(Name, IntToStr(i), ColumnsWidth[i]);
	}

	catch(Exception* e)
	{
	}

	delete SettingsFile;
}

void cConfig::Load(void)
{
	/* Open an instance */
	TCustomIniFile* SettingsFile = OpenIniFileInstance();

	try
	{
		/*
		Read all saved values from the last session. The section name
		is the name of the form. Also use form's properties as defaults
		*/
		UnicodeString Name = "Params";
		SearchOn = SettingsFile->ReadBool(Name, "Search", false);
		SearchOn = true;
		DebugMode = SettingsFile->ReadBool(Name, "DebugMode", false);
		ExLOG = SettingsFile->ReadBool(Name, "DebugMode_ExLOG", false);
		COMportName = SettingsFile->ReadString (Name, "COMport", "COM1");

		StartTrayMinimized = SettingsFile->ReadBool (Name, "StartTrayMinimized", false);
		DoNotCloseGotoToTray = SettingsFile->ReadBool (Name, "DoNotCloseGotoToTray", false);

		SearchPath = SettingsFile->ReadString(Name, "SearchPath", "D:\\EKASUI\\EXPORT\\" /* ExtractFilePath(Application->ExeName) + "OUTBox\\"*/);
		SearchPath = IncludeTrailingPathDelimiter(SearchPath);
		ProgramMode = (tProgramMode)SettingsFile->ReadInteger(Name, "ProgramMode", 0);
//		SendMode = SettingsFile->ReadInteger(Name, "SendMode", 1);

		SendMode_forUZK = (tSendMode)SettingsFile->ReadInteger(Name, "SendMode_forUZK", 1);
		SendMode_forVideo = (tSendMode)SettingsFile->ReadInteger(Name, "SendMode_forVideo", 1);
//		EnableSendToEKASUI = SettingsFile->ReadBool(Name, "EnableSendToEKASUI", 1);
		EKASUISendMode = (tEKASUISendMode)SettingsFile->ReadInteger(Name, "EKASUISendMode", false);
		IncidentDisplayMode1 = (tIncidentDisplayMode1)SettingsFile->ReadInteger(Name, "IncidentDisplayMode1", 0);
		IncidentDisplayMode2 = (tIncidentDisplayMode2)SettingsFile->ReadInteger(Name, "IncidentDisplayMode2", 0);

		ViewImagesMode = (tViewImagesMode)SettingsFile->ReadInteger(Name, "ViewImages_Mode", 0);
		ViewImages_Width = SettingsFile->ReadInteger(Name, "ViewImages_Width", 500);
		IncidentSaveTimeInDay = SettingsFile->ReadInteger(Name, "IncidentSaveTimeInDay", 7);

//		PicViewFormPos.x = SettingsFile->ReadInteger(Name, "PicViewFormPos.X", 300);
//		PicViewFormPos.y = SettingsFile->ReadInteger(Name, "PicViewFormPos.Y", 300);
//		PicViewFormSize.Width =	SettingsFile->ReadInteger(Name, "PicViewFormSize.Width", 300);
//		PicViewFormSize.Height = SettingsFile->ReadInteger(Name, "PicViewFormSize.Height", 300);

		XMLEditNSIverState_On = SettingsFile->ReadBool(Name, "XMLEditNSIverState_On", false);
		XMLEditNSIver = SettingsFile->ReadString(Name, "XMLEditNSIver", "");

		XMLEditPathState_On = SettingsFile->ReadBool(Name, "XMLEditPathState_On", false);
		XMLEditPathType = SettingsFile->ReadString(Name, "XMLEditPathType", "");
		XMLEditPathID   = SettingsFile->ReadString(Name, "XMLEditPathID", "");
		XMLEditPathText = SettingsFile->ReadString(Name, "XMLEditPathText", "");

		Name = "ColumnsWidth";
		for (int i = 0; i < ColumnCount; i++)
			ColumnsWidth[i] = SettingsFile->ReadInteger(Name, IntToStr(i), 65);
	}
	catch(Exception* e)
	{
	}

	delete SettingsFile;
}

TCustomIniFile* cConfig::OpenIniFileInstance()
{
	return new TRegistryIniFile(String("Software\\ekasuisync")/* + Application->Title*/);

	/*
	Open/create a new INI file that has the same name as our executable
	only with the INI extension.
	*/
/*
	switch (RadioGroup1->ItemIndex)
	{
		case 0:
			// Registry mode selected: in HKEY_CURRENT_USER\Software\...
			return new TRegistryIniFile(String("Software\\") + Application->Title);
		case 1:
			// Ini file mode selected
			return new TIniFile(ChangeFileExt(Application->ExeName, ".INI"));
		case 2:
			// Memory based Ini file mode selected
			return new TMemIniFile(ChangeFileExt(Application->ExeName, ".INI"));
	  } */
}