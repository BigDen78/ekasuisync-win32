//---------------------------------------------------------------------------

#pragma hdrstop

#include "FilesUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


// std::vector <FileInfo> Files;

FileList::FileList(void) {

	WaitDelay = 2000;
	callbackFunc = NULL;
//	log = NULL;
	AddToLog = NULL;
}
/*
void FileList::setLog(TStrings* log_)
{
	log = log_;
}*/

FileList::~FileList(void) {

//	for (int i = 0; i < Files.size(); i++) delete(Files[i]);
	Files.resize(0);
}

void FileList::setLog(void (*func)(UnicodeString Text))
{
	AddToLog = func;
}

void FileList::AddFile(TSearchRec f) {

   if ((f.Name != ".") && (f.Name!="..") && (f.Attr != 16) && (f.Attr != 48) && (f.Size != 0)) {

		bool Allready = false;
		for (unsigned int i = 0; i < Files.size(); i++) {

			Allready = Allready || (Files[i].name.Compare(f.Name) == 0);
			if ((Files[i].name.Compare(f.Name) == 0) && (Files[i].size == f.Size) && (GetTickCount() - Files[i].time > WaitDelay) && (Files[i].state == fs_wait)) {

				Files[i].state = fs_ready;
				if (callbackFunc) callbackFunc(Path_ + Files[i].name);
				if (AddToLog) AddToLog("������ ���� ����������: " + Files[i].name);
			}
		}
		if (!Allready) {
			FileInfo newf;
			newf.name = f.Name;
			newf.size = f.Size;
			newf.time = GetTickCount();
			newf.state = fs_wait;

			Files.push_back(newf);
		}
   }
}

void FileList::setCallbackFunc(void (*func)(TFileName name))
{
	// ������������� ��������� �� callback �������
	callbackFunc = func;
}

void FileList::Scan(UnicodeString Path) {

//	int c = 0;
	TSearchRec sr;

	Path = IncludeTrailingPathDelimiter(Path);

	Path_ = Path;
	Path = Path + "*.xml";

	if (FindFirst(Path, faAnyFile | faDirectory, sr) == 0)
	{
		AddFile(sr);
//		Memo1->Lines->Add(sr.Name+", ������: "+IntToStr(sr.Size));
		while (FindNext(sr) == 0) AddFile(sr);
//			Memo1->Lines->Add(sr.Name+", ������: "+IntToStr(sr.Size));
	}
	FindClose(sr);
//	c=Memo1->Lines->Count;
//	Memo1->Lines->Add("������� ������: "+ IntToStr(c));
/*
   if ((sr.Name!=".")&&(sr.Name!=".."))
   {
	if ((sr.Attr==16)||(sr.Attr==48))
	{
	  // ������� ��������� �������
	}
	el
	}
	*/


}

