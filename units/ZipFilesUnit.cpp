//---------------------------------------------------------------------------

#pragma hdrstop

#include "ZipFilesUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

tZipFilesManager::tZipFilesManager(void)
{
	ZipFiles = new TStringList();
	DIRECTION = new TStringList();
	PUT_GL = new TStringList();

	for (int FileIdx = 0; FileIdx < BPDFileCount; FileIdx++)
		ResFiles[FileIdx] = NULL;

}

tZipFilesManager::~tZipFilesManager()
{
	delete ZipFiles;
	delete DIRECTION;
	delete PUT_GL;
}

bool tZipFilesManager::Work(UnicodeString SrcDir, UnicodeString ResDir, UnicodeString TempDir, UnicodeString* NSIVer)
{
 // 1. �������� ������ zip ������ � �������� SrcDir
 // 2. �� ������� zip �����
 //   2.1. ������ ����� "DIRECTION.csv", "PUT_GL.csv"
 //   2.2. ����������� ��������� ����� "DIRECTION.csv" � "PUT_GL.csv"
 //   2.3. ��� ������� ������ ������ "info.xml"

	bool Result = true;

	ScanZipFiles(SrcDir);

	TStringList* tmp = new TStringList;

	for (int FileIdx = 0; FileIdx < BPDFileCount; FileIdx++)
		ResFiles[FileIdx] = NULL;

	Result = ZipFiles->Count > 0;
	for (int i = 0; i < ZipFiles->Count; i++) {

		TZipFile* fzip = new TZipFile();
		//try
		fzip->ExtractZipFile(ZipFilesPath + ZipFiles->Strings[i], TempDir);
		//finally
		delete fzip;
		//end;


		for (int FileIdx = 0; FileIdx < BPDFileCount; FileIdx++) {

			if (i == 0) ResFiles[FileIdx] = new TStringList();
			tmp->Clear();
			if (FileExists(TempDir + BPDFiles[FileIdx])) {
				tmp->LoadFromFile(TempDir + BPDFiles[FileIdx]);
				if (i != 0) tmp->Delete(0);
				ResFiles[FileIdx]->AddStrings(tmp);
				DeleteFile(TempDir + BPDFiles[FileIdx]);
			} else
				Result = false;
		}

		if (FileExists(TempDir + "info.xml")) {

			if ((i == 0) && (NSIVer)) {
				tmp->Clear();
				tmp->LoadFromFile(TempDir + "info.xml");
				*NSIVer = tmp->Text.SubString(10, 10);
			}
			DeleteFile(TempDir + "info.xml");
		}
	}
	delete tmp;

	for (int FileIdx = 0; FileIdx < BPDFileCount; FileIdx++)
		if (ResFiles[FileIdx])
			ResFiles[FileIdx]->SaveToFile(ResDir + BPDFiles[FileIdx]);

	TZipFile* fzip = new TZipFile();

	fzip->Open(ResDir + "BPD_zip.cmd", zmWrite);
	for (int FileIdx = 0; FileIdx < BPDFileCount; FileIdx++) {

		if (FileExists(ResDir + BPDFiles[FileIdx])) {
			fzip->Add(ResDir + BPDFiles[FileIdx], BPDFiles[FileIdx]);
			DeleteFile(ResDir + BPDFiles[FileIdx]);
		}
	}
	delete fzip;
	return Result;
}

void tZipFilesManager::ScanZipFiles_AddFile(TSearchRec f) {

   if ((f.Name != ".") && (f.Name!="..") && (f.Attr != 16) && (f.Attr != 48) && (f.Size != 0))
		ZipFiles->Add(f.Name);
}

void tZipFilesManager::ScanZipFiles(UnicodeString Path) {

//	int c = 0;
	TSearchRec sr;

	ZipFilesPath = Path;
	Path = Path + "*.zip";
	ZipFiles->Clear();

	if (FindFirst(Path, faAnyFile | faDirectory, sr) == 0)
	{
		ScanZipFiles_AddFile(sr);
		while (FindNext(sr) == 0) ScanZipFiles_AddFile(sr);
	}
	FindClose(sr);


}