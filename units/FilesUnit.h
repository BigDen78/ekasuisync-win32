//---------------------------------------------------------------------------

#ifndef FilesUnitH
#define FilesUnitH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <vector>


enum filestate {

	fs_wait = 0,
	fs_ready = 1,
	fs_processed = 2
};

typedef struct {
	TFileName name;
	unsigned int size;
	unsigned int time;
	filestate state;

} FileInfo;

class FileList {

	private:

		unsigned int WaitDelay;
		UnicodeString Path_;
		//TStrings* log;

		void AddFile(TSearchRec f);
		// ��������� �� callback �������
		void (*callbackFunc)(TFileName name);
		void (*AddToLog)(UnicodeString Text);

	public:
		std::vector <FileInfo> Files;

		FileList(void);
		~FileList(void);

		void setCallbackFunc(void (*func)(TFileName name));
		void Scan(UnicodeString Path);
		//void setLog(TStrings* log_);
		void setLog(void (*func)(UnicodeString Text));
};

//---------------------------------------------------------------------------

#endif

