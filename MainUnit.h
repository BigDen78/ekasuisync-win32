//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.FileCtrl.hpp>
#include <Vcl.ComCtrls.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <IdCoder.hpp>
#include <IdCoder3to4.hpp>
#include <IdCoderMIME.hpp>
#include <Vcl.AppEvnts.hpp>
#include <Vcl.Buttons.hpp>

#include "trayicon.h"
#include "DBAxisGridsEh.hpp"
#include "DBGridEh.hpp"
#include "DBGridEhGrouping.hpp"
#include "DBGridEhToolCtrls.hpp"
#include "DynVarsEh.hpp"
#include "EhLibVCL.hpp"
#include "GridsEh.hpp"
#include "ToolCtrlsEh.hpp"

#include "FilesUnit.h"
#include "XMLPerserUnit.h"
#include "ZModemUnit_.h"
#include "DataBaseUnit.h"
#include "ZipFilesUnit.h"
#include "ConfigUnit.h"
#include "TypesUnit.h"
#include "nrcommbox.hpp"
#include "nrclasses.hpp"
#include "nrcomm.hpp"
#include "EKASUIDataUnit.h"
#include "Sysutils.hpp"
#include <Vcl.Menus.hpp>
#include <Datasnap.DSCommonServer.hpp>
#include <Datasnap.DSHTTP.hpp>
#include <IPPeerServer.hpp>

#include <Excel_XP.h>
#include <Vcl.Grids.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <locale>
#include <fcntl.h>
#include <io.h>

//#define NoDeleteCMDFiles

const TestStatePeriod = 5000;

const cmd_GetState = 100;
const cmd_GetSD = 101;
const cmd_GetBPD = 102;
//const cmd_TransferAnswer = 103;
const cmd_NSIver = 104;
const cmd_StateAnswer = 105;

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
	TTimer *WorkTimer;
	TPageControl *PageControl;
	TTabSheet *CarSheet;
	TTabSheet *EKASUISheet;
	TTrayIcon *TrayIcon;
	TApplicationEvents *ApplicationEvents;
	TPageControl *PageControl1;
	TTabSheet *DBPage;
	TTabSheet *ConfigTabSheet1;
	TTabSheet *TabSheet2;
	TMemo *Memo1;
	TPanel *Panel2;
	TSpeedButton *SpeedButton1;
	TSpeedButton *SpeedButton2;
	TSpeedButton *SpeedButton3;
	TTabSheet *DebugTabSheet;
	TMemo *Memo4;
	TButton *Button2;
	TPanel *ConfigPanel;
	TGroupBox *gbDownloadMode;
	TGroupBox *gbOnLine;
	TRadioButton *rbWorkOn;
	TRadioButton *rbWorkOff;
	TGroupBox *GroupBox4;
	TGroupBox *GroupBox5;
	TRadioButton *rbModeAvicon03;
	TRadioButton *rbModeEKASUI;
	TGroupBox *gbDir;
	TLabel *DirLabel;
	TButton *Button1;
	TPageControl *PageControl2;
	TTabSheet *TabSheet3;
	TTabSheet *ConfigTabSheet2;
	TTimer *TestOutBoxDirTimer;
	TLabel *Label1;
	TBevel *Bevel1;
	TSpeedButton *SendToEKASUI_State;
	TLabel *Label2;
	TTimer *TestConnectionTimer;
	TOpenDialog *OpenDialog;
	TStatusBar *StatusBar_;
	TLabel *Label4;
	TSpeedButton *ViewImages_State;
	TLabel *Label5;
	TTabSheet *ZModemDebugSheet;
	TButton *Button3;
	TOpenDialog *OpenDialog1;
	TButton *Button4;
	TRadioButton *rbUZKSendAuto;
	TRadioButton *rbUZKSendManual;
	TLabel *Label3;
	TLabel *Label6;
	TGroupBox *GroupBox1;
	TRadioButton *rbAutoSendModeToEKASUI;
	TRadioButton *rbManualSendModeToEKASUI;
	TPanel *Panel1;
	TRadioButton *rbVideoSendAuto;
	TRadioButton *rbVideoSendManual;
	TPanel *Panel3;
	TDBGridEh *IncidentsDBGrid;
	TPanel *ImagePanel;
	TImage *PicImage;
	TPanel *NoImagePanel;
	TSplitter *Splitter;
	TnrDeviceBox *cbCOMPortName;
	TnrComm *nrComm1;
	TTabSheet *DBDebugSheet;
	TSpeedButton *SendButton;
	TPopupMenu *TrayPopupMenu;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TMenuItem *PrgVer;
	TMenuItem *N4;
	TGroupBox *GroupBox2;
	TCheckBox *cbStartTrayMinimized;
	TCheckBox *cbDoNotCloseGotoToTray;
	TTimer *MinimizeTimer;
	TBevel *Bevel3;
	TLabel *Label7;
	TBevel *Bevel4;
	TSpeedButton *IncidentDisplay_Mode2;
	TBevel *Bevel5;
	TLabel *lTestConnection;
	TButton *Button5;
	TDSHTTPService *DSHTTPService1;
	TGroupBox *gIncidentSaveTimeInDay;
	TComboBox *cbIncidentSaveTimeInDay;
	TSpeedButton *IncidentDisplay_Mode1;
	TGroupBox *GroupBox3;
	TCheckBox *XMLEditPathState;
	TLabel *XMLEditPathType;
	TEdit *EditXMLEditPathType;
	TEdit *EditXMLEditPathID;
	TLabel *XMLEditPathID;
	TEdit *EditXMLEditPathText;
	TLabel *XMLEditPathText;
	TCheckBox *XMLEditNSIverState;
	TLabel *XMLEditNSIverText;
	TEdit *EditXMLEditNSIver;
	TSpeedButton *SpeedButton4;
	TPopupMenu *pmDisplayMode;
	TMenuItem *Type_All;
	TMenuItem *Type_US_M;
	TMenuItem *Type_Video;
	TMenuItem *Filtr_Off;
	TMenuItem *Filtr_DontSend;
	TMenuItem *Filtr_Sendet;
	TMenuItem *Filtr_ThisDay;
	TMenuItem *N8;
	TPopupMenu *pmSendToEKASUIMode;
	TMenuItem *SendMode_OFF;
	TMenuItem *SendMode_Handly;
	TMenuItem *SendMode_Auto;
	TTimer *PopUpMenu_Timer;
	TButton *InsertText1;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TGroupBox *gbLoadLightBPD;
	TButton *LoadLigthBPD;
	TLabel *Label8;
	TLabel *Label9;
	TButton *Button6;
	TTabSheet *TabSheet1;
	TButton *Button11;
	TStringGrid *StringGrid1;
	TPanel *Panel4;
	TButton *Button12;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall WorkTimerTimer(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall cbOnLineClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall ApplicationEventsMinimize(TObject *Sender);
	void __fastcall TrayIconClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall ControlsToConfig(void);
//  void __fastcall ControlsToConfig(UnicodeString SearchPath_);
	void __fastcall rbWorkOnClick(TObject *Sender);
	void __fastcall rbWorkOffClick(TObject *Sender);
	void __fastcall rbModifyByUser(TObject *Sender);
	void __fastcall SpeedButton1Click(TObject *Sender);
	void __fastcall SpeedButton2Click(TObject *Sender);
	void __fastcall SpeedButton3Click(TObject *Sender);
	void __fastcall TestOutBoxDirTimerTimer(TObject *Sender);
	void __fastcall SendToEKASUI_StateClick(TObject *Sender);
	void __fastcall TestConnectionTimerTimer(TObject *Sender);
	void __fastcall ViewImages_StateClick(TObject *Sender);
	void __fastcall ApplicationEventsMessage(tagMSG &Msg, bool &Handled);
//	void __fastcall IncidentsDBGridDrawColumnCell(TObject *Sender, const TRect &Rect,
//		  int DataCol, TColumnEh *Column, Gridseh::TGridDrawState State);
	void __fastcall IncidentsDBGridDblClick(TObject *Sender);
	void __fastcall IncidentsDBGridCellClick(TColumnEh *Column);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall PicImageClick(TObject *Sender);
	void __fastcall SplitterMoved(TObject *Sender);
	void __fastcall SendButtonClick(TObject *Sender);
	void __fastcall IncidentsDBGridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall cbCOMPortNameChange(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall cbStartTrayMinimizedClick(TObject *Sender);
	void __fastcall cbDoNotCloseGotoToTrayClick(TObject *Sender);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall MinimizeTimerTimer(TObject *Sender);
	void __fastcall IncidentDisplay_Mode2Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall IncidentDisplay_Mode1Click(TObject *Sender);
	void __fastcall EditXMLChange(TObject *Sender);
	void __fastcall LoadLigthBPDClick(TObject *Sender);
	void __fastcall InsertText1Click(TObject *Sender);
	void __fastcall SpeedButton4Click(TObject *Sender);
	void __fastcall pmDisplayMode_Click(TObject *Sender);
	void __fastcall PopUpMenu_TimerTimer(TObject *Sender);
	void __fastcall pmSendToEKASUIMode_Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);



private:	// User declarations

	TPoint pt;
	bool PopupMenu_Visible;
	TPopupMenu* WorkPM;

	UnicodeString Test;
	int DisplayPictureId;
	FileList * searchfiles;
	WebServiceEKASUI* EKASUI;

	int TestCount;
	bool TestReadyFile; // ���� ����������� ��� ����� ��������� ��� �� ������ ������� � ��������. ���� �������� ��� ��������� ����� ������ � �������� ���������

	TListItem *ListSelectItem;
	System::Uitypes::TWindowState saveWindowState;

//	bool FileSendinProgress;
//	UnicodeString FileSendFN;
//	int LoadDataSize;
//	int SendFile_iBDIdx;
	bool FirstTime;
//	tIncidentList IList;
	bool AlreadyRun;
//	bool SkipRebootMessage;
	bool SkipModify;

//	System::DynamicArray<System::Byte> MouseDown_BM;
//	System::DynamicArray<System::Byte> MouseUp_BM;
//	bool ShiftDown;

//	int ZModem_SendFile_Id;
//	UnicodeString ZModem_FileName;
	int ZModem_FileProceedPercent;
	int SaveImagePanelWidth;
	DWORD StatusBarAddTextTime;

	std::wofstream log;
	UnicodeString TEMPDir;

	int CmdFileIndex;
	int BPDLoaded;
	long InitGetStateTime;
	bool InitGetStateFlag;
	bool PC_connection;
	bool EKASUI_connection;
	DWORD OpenParam1;
	DWORD OpenParam2;

	void static ReadyNewFile(UnicodeString fn);

	void __fastcall ConfigToControls(void);
	void SetPicImageSize(void);
	void SetStateForSelectRows(UnicodeString State);
	static void OnTransmissionProgress(tTransmissionState State, UnicodeString Fn, int Tag, int Progress, float Speed);
	static void OnFileReceived(UnicodeString Fn);
	static void OnFileTransferred(UnicodeString Fn, int Tag);
	static void OnWaitForReceivingTimeOut(void);
	static void OnError(void);
	static void OnTextToLog(UnicodeString Text);

	void InitGetState(void);
	void SendStateAnswer(bool EKASUIOK, std::vector <FilesListItem> FilesList); // LOCA <<< ������
//	void SendTransferAnswer(bool TransferOK, UnicodeString FileName);

	void GetSD(int rrId);
	void GetBPD(void);
	void SendNSIver(UnicodeString NSIver);
	void SendIncident(UnicodeString FileName); // LOCA >>> ������

public:		// User declarations
	__fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern cConfig* Config_;
extern PACKAGE TMainForm *MainForm;
//extern bool ExLOG;
//---------------------------------------------------------------------------
#endif

